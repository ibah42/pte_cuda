#pragma once

#include "PTESources/CudaInclude.hpp"

#include "PTESources/Math/Utility.hpp"
#include "PTESources/Math/Angle.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace PTE{
		
/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

class Vec3
{
public:
	
/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

	__host__ __device__ void zero()
	{
		x=0;y=0;z=0;
	}

	__host__ __device__ static Vec3 randomVector()
	{
		float x1 = ((::rand() / (float)RAND_MAX) - 0.5f) * 2;
		float x2 = ((::rand() / (float)RAND_MAX) - 0.5f) * 2;
		while(x1*x1 + x2*x2 >= 1)
		{
			x1 = ((::rand() / (float)RAND_MAX) - 0.5f) * 2;
			x2 = ((::rand() / (float)RAND_MAX) - 0.5f) * 2;
		}

		float x = 2*x1*sqrt(1-x1*x1-x2*x2);
		float y = 2*x2*sqrt(1-x1*x1-x2*x2);
		float z = 1 - 2*(x1*x1+x2*x2);

		return Vec3(x,y,z);
	}

	__host__ __device__ Vec3()	{}

	__host__ __device__ explicit Vec3( float f )
		:	x( f )
		,	y( f )
		,	z( f ) 
	{}

	__host__ __device__ Vec3( float _x, float _y, float _z )
		:	x( _x )
		,	y( _y )
		,	z( _z )
	{}


#ifdef USE_GLM
	__host__ __device__ Vec3( const glm::vec3& v )
		:	x( v.x )
		,	y( v.y )
		,	z( v.z )
	{}
#endif// USE_GLM

	__host__ __device__ Vec3( const Vec3& _v )
		:	x( _v.x )
		,	y( _v.y )
		,	z( _v.z )
	{}

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


	__host__ __device__ float& operator[]( int32 _index )
	{
		assert( _index >= 0 && _index <= 2 );
		return (&x)[ _index ];
	}
	
	__host__ __device__ float operator[]( int32 _index ) const
	{
		assert( _index >= 0 && _index <= 2 );
		return (&x)[ _index ];
	}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

	
	__host__ __device__ Vec3& operator = ( const Vec3& _v )
	{
		x = _v.x;
		y = _v.y;
		z = _v.z;
		return *this;
	}
	
	__host__ __device__ bool operator == ( const Vec3& _v ) const
	{
		return x == _v.x && y == _v.y && z == _v.z;
	}

	__host__ __device__ bool operator != ( const Vec3& _v ) const
	{
		return x != _v.x || y != _v.y || z != _v.z;
	}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


	__host__ __device__ Vec3 operator - () const
	{
		return Vec3( -x, -y, -z );
	}

	__host__ __device__ Vec3 operator + ( const Vec3& _v ) const
	{
		return Vec3( x + _v.x, y + _v.y, z + _v.z );
	}

	__host__ __device__ Vec3 operator - ( const Vec3& _v ) const
	{
		return Vec3( x - _v.x, y - _v.y, z - _v.z );
	}

	__host__ __device__ Vec3 operator * ( float f ) const
	{
		return Vec3( x * f, y * f, z * f );
	}

	__host__ __device__ Vec3 operator / ( float f ) const
	{
		f = 1.0f / f;
		return Vec3( x * f, y * f, z * f );
	}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
	

	__host__ __device__ Vec3& operator += ( const Vec3& _v )
	{
		x += _v.x;
		y += _v.y;
		z += _v.z;

		return *this;
	}
	
	__host__ __device__ Vec3& operator -= ( const Vec3& _v )
	{
		x -= _v.x;
		y -= _v.y;
		z -= _v.z;

		return *this;
	}

	__host__ __device__ Vec3& operator *= ( float f )
	{
		x *= f;
		y *= f;
		z *= f;

		return *this;
	}

	__host__ __device__ Vec3& operator /= ( float f )
	{
		f = 1.0f / f;
		x *= f;
		y *= f;
		z *= f;

		return *this;
	}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

	__host__ __device__ bool equal( const Vec3& that, float eps = 0.001f ) const
	{
		return
				fabs( x-that.x) < eps 
			&& 	fabs( y-that.y) < eps
			&& 	fabs( z-that.z) < eps
		;
	}

	__host__ __device__ bool isZero() const
	{
		return x==0.0f && y==0.0f && z==0.0f;
	}

	__host__ __device__ bool isFinite() const
	{
		return PTE::isFinite(x) && PTE::isFinite(y) && PTE::isFinite(z);
	}

	__host__ __device__ bool isNormalized() const
	{
		return isFinite() && fabs( magnitude() - 1.f ) < 0.00001f;
	}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


	__host__ __device__ float magnitudeSquared() const
	{
		return x*x + y*y + z*z;
	}

	__host__ __device__ float magnitude() const
	{
		return sqrt( magnitudeSquared() );
	}

	__host__ __device__ float dot(const Vec3& _v) const		
	{	
		return x*_v.x + y*_v.y + z*_v.z;				
	}

	__host__ __device__ Vec3 cross(const Vec3& _v) const
	{
		return Vec3(
				y * _v.z - z * _v.y
			,	z * _v.x - x * _v.z
			,	x * _v.y - y * _v.x
		);
	}

	__host__ __device__ Vec3 getNormalized() const
	{
		const float magnitude = magnitudeSquared();
		return
			magnitude > 0
				?	*this / sqrt( magnitude )
				:	Vec3( 0, 0, 0 )
			;
	}

	__host__ __device__ float normalize()
	{
		const float magnitude = this->magnitude();
// 		if ( magnitude < 0.0000000000001f )
// 			return 0.0f;
// 		else
		{
			*this *= 1.0f / magnitude;
			return magnitude;
		}
	}

	__host__ __device__ void voidNormalize()
	{
		const float magnitude = this->magnitude();
// 		if ( magnitude < 0.0000000000001f )
// 			return;
// 		else
		{
			*this *= 1.0f / magnitude;
		}
	}

	__host__ __device__ Vec3 multiply( const Vec3& _v ) const
	{
		return Vec3( x*_v.x , y*_v.y , z*_v.z );
	}

	__host__ __device__ Vec3 minimal(const Vec3& _v) const
	{ 
		return Vec3(
				PTE::minimal( x, _v.x )
			,	PTE::minimal( y, _v.y )
			,	PTE::minimal( z, _v.z )
		);	
	}

	__host__ __device__ float minElement()	const
	{
		return PTE::minimal( x, PTE::minimal( y, z ) );
	}
	
	__host__ __device__ Vec3 maximal(const Vec3& _v) const
	{ 
		return Vec3(
				PTE::maximal( x, _v.x )
			,	PTE::maximal( y, _v.y )
			,	PTE::maximal( z, _v.z )
		);	
	} 

	__host__ __device__ float maxElement()	const
	{
		return PTE::maximal( x, PTE::maximal( y, z ) );
	}


	__host__ __device__ void set( float _x, float _y, float _z )
	{
		x=_x; y=_y; z=_z;
	}


	__host__ __device__ void rotate( float cosA, const Vec3& rotator )
	{		
		float sinA = sqrt( 1 - cosA*cosA );
		
		*this = (*this) * cosA
			+	rotator.cross(*this) * sinA
			+	rotator * ( rotator.dot(*this) * (1.f-cosA) )
		;
	};

	__host__ __device__ void rotateToVector3( Angle a, const Vec3& v, const Vec3& dv )
	{
		Vec3 rot = cross( v );
		rot.normalize();
		Angle currAngle = Angle::acos( dot( v ) );
		Angle rotA = currAngle - a.radians();
		
		Vec3 result = *this;
		result.rotate( rotA.cos(), rot );
		
		if ( fabs( result.dot(v) - a.cos() ) < 1e-4f )
		{
			*this = result;
		}
		else
		{
			rotA = -rotA;
			this->rotate( rotA.cos(), rot );	
		}

		voidNormalize();

// 		if( dot(dv)< -0.6 )
// 			*this = -*this;
		
	}


	__host__ __device__ float magnitudeXZ()
	{
		return sqrt( x*x + z*z );
	}

public:

	float x;
	float y;
	float z;

}; // class Vec3


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__host__ __device__ static inline Vec3 operator *( float f, const Vec3& _v )
{
	return _v * f;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


} // namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
