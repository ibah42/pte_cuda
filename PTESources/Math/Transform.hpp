#pragma once

#include "PTESources/CudaInclude.hpp"

#include "PTESources/Math/Quaternion.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace PTE{

		
/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

class Matrix44;
class Matrix33;
class Vec4;
class Vec3;

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

class Transform
{

public:
	
	__host__ __device__ Transform() 
		:	q( 0, 0, 0, 0 )
		,	v( 0, 0, 0 )
	{
	}

	__host__ __device__ explicit Transform(const Vec3& _position)
		:	q( 0, 0, 0, 1 )
		,	v( _position )
	{
	}

	__host__ __device__ explicit Transform( const Quaternion& _orientation )
		:	q( _orientation)
		,	v( 0, 0, 0)
	{
		assert( q.isSane() );
	}

	__host__ __device__ Transform( const Vec3& _v, const Quaternion& _q )
		:	q( _q )
		,	v( _v ) 
	{
		assert( q.isSane() );
	}

	__host__ __device__ Transform( const Matrix44& m );


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


	//! Transform transform to parent (returns compound transform: first src, then *this)
	__host__ __device__ Transform transform( const Transform& _source) const
	{
		assert( _source.isSane() );
		assert( isSane() );
		// src = [srct, srcr] -> [r*srct + t, r*srcr]
		return Transform( q.rotate( _source.v ) + v, q * _source.q );
	}

	//! Transform transform from parent (returns compound transform: first source, then this->inverse)
	__host__ __device__ Transform transformInv( const Transform& _source ) const
	{
		assert( _source.isSane() );
		assert( isFinite() );
		// src = [srct, srcr] -> [r^-1*(srct-t), r^-1*srcr]
		Quaternion qinv = q.getConjugate();
		return Transform( qinv.rotate( _source.v - v ), qinv * _source.q );
	}

	__host__ __device__ Vec3 transform( const Vec3& _v ) const
	{
		assert( isFinite() );
		return q.rotate( _v ) + v;
	}

	__host__ __device__ Vec3 transformInv(const Vec3& _v ) const
	{
		assert( isFinite() );
		return q.rotateInv( _v - v );
	}
	

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


	__host__ __device__ Transform operator * ( const Transform& t ) const
	{
		assert( t.isSane() );
		return transform( t );
	}

	__host__ __device__ Transform getInverse() const
	{
		assert( isFinite() );
		return Transform( q.rotateInv( -v ), q.getConjugate() );
	}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


	__host__ __device__ Vec3 rotate(const Vec3& _v ) const
	{
		assert( isFinite() );
		return q.rotate( _v );
	}

	__host__ __device__ Vec3 rotateInv(const Vec3& _v ) const
	{
		assert(isFinite());
		return q.rotateInv( _v );
	}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


	__host__ __device__ bool isValid() const
	{
		return v.isFinite() && q.isFinite() && q.isUnit();
	}
		
	__host__ __device__ bool isSane() const
	{
	      return isFinite() && q.isSane();
	}
	
	__host__ __device__ bool isFinite() const
	{
		return v.isFinite() && q.isFinite();
	}
	
	__host__ __device__ static Transform createIdentity() 
	{ 
		return Transform( Vec3( 0 ) ); 
	}

	__host__ __device__ Transform getNormalized() const
	{
		return Transform( v, q.getNormalized() );
	}
		
public:
	
	Quaternion q;
	Vec3 v;

}; // class Transform


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


} // namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
