#pragma once

#include "PTESources/CudaInclude.hpp"

#include "PTESources/Math/Vector3.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

class Vec4
{
public:
	
/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


	__host__ __device__ Vec4()
	{}

	__host__ __device__ explicit Vec4( float f )
		:	x( f )
		,	y( f )
		,	z( f )
		,	w( f )
	{}

	__host__ __device__ Vec4( float _x, float _y, float _z, float _w )
		:	x( _x )
		,	y( _y )
		,	z( _z )
		,	w( _w )
	{}

	__host__ __device__ Vec4( const Vec4& _v )
		:	x( _v.x )
		,	y( _v.y )
		,	z( _v.z )
		,	w( _v.w )
	{}
	
	__host__ __device__ Vec4( const Vec3& _v, float _w )
		:	x( _v.x )
		,	y( _v.y )
		,	z( _v.z )
		,	w( _w )
	{}

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


	__host__ __device__ float& operator[]( int32 _index )
	{
		assert( _index >= 0 && _index <= 3 );
		return (&x)[ _index ];
	}
	
	__host__ __device__ float operator[]( int32 _index ) const
	{
		assert( _index >= 0 && _index <= 3 );
		return (&x)[ _index ];
	}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

	
	__host__ __device__ Vec4& operator = ( const Vec4& _v )
	{
		x = _v.x;
		y = _v.y;
		z = _v.z;
		w = _v.w;
		return *this;
	}
	
	__host__ __device__ bool operator == ( const Vec4& _v ) const
	{
		return x == _v.x && y == _v.y && z == _v.z && w == _v.w;
	}

	__host__ __device__ bool operator !=( const Vec4& _v ) const
	{
		return x != _v.x || y != _v.y || z != _v.z || w != _v.w;
	}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


	__host__ __device__ Vec4 operator - () const
	{
		return Vec4( -x, -y, -z, -w );
	}

	__host__ __device__ Vec4 operator + ( const Vec4& _v ) const
	{
		return Vec4( x + _v.x, y + _v.y, z + _v.z, w + _v.w );
	}

	__host__ __device__ Vec4 operator - ( const Vec4& _v ) const
	{
		return Vec4( x - _v.x, y - _v.y, z - _v.z, w - _v.w );
	}

	__host__ __device__ Vec4 operator * ( float f ) const
	{
		return Vec4( x * f, y * f, z * f, w * f );
	}

	__host__ __device__ Vec4 operator / ( float f ) const
	{
		f = 1.0f / f;
		return Vec4( x * f, y * f, z * f, w * f );
	}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
	

	__host__ __device__ Vec4& operator += ( const Vec4& _v )
	{
		x += _v.x;
		y += _v.y;
		z += _v.z;
		w += _v.w;

		return *this;
	}
	
	__host__ __device__ Vec4& operator -= ( const Vec4& _v )
	{
		x -= _v.x;
		y -= _v.y;
		z -= _v.z;
		w -= _v.w;

		return *this;
	}

	__host__ __device__ Vec4& operator *= ( float f )
	{
		x *= f;
		y *= f;
		z *= f;
		w *= f;

		return *this;
	}

	__host__ __device__ Vec4& operator /= ( float f )
	{
		f = 1.0f / f;
		x *= f;
		y *= f;
		z *= f;
		w *= f;

		return *this;
	}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


	__host__ __device__ bool isZero() const
	{
		return x==0.0f && y==0.0f && z==0.0f && w==0.0f;
	}

	__host__ __device__ bool isFinite() const
	{
		return PTE::isFinite(x) && PTE::isFinite(y) && PTE::isFinite(z) && PTE::isFinite(w);
	}

	__host__ __device__ bool isNormalized() const
	{
		return isFinite() && fabs( magnitude() - 1.f ) < 0.00001f;
	}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


	__host__ __device__ float magnitudeSquared() const
	{
		return x*x + y*y + z*z + w*w;
	}

	__host__ __device__ float magnitude() const
	{
		return sqrt( magnitudeSquared() );
	}

	__host__ __device__ float dot( const Vec4& _v ) const		
	{	
		return x*_v.x + y*_v.y + z*_v.z + w*_v.w;
	}

	__host__ __device__ Vec4 getNormalized() const
	{
		const float magnitude = magnitudeSquared();
		return
			magnitude > 0
				?	*this / sqrt( magnitude )
				:	Vec4( 0, 0, 0, 0 )
			;
	}

	__host__ __device__ float normalize()
	{
		const float magnitude = this->magnitude();
		if ( magnitude < 0.00001f )
			return 0.0f;
		else
		{
			*this *= 1.0f / magnitude;
			return magnitude;
		}
	}

	__host__ __device__ void voidNormalize()
	{
		const float magnitude = this->magnitude();
		if ( magnitude < 0.00001f )
			return;
		else
		{
			*this *= 1.0f / magnitude;
		}
	}

	__host__ __device__ Vec4 multiply( const Vec4& _v ) const
	{
		return Vec4( x*_v.x , y*_v.y , z*_v.z , w*_v.w );
	}

	__host__ __device__ Vec4 minimal(const Vec4& _v) const
	{ 
		return Vec4(
				PTE::minimal( x, _v.x )
			,	PTE::minimal( y, _v.y )
			,	PTE::minimal( z, _v.z )
			,	PTE::minimal( w, _v.w )
		);	
	}

	__host__ __device__ float minElement()	const
	{
		return PTE::minimal( x, PTE::minimal( y, PTE::minimal( z, w ) ) );
	}
	
	__host__ __device__ Vec4 maximal(const Vec4& _v) const
	{ 
		return Vec4(
				PTE::maximal( x, _v.x )
			,	PTE::maximal( y, _v.y )
			,	PTE::maximal( z, _v.z )
			,	PTE::maximal( w, _v.w )
		);	
	} 

	__host__ __device__ float maxElement()	const
	{
		return PTE::maximal( x, PTE::maximal( y, PTE::maximal( z, w ) ) );
	}


	__host__ __device__ Vec3 getXYZ() const
	{
		return Vec3( x, y, z );
	}

public:

	float x;
	float y;
	float z;
	float w;

}; // class Vec4


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__host__ __device__ static inline Vec4 operator *( float f, const Vec4& _v )
{
	return _v * f;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
