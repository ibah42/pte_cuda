#pragma once

#include "PTESources/CudaInclude.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace PTE{
	
/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
	
#define Pi		(3.14159265359f)
#define HalfPi	(Pi * 0.5f)
#define TwoPi	(Pi * 2.0f)
#define G		9.80665f


	typedef unsigned char		byte;

	typedef int					int32;
	typedef long long			int64;
	
	typedef unsigned long long	uint64;
	typedef unsigned int		uint32;


__host__ void detectedFaultCPU();


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


#define PTE_CHECK( error )				\
	if( (error) != 0 )					\
	{									\
		detectedFaultCPU();				\
		checkCudaErrors( (error) );		\
	}									\


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


//use buffer not less then char[40]
__device__ void inline printDeviceInt32( char* const buff, int32 value )
{
	// 40, do not modify buffer sizes!
	//because at end it will end with ONE zeros!

	if( value<0 )
	{
		buff[0] = '-';
		value = -value;
	}
	else
		buff[0] = '+';

	int32 value2 = value;
	int32 size = 0;
	for( int32 i=1; i<16; i++) 
	{
		buff[i] = ' ';
		value2 /= 10;
		++size;

		if( value2 == 0 )
			break;
	}

	char* wb = buff + size + 1;
	wb[0] = 0;
	--wb;

	do
	{
		*wb = value % 10 + '0';
		-- wb;
		value /= 10;
	}
	while (value != 0);		
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template< typename T >
__device__ void DEVICE_PTE_ASSERT( T data, const char* file, uint32 line )		
{
#ifdef _DEBUG
	if ( data == 0 )								
	{	
		char buff[40];
		printDeviceInt32(buff, line);
		printf(
				"%s%s%s%s%s"
			,	" File::"
			,	file
			,	" Line::"
			,	buff
			,	";\n"
		);		
		assert(0);
	}		
#endif
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template< typename T >
__host__ void CPU_PTE_ASSERT( T data, const char* file, uint32 line )						
{				
#ifdef _DEBUG
	if ( data == 0 )								
	{												
		detectedFaultCPU();							
		std::cout<< file << line <<" critical error occured \n ";
		
		exit(3);
	}		
#endif // _DEBUG
}													


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

//http://habrahabr.ru/company/ifree/blog/197520/
#define PARALLEL_SPECULATIVE_EXECUTION_BARRIER  _ReadWriteBarrier();


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template< typename T >
__host__ void copyToDevice( T* dest, const T* source, const int32 count )
{
	assert(count>0 && dest != 0 && source != 0 );
	PTE_CHECK( cudaMemcpy( dest, source, count * sizeof(T), cudaMemcpyHostToDevice ) );	
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template< typename T >
__host__ void copyToHost( T* dest, const T* source, const int32 count )
{
	assert(count>0 && dest != 0 && source != 0 );
	PTE_CHECK( cudaMemcpy( dest, source, count * sizeof(T), cudaMemcpyDeviceToHost ) );	
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template< typename T >
__host__ void allocHost( T** ptrToHost, int32 count )
{
	*ptrToHost = (T*) malloc( sizeof(T) * count );
	assert(*ptrToHost);
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template< typename T >
__host__ void freeHost( T* ptrToHost )
{
	free(ptrToHost);	
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template< typename T >
__host__ void allocDevice( T** ptrToDevice, int32 count )
{
	assert(count>0);
	PTE_CHECK( cudaMalloc( ptrToDevice, sizeof(T) * count ) );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template< typename T >
__host__ void freeDevice( const T* ptrToDevice )
{
	PTE_CHECK( cudaFree( const_cast< T* >(ptrToDevice) ) );
}

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template< typename T >
__host__ __device__ void swap( const T* t1, const T* t2 )
{
	T temp = *t1;
	const_cast< T& >( *t1 ) = const_cast< T& >( *t2 );
	const_cast< T& >( *t2 ) = temp;	
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template< typename Base, typename Radix >
__host__ __device__ Base powPositive( Base value, Radix radix )
{
	Base result = 1;
	while( radix > 0 )
	{
		result *= value;
		--radix;
	}

	return result;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__host__ __device__ inline float sqrt( float v )
{
	return ::sqrt( v );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__host__ __device__ inline bool	isFinite( float v )
{
	return
			v < 999999.f * 999999.f
		&&	v > - 999999.f * 999999.f
	;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template < typename T >
__host__ __device__ inline T linearInterpolation ( T a, T b, float mix )
{
	return a + ( b - a ) * mix; 
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template < typename T >
__host__ __device__ inline T
cubicInterpolation ( T P0, T P1, T P2, T P3, float mix )
{
	float mix_sq = mix * mix;
	T PH = ( P3 - P2 - P0 + P1 );
	return( 
			PH * mix * mix_sq
		+	( P0 - P1 - PH ) * mix_sq
		+	( P2 - P0 ) * mix
		+	P1
	); 
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template < typename T, typename T1 >
__host__ __device__ inline T
clamp ( T1 downTresh, T1 upTresh, T value )
{
	return
		value >= downTresh
		?	(
				value <= upTresh
				?	value
				:	upTresh
			)
		:	downTresh
	;
};


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template <class T>
__host__ __device__ inline void swapValues ( T & a, T & b )
{
	T c = a;
	a = b;
	b = c;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__host__ __device__ inline bool cmpEqual ( float a, float b, float eps )
{
	return fabs( a-b ) < eps;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template< typename T > 
__host__ __device__ inline T minimal( T a, T b )
{
	return a > b	?	b : a;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template< typename T >
__host__ __device__ inline T maximal( T a, T b )
{
	return a > b	?	a : b;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template< typename TVec3 >
__host__ __device__ inline TVec3 
getTriangleLocalBounds( const TVec3& v1, const TVec3& v2, const TVec3& v3 )
{
	float xmin = minimal( v1.x, v2.x );
	xmin = minimal( xmin, v3.x );

	float ymin = minimal( v1.y, v2.y );
	ymin = minimal( ymin, v3.y );

	float zmin = minimal( v1.z, v2.z );
	zmin = minimal( zmin, v3.z );


	float xmax = maximal( v1.x, v2.x );
	xmax = maximal( xmax, v3.x );

	float ymax = maximal( v1.y, v2.y );
	ymax = maximal( ymax, v3.y );

	float zmax = maximal( v1.z, v2.z );
	zmax = maximal( zmax, v3.z );

	assert( xmax - xmin >= 0 );
	assert( ymax - ymin >= 0 );
	assert( zmax - zmin >= 0 );

	return TVec3( xmax-xmin, ymax-ymin, zmax-zmin );

} // getTriangleLocalBounds


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


// reflect to the n.dot(dir) > 0 - makes refraction effect!
// only if n.dot(dir) <= 0 it takes origin sence!
//http://users.skynet.be/bdegreve/writings/reflection_transmission.pdf
template< typename TVec >
__host__ __device__ inline TVec
reflect(const TVec& dir, const TVec& normal)
{
	TVec temp = normal * ( dir.dot(normal) * (-2.f) );
	temp += dir;
	temp.voidNormalize(); 

	return temp;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template< typename TVec >
__host__ __device__ inline float
vecsCosAngle(const TVec& v1, const TVec& v2 )
{
	return v1.dot( v2 ) / sqrt( v1.magnitudeSquared() * v2.magnitudeSquared() );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__device__ inline float devRand_0_p1f(curandState* state)
{
	return curand_uniform( state );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__device__ inline float devRand_m1_p1f(curandState* state)
{
	return -1.f + curand_uniform( state )*2.f;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__device__ inline float devRandf(curandState* state, float min, float max)
{
	return min + curand_uniform( state ) * (max-min) ;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__host__ inline int32 rand( int32 max )
{
	return ::rand() % max;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__host__ inline int32 rand( int32 min, int32 max )
{
	return ( ::rand() % (max-min) ) + min;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__host__ inline float randf01()
{
	return (float) ::rand() / (float) RAND_MAX;
}

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__host__ inline float randf( float min, float max )
{
	return min + randf01() * (max-min) ;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__host__ inline float randf_11()
{
	return 1.f - randf01() * 2.f;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


//check-value exists in bounds ( check in [left;right] )
template< typename T >
__host__ __device__ bool inline
inSegment( T left, T right, T check )
{
	assert( left <= right );
	return check >= left && check <= right;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


//http://users.skynet.be/bdegreve/writings/reflection_transmission.pdf
template< typename TVec >
__host__ __device__ inline TVec
refract(const TVec& dir, const TVec& normal, float ior)
{
	float cos_phi_i = - ( dir.dot(normal) );
	float sin_phi_t_2 = ior*ior * ( 1 - cos_phi_i * cos_phi_i );

	TVec temp = ior * dir + normal * ( ior * cos_phi_i - sqrt( 1 - sin_phi_t_2 ) );
	temp.voidNormalize(); 

	return temp;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

template < typename T >
__host__ __device__ inline float returnCorrectDivider( T value )
{

	if ( abs( value ) == 0 )
		return float( 0.000000001f );
	else
		return float( value );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template < typename TVec3 >
__device__ inline float magnitudeXZSquared( const TVec3& vec )
{
	return vec.x * vec.x  +  vec.z * vec.z;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template < typename TVec3 >
__host__ __device__ inline float magnitudeXZ( const TVec3& vec )
{
	return sqrt( magnitudeXZSquared( vec ) );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template< typename TVec3 >
__device__ inline void normalizeXZ( TVec3 & vec )
{
	float n = 1.f / returnCorrectDivider( sqrt( magnitudeXZSquared( vec ) ) );
	vec.x *= n;
	vec.z *= n;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template < typename TVec3 >
__device__ __inline float getXZ_YAngle( const TVec3 & v )
{
	float xzVector = returnCorrectDivider( magnitudeXZ( v ) );
	if( xzVector < 0.000001f )
	{
		if( v.y < 0 )
			return Pi;
		else
			return 0;
	}

	float val = fabs( ::atan( fabs(v.y) / xzVector ) ); //[0..�/2]
	if( v.y < 0 )
		val += HalfPi;
	
	if( v.x < 0 )
		return Pi + val;
	else
		return val;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template < typename TVec3 >
__device__ __inline float getXZAngle( const TVec3 & v )
{
	float xzVector = magnitudeXZ( v );
	if( xzVector < 0.00001f )
		return 0;
		
	float x = v.x / xzVector;
	float z = v.z / xzVector;

	float f = ::atan( x / z );
	if( f < 0 )
		f = -f + HalfPi;
	
	if( x < 0 )
		return f + Pi;
	else
		return f;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/



