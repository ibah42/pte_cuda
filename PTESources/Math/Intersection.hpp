#pragma once

#include "PTESources/CudaInclude.hpp"

#include "PTESources/Math/Utility.hpp"
#include "PTESources/Math/Geometry.hpp"
#include "PTESources/Math/Vector3.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

//__device__
class Intersection
{
public:

	__device__ Intersection()
	{
		type = ContactType::undef;
		normalExtraction = ( &Intersection::calculatedPNandReturn );
	}

	__device__ void set( const Intersection& that );

	struct ContactType
	{
		enum Enum
		{
				disjoint	=  0	//  disjoint (no intersect)
			,	degenerate	=  1	//	triangle is degenerate ( a segment or point )
			,	onePoint	=  2	//  intersect in unique point 
			,	samePlane	=  3	//  both are in the same plane
			,	undef		=  4
			,	culled		=  5
		};
	};

	__device__ void setBestIsec( float maxQuadDist, const Intersection& that );
	
	__device__ static void intersect( Intersection& out, const Ray& ray, const BoundedTriangle& t );
	
	__device__ static bool intersect( const Ray& ray, const AABB& box, float& isecDist );
	__device__ static bool intersect( const AABB& b1, const AABB& b2 );

	
	__device__ bool hasOnePointContact() const { return type == ContactType::onePoint; }
	__device__ void resetType() { type = ContactType::undef; }

	__device__ const Vec3& getPointNormal() const { return (this->*normalExtraction)(); }

private:

	__device__ static void intersect( Intersection& out, const Ray& ray, const Triangle& t );

public:

	float Up0, Vp1, Wp2;
	ContactType::Enum type;
	Vec3 impactNorm;
	Vec3 contact;
	const BoundedTriangle* hitTriangle;
	float quadDistance;

private:

	typedef const Vec3& (Intersection::*normalExtractionFunc)() const;

	__device__ const Vec3& getCalculatedPN() const{ return pointNormal; }
	__device__ const Vec3& calculatedPNandReturn() const;

private:
	
	mutable normalExtractionFunc normalExtraction;
	mutable Vec3 pointNormal;

}; // class Intersection



/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__device__ void
Intersection::set( const Intersection& that )
{	
	Up0 = that.Up0;
	Vp1 = that.Vp1;
	Wp2 = that.Wp2;
	type = that.type;
	impactNorm = that.impactNorm;
	contact = that.contact;
	hitTriangle = that.hitTriangle;
	quadDistance = that.quadDistance;
	normalExtraction = that.normalExtraction;
	pointNormal = that.pointNormal;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__device__ const Vec3&
Intersection::calculatedPNandReturn() const
{
	assert( hitTriangle && hasOnePointContact() );

	int32 v0 = ( impactNorm.dot( hitTriangle->trig.n0 ) >= 0 ) & 1;
	int32 nv0 = v0 ^ 1;

	int32 v1 = ( impactNorm.dot( hitTriangle->trig.n1 ) >= 0 ) & 1;
	int32 nv1 = v1 ^ 1;

	int32 v2 = ( impactNorm.dot( hitTriangle->trig.n2 ) >= 0 ) & 1;
	int32 nv2 = v2 ^ 1;

	assert( v0 + nv0 == 1 && v1 + nv1 == 1 && v2 + nv2 == 1 );
	
	pointNormal += v0  * Up0 * hitTriangle->trig.n0;
	pointNormal -= nv0 * Up0 * hitTriangle->trig.n0;

	pointNormal += v1  * Vp1 * hitTriangle->trig.n1;
	pointNormal -= nv1 * Vp1 * hitTriangle->trig.n1;

	pointNormal += v2  * Wp2 * hitTriangle->trig.n2;
	pointNormal -= nv2 * Wp2 * hitTriangle->trig.n2;

	pointNormal.voidNormalize();
	normalExtraction = &Intersection::getCalculatedPN;
	return pointNormal;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__device__ void 
Intersection::setBestIsec( float maxQuadDist, const Intersection& that )
{
	if ( !that.hasOnePointContact() )
		return;

	if ( !hasOnePointContact() )
	{
		set( that );
		return;
	}

 	if ( that.quadDistance <= maxQuadDist && quadDistance > that.quadDistance )
 		set( that );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__device__ void
Intersection::intersect( Intersection& out, const Ray& ray, const Triangle& trig )
{
	//http://geomalgorithms.com/a06-_intersect-2.html

	Vec3		u, v, triangleNormal;	// triangle vectors
	Vec3		w0, w;					// ray vectors
	float		a, b;					// params to calc ray-plane intersect

	// get triangle edge vectors and plane normal

	Vec3 tp0 = trig.p0;

	u = trig.p1 - tp0;
	v = trig.p2 - tp0;
	
	out.type = ContactType::disjoint;

	triangleNormal = u.cross( v );
	if ( triangleNormal.isZero() )      
	{
		out.type = ContactType::degenerate;
		return;
	}

	w0 = ray.origin - tp0;
	a = - triangleNormal.dot( w0 );
	b = triangleNormal.dot( ray.dir );

	if ( fabs( b ) < 0 )  // ray is  parallel to triangle plane
	{   
// 		if ( a == 0 )
// 			out.mType = ContactType::samePlane;
// 		else
// 			out.mType = ContactType::disjoint;
		return;
	}

	// get intersect point of ray with triangle plane
	// for a segment, also test if (r > 1.0) => no intersect
	float r = a / b;
	if ( r <= 0 )	// ray goes away from triangle
	{
		return;
	}
	
	out.contact = ray.origin + r * ray.dir; // intersect point of ray and plane

	// is I inside T?
	float    uu, uv, vv, wu, wv, D;

	uu = u.dot( u );
	uv = u.dot( v );
	vv = v.dot( v );
	w  = out.contact - tp0;
	wu = w.dot( u );
	wv = w.dot( v );
	D  = uv * uv - uu * vv;

	// get and test parametric coords
	float s, t;
	s = ( uv * wv - vv * wu ) / D;
	if ( s < 0 || s > 1 )			// I is outside T
	{
		return;
	}

	t = ( uv * wu - uu * wv ) / D;
	if ( t < 0 || (s + t) > 1 )		// I is outside T
	{
		return;
	}


	Vec3 trueNormal = trig.trueNormal;
	if ( ray.dir.dot( trueNormal ) >= 0 )
		out.impactNorm = - trueNormal;
	else
		out.impactNorm =   trueNormal;

	out.Up0 = 1 - s - t;
	out.Vp1 = s;
	out.Wp2 = t;

	out.type = ContactType::onePoint;
	out.quadDistance = ( ray.origin - out.contact ).magnitudeSquared();

} // Intersection::intersect


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__device__ void
Intersection::intersect( Intersection& out, const Ray& ray, const BoundedTriangle& trig )
{
	float trash;
	if ( !intersect( ray, trig.box, trash ) )
	{
		out.type = ContactType::disjoint;
		return;
	}

	intersect( out, ray, trig.trig );
	out.hitTriangle = &trig;

} // Intersection::intersect


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__device__ bool
Intersection::intersect( const Ray &r, const AABB& box, float& isecDist )
{	
	//http://jcgt.org/published/0002/02/02/paper.pdf

	float tmin = 0;
	float tmax = 9E+35f;
	float txmin, txmax, tymin, tymax, tzmin, tzmax;
	txmin = (box.bounds[  r.sign[0] ].x - r.origin.x ) * r.invDir.x;
	txmax = (box.bounds[1-r.sign[0] ].x - r.origin.x ) * r.invDir.x;
		
	tymin = (box.bounds[  r.sign[1] ].y - r.origin.y ) * r.invDir.y;
	tymax = (box.bounds[1-r.sign[1] ].y - r.origin.y ) * r.invDir.y;

	tzmin = (box.bounds[  r.sign[2] ].z - r.origin.z ) * r.invDir.z;
	tzmax = (box.bounds[1-r.sign[2] ].z - r.origin.z ) * r.invDir.z;
	
	tmin = maximal( tzmin, maximal( tymin, maximal( txmin, tmin ) ) );
	tmax = minimal( tzmax, minimal( tymax, minimal( txmax, tmax ) ) );
	
	tmax *= 1.00000024f;

	isecDist = tmin;
	if ( tmin <= tmax )
	{
		isecDist = tmin;
		return true;
	}
	else
	{
		isecDist = 9E+35f;
		return false;
	}


//  	float tmin = 0, tmax = 9E+30f;
//  	float lo = r.invDir.x*(box.min().x - r.origin.x);
//  	float hi = r.invDir.x*(box.max().x - r.origin.x);
//  
//  	tmin = minimal(lo, hi);
//  	tmax = maximal(lo, hi);
//  	
//  	float lo1 = r.invDir.y*(box.min().y - r.origin.y);
//  	float hi1 = r.invDir.y*(box.max().y - r.origin.y);
//  
//  	tmin = maximal(tmin, minimal(lo1, hi1));
//  	tmax = minimal(tmax, maximal(lo1, hi1));
//  	
//  	float lo2 = r.invDir.z*(box.min().z - r.origin.z);
//  	float hi2 = r.invDir.z*(box.max().z - r.origin.z);
//  
//  	tmin = maximal(tmin, minimal(lo2, hi2));
//  	tmax = minimal(tmax, maximal(lo2, hi2));
//  
//  	isecDist = tmin;
//  
//  	return (tmin <= tmax) && (tmax > 0);


// 	float tmin, tmax, tymin, tymax, tzmin, tzmax;
// 	tmin	= ( box.bounds[   r.sign[0] ].x - r.origin.x) * r.invDir.x;
// 	tmax	= ( box.bounds[ 1-r.sign[0] ].x - r.origin.x) * r.invDir.x;
// 	tymin	= ( box.bounds[   r.sign[1] ].y - r.origin.y) * r.invDir.y;
// 	tymax	= ( box.bounds[ 1-r.sign[1] ].y - r.origin.y) * r.invDir.y;
// 	
// 	if ( (tmin > tymax) || (tymin > tmax) )
// 		return false;
// 	
// 	if (tymin > tmin)
// 		tmin = tymin;
// 	
// 	if (tymax < tmax)
// 		tmax = tymax;
// 
// 	tzmin = ( box.bounds[   r.sign[2] ].z - r.origin.z) * r.invDir.z;
// 	tzmax = ( box.bounds[ 1-r.sign[2] ].z - r.origin.z) * r.invDir.z;
// 	
// 	if ( (tmin > tzmax) || (tzmin > tmax) )
// 		return false;
// 
// 	if (tzmin > tmin)
// 		tmin = tzmin;
// 	
// 	if (tzmax < tmax)
// 		tmax = tzmax;
// 
// 	//http://gamedev.stackexchange.com/questions/26958/unsure-how-to-decide-ray-intersection-interval
// 	// choose bounds
// 
// 	float t0 = 0.00001f;
// 	float t1 = 9E+20f;
// 
// 	isecDist = tmin;
// 	return tmin < t1 && tmax > t0;

} // Intersection::intersect


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


// __device__ bool
// Intersection::intersect( const AABB& b1, const AABB& b2 )
// {	
// 	//http://studiofreya.com/blog/3d-math-and-physics/simple-aabb-vs-aabb-collision-detection/s
// 
// 	volatile bool check = ( fabs(b1.center.x - b2.center.x) > (b1.halfSize.x + b2.halfSize.x) );
// 			
// 	check = check ||	  ( fabs(b1.center.y - b2.center.y) > (b1.halfSize.y + b2.halfSize.y) );
// 	
// 	check = check ||	  ( fabs(b1.center.z - b2.center.z) > (b1.halfSize.z + b2.halfSize.z) );
// 		
// 	return ! check;
// }


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
