
#include "PTESources/PCH.hpp"

#include "PTESources/CudaInclude.hpp"
#include "PTESources/Math/Utility.hpp"
#include "PTESources/RenderSystem/GeometryBuilders.hpp"
#include "PTESources/RenderSystem/HLBVH.cuh"
#include "PTESources/RenderSystem/UDTracer.cuh"
#include "PTESources/GLViewport_routines.hpp"


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

using namespace PTE;

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

void buildCornellBox();
void testRandomHLBVH( int maxTrigsPerTree );

std::vector< Triangle > inputSoftImage_StaticGeometry(
		const std::vector< Vec3 >& vertices, const std::vector< Vec3 >* normals,  const std::vector< Vec3 >* uvw
	,	const std::vector< int >& indexes, const int maxTrigs = 1024*1024*32
);


void testBuildMesh(const char* fileName)
{
	std::vector< Vec3 > outVertices;
	std::vector< int > outIndexes;

	std::vector <Vec3> outNormals; // three serial vec3 for each triangle
	std::vector <Vec3> outUVWs; // three serial vec3 for each triangle
	std::vector <LONG> outMatIDperFace; // resulted length is equal to number of polygons\triangles

	ReadGT4Topology( fileName, outVertices, outIndexes, outNormals, outUVWs, outMatIDperFace );
	std::vector< Triangle > array = inputSoftImage_StaticGeometry( outVertices, &outNormals, &outUVWs, outIndexes );

	std::cout<< "building test mesh: "<<fileName<<std::endl;
	TreeData tree;
	buildHLBVH( tree, array, 32 );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void inputSoftImageGeomMesh( Instance& instance, const char* fileName, const SurfaceMaterial& mat, int32 maxTrigs, float scale = 1, Vec3 disp=Vec3(0,0,0) )
{
	std::vector< Vec3 > outVertices;
	std::vector< int > outIndexes;

	std::vector <Vec3> outNormals; // three serial vec3 for each triangle
	std::vector <Vec3> outUVWs; // three serial vec3 for each triangle
	std::vector <LONG> outMatIDperFace; // resulted length is equal to number of polygons\triangles

	ReadGT4Topology( fileName, outVertices, outIndexes, outNormals, outUVWs, outMatIDperFace );
	
	instance.inputSoftImage_StaticGeometry( outVertices, outIndexes, &mat, &outNormals, &outUVWs,  scale, disp, maxTrigs );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


int main( int argc , char **argv )
{
	//srand (time(NULL));


	std::cout<< "Bounded trig size: " << sizeof(BoundedTriangle) << " AABB size: " << sizeof (AABB) << " Trig size: "<< sizeof(Triangle) << std::endl <<std::endl;
	
	int id = findCudaDevice(argc, (const char**)argv);
	
	try
	{	
		Camera camera;
		Instance instance;

		SphereEnvTex envMap( "c:\\Global repositories\\PTE_CUDA\\smap2.bmp" );
		RayTraceParams rayTraceParams( 2,14,15, 1,1,1, 1 );

		UDTracer tracer( 10050000000, &envMap, rayTraceParams, 1344 );

		gCamera = &camera;
		gTracer = &tracer;
		gInstance = &instance;
	
		/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

 		char * p_geometryModelPath = 0;
// 		p_geometryModelPath = "top2.ifxgt4";
// 		testBuildMesh(p_geometryModelPath);
// 
// 		p_geometryModelPath = "dragon1.ifxgt4";
// 		testBuildMesh(p_geometryModelPath);		
// 
// 		p_geometryModelPath = "xsiMan.ifxgt4";
// 		testBuildMesh(p_geometryModelPath);
// 
// 		p_geometryModelPath = "sponza_default.ifxgt4";
// 		testBuildMesh(p_geometryModelPath);
// 
	
// 
// 		p_geometryModelPath = "c:\\Global repositories\\PTE_CUDA\\1m_tree.ifxgt4";
// 		testBuildMesh(p_geometryModelPath);
// 
// 		p_geometryModelPath = "c:\\Global repositories\\PTE_CUDA\\BUNNY_MOTHER_MATKA1.ifxgt4";
// 		testBuildMesh(p_geometryModelPath);
	
 				
	//	testRandomHLBVH( 1024*1024*7 );

		Color c1(0.7f,0.7f,0.7f);
		Color c2(0.8,0.5f,0.3f);
		Color c3(0.05f,0.1f,0.7f);
		Color cSun(0.9f,0.8f,0.78f);
		EmitTextureUchar4 emit( ColorA( 1,1,1, 0 ), 0 );
		ColorTextureUchar4 simpleDiff( ColorA( 1,1,1, 1 ) );
		ColorTextureUchar4 diffuseTile1( ColorA( 1,1,1, 1 ) );
		NormalTextureVec3 normal1( "c:\\Global repositories\\PTE_CUDA\\normals.bmp" );

 		SurfaceMaterial mat( 0, &simpleDiff, &emit );
		mat.reflectK = 0;
 		mat.refractK = 0;
		mat.glossRefract = 0.99999;
		mat.glossReflect = 1;
 		mat.ior = 0.5;

		SurfaceMaterial mat1( 0, &emit, &emit );
		mat1.reflectK = 0;
		mat1.refractK = 1;
		mat1.glossRefract = 0.99999;
		mat1.glossReflect = 1;
		mat1.ior = 0.5;


// 		Triangle t( Vec3(-1000,-1000,0), Vec3(1000,-1000,0), Vec3(0,2000,0) );
// 		t.setUV( 0,0, 1,1, 0,1 );
// 		instance.addTrig( t, &mat );
// // 
// 		Triangle t1( Vec3(100,10,0), Vec3(100,10,100), Vec3(-100,10,100) );
// 		t1.setUV( 0,0, 1,1, 0,1 );
// 		instance.addTrig( t1, &mat );
//  		inputSoftImageGeomMesh(instance, "c:\\Global repositories\\PTE_CUDA\\buddha1.ifxgt4", mat, 4500000, 1, Vec3( 0,0,0 ) );
  		//inputSoftImageGeomMesh(instance, "c:\\Global repositories\\PTE_CUDA\\dragon1.ifxgt4", mat, 5000000, 2, Vec3( 0,-10,0 ) );
//  		inputSoftImageGeomMesh(instance, "c:\\Global repositories\\PTE_CUDA\\buddha1.ifxgt4", mat, 4500000, 1, Vec3( 40,0,0 ) );
  //		inputSoftImageGeomMesh(instance, "c:\\Global repositories\\PTE_CUDA\\buddha1.ifxgt4", mat1, 4500000, 3, Vec3( 0,10,0 ) );
  	//	inputSoftImageGeomMesh(instance, "c:\\Global repositories\\PTE_CUDA\\tree.ifxgt4", mat, 4500000, 0.1, Vec3( 0,-5,0 ) );
 	//	inputSoftImageGeomMesh(instance, "c:\\Global repositories\\PTE_CUDA\\cornellBox.ifxgt4", mat, 4500000, 1, Vec3( 0, -10,0 ) );
 //			inputSoftImageGeomMesh(instance, "c:\\Global repositories\\PTE_CUDA\\cornellBox.ifxgt4", mat, 4500000, 1, Vec3( 50, -10,0 ) );
// 			inputSoftImageGeomMesh(instance, "c:\\Global repositories\\PTE_CUDA\\cornellBox.ifxgt4", mat, 4500000, 1, Vec3( 100, -10,0 ) );
 	//		inputSoftImageGeomMesh(instance, "c:\\Global repositories\\PTE_CUDA\\tree.ifxgt4", mat, 4500000, 0.2, Vec3( 0, 50,0 ) );
	//	inputSoftImageGeomMesh(instance, "c:\\Global repositories\\PTE_CUDA\\BUNNY_MOTHER_MATKA1.ifxgt4", mat, 4500000, 1, Vec3( 0, -10,0 ) );
		buildCornellBox();
	//	inputSoftImageGeomMesh(instance, "c:\\Global repositories\\PTE_CUDA\\tree.ifxgt4", mat, 10000000, 1, Vec3( 0,0,0 ) );
		instance.buildAndExportGeometry(32);
		
		// init GLUT and create Window
		glutInit(&argc, argv);
		glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA |  GLUT_STENCIL);
		glutInitWindowPosition(200,200);
		glutInitWindowSize( GL::gPrefs.winSize_w, GL::gPrefs.winSize_h );
		glutCreateWindow("PTE"); 

		// register callbacks
		glutDisplayFunc(GL::Callbacks::DisplayScene);
		glutSpecialFunc(GL::Callbacks::SpecialKeys);
		glutKeyboardFunc(GL::Callbacks::RegularKeys);
		glutReshapeFunc(GL::Callbacks::ReshapeWindow); 
		glutReshapeWindow(GL::gPrefs.winSize_w, GL::gPrefs.winSize_h);
		glutMouseFunc(GL::Callbacks::MouseClickEvent) ;
		glutMotionFunc(GL::Callbacks::MouseDragEvent) ;
		//glutPassiveMotionFunc(GL::Callbacks::MousePureDragEvent) ; // in case of mouse moves without buttons pressed

		// init rest of data
		GL::Callbacks::InitFrame ();

	
		glutMainLoop();


	}
	catch( std::exception& e )
	{
		std::cout<<std::endl<<" std::exception arrived: "<<e.what();
	}
	catch( ... )
	{
		std::cout<<std::endl<<" undiagnosted exception arrived: ";
	}	

	//int a;
	//std::cin >> a;

	return 0;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void buildCornellBox()
{
	std::vector< Vec3 > outVertices;
	std::vector< int > outIndexes;

	std::vector <Vec3> outNormals; // three serial vec3 for each triangle
	std::vector <Vec3> outUVWs; // three serial vec3 for each triangle
	std::vector <LONG> outMatIDperFace; // resulted length is equal to number of polygons\triangles
	buildSphere( outVertices, outIndexes, outNormals, 100, 100, 8 );

	Color c1(0.7f,0.7f,0.7f);
	Color c2(0.8,0.5f,0.3f);
	Color c3(0.05f,0.1f,0.7f);
	Color cSun(0.9f,0.8f,0.78f);

	EmitTextureUchar4 *emit1 = new EmitTextureUchar4( ColorA( 1,0.8,1, 1 ), 5 );
	EmitTextureUchar4 *emit2 = new EmitTextureUchar4( ColorA( 1,0.3,0.2, 1 ), 1 );
	ColorTextureUchar4 *simpleDiff= new ColorTextureUchar4( ColorA( 0.5, 0.4, 0.5, 1 ) );
	ColorTextureUchar4 *diffuseTile1= new ColorTextureUchar4( "c:\\Global repositories\\PTE_CUDA\\tile1.bmp" );
	ColorTextureUchar4 *diffuseTile2= new ColorTextureUchar4( "c:\\Global repositories\\PTE_CUDA\\tile2.bmp" );
	ColorTextureUchar4 *diffuseTile3= new ColorTextureUchar4( "c:\\Global repositories\\PTE_CUDA\\tile3.bmp" );
	ColorTextureUchar4 *diffuseTile4= new ColorTextureUchar4( "c:\\Global repositories\\PTE_CUDA\\tile4.bmp" );
	ColorTextureUchar4 *diffuseTile5= new ColorTextureUchar4( "c:\\Global repositories\\PTE_CUDA\\tile5.bmp" );
	ColorTextureUchar4 *diffuseTransparent = new ColorTextureUchar4( ColorA( 1,1,1, 0 ) );
	EmitTextureUchar4 *emitTransparent = new EmitTextureUchar4( ColorA( 1,1,1, 0 ), 0 );
	NormalTextureVec3 *normal1 = new NormalTextureVec3( "c:\\Global repositories\\PTE_CUDA\\normals.bmp" );
	NormalTextureVec3 *normal2 = new NormalTextureVec3( "c:\\Global repositories\\PTE_CUDA\\normal2.bmp" );
	NormalTextureVec3 *normal3 = new NormalTextureVec3( "c:\\Global repositories\\PTE_CUDA\\normal3.bmp" );

	SurfaceMaterial* matTopBot = new SurfaceMaterial( normal1, diffuseTransparent, emit2  );
	matTopBot->refractK = 0;
	matTopBot->ior = 0.61;
	SurfaceMaterial* matBack  = new SurfaceMaterial  (normal2, diffuseTransparent, emit1 );
	matBack->refractK = 0;
	matBack->ior = 0.1f;

	SurfaceMaterial* matFront  = new SurfaceMaterial (normal1, diffuseTile4, emitTransparent );
	SurfaceMaterial* matLeftRight = new SurfaceMaterial(0/*normal1*/, diffuseTile2, emitTransparent);
	SurfaceMaterial* matTopLight = new SurfaceMaterial (0/*normal1*/, diffuseTile3, emitTransparent );
	float eps = 0.1f;

	float planeSize = 50;
	float lightSize = 50;
	float k = 40;

	SurfaceMaterial* matSphere = new SurfaceMaterial (0/*normal1*/, diffuseTransparent, emitTransparent );
	matSphere->refractK = 1;
	matSphere->ior = 0.0;
	gInstance->inputSoftImage_StaticGeometry( outVertices, outIndexes, matSphere, &outNormals, 0,  3, Vec3(0, 25,0) );
	gInstance->inputSoftImage_StaticGeometry( outVertices, outIndexes, matSphere, &outNormals, 0,  2, Vec3(30, 28,30) );
	gInstance->inputSoftImage_StaticGeometry( outVertices, outIndexes, matSphere, &outNormals, 0,  1, Vec3(30, 10,30) );
	gInstance->inputSoftImage_StaticGeometry( outVertices, outIndexes, matSphere, &outNormals, 0,  3, Vec3(-30, 2,-30) );

// 	gInstance->addFlatQuad(
// 			Vec3(-lightSize, +planeSize - eps, -lightSize)
// 		,	Vec3(-lightSize, +planeSize - eps, lightSize)
// 		,	Vec3( lightSize, +planeSize - eps, lightSize)
// 		,	Vec3( lightSize, +planeSize - eps, -lightSize)	
// 		,	matTopBot
// 		);


	gInstance->addFlatQuad(
			Vec3(-planeSize, -planeSize + eps, -planeSize)
		,	Vec3(-planeSize, -planeSize + eps, planeSize)
		,	Vec3( planeSize, -planeSize + eps, planeSize)
		,	Vec3( planeSize, -planeSize + eps, -planeSize)	
		,	matBack
		);

 	gInstance->addFlatQuad(
 			Vec3(-planeSize, +planeSize - eps, -planeSize)
 		,	Vec3(-planeSize, +planeSize - eps, planeSize)
 		,	Vec3( planeSize, +planeSize - eps, planeSize)
 		,	Vec3( planeSize, +planeSize - eps, -planeSize)	
 		,	matLeftRight
 	);


	gInstance->addFlatQuad(
		Vec3(-planeSize, -planeSize , -planeSize + eps )
		,	Vec3(-planeSize, planeSize  , -planeSize + eps )
		,	Vec3( planeSize, planeSize  , -planeSize + eps )
		,	Vec3( planeSize, -planeSize , -planeSize + eps )	
		,	matFront
		);

	gInstance->addFlatQuad(
		Vec3(-planeSize, -planeSize , +planeSize - eps )
		,	Vec3(-planeSize, planeSize  , +planeSize - eps )
		,	Vec3( planeSize, planeSize  , +planeSize - eps )
		,	Vec3( planeSize, -planeSize , +planeSize - eps )	
		,	matLeftRight
		);
// 
	gInstance->addFlatQuad(
		Vec3(-planeSize + eps, -planeSize,-planeSize)
		,	Vec3(-planeSize + eps,-planeSize, planeSize)
		,	Vec3(-planeSize + eps, planeSize, planeSize)
		,	Vec3(-planeSize + eps, planeSize,-planeSize)	
		,	matFront
		);

	// 		gInstance->addFlatQuad(
	// 				Vec3( 30,-planeSize*2,-planeSize*2)
	// 			,	Vec3( 30,-planeSize*2, planeSize*2)
	// 			,	Vec3( 30, planeSize*2, planeSize*2)
	// 			,	Vec3( 30, planeSize*2,-planeSize*2)	
	// 			,	matBack
	// 		);

	/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

}

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


std::vector< Triangle > inputSoftImage_StaticGeometry(
		const std::vector< Vec3 >& vertices, const std::vector< Vec3 >* normals,  const std::vector< Vec3 >* uvw
	,	const std::vector< int >& indexes, const int maxTrigs
)
{
	std::vector< Triangle > triangles;
	CPU_PTE_ASSERT( indexes.size() >= 4, __FILE__, __LINE__ );
	CPU_PTE_ASSERT( vertices.size() >= 3, __FILE__, __LINE__ );
	if ( normals )
		CPU_PTE_ASSERT( normals->size() >= 3, __FILE__, __LINE__ );

	int currId = 0;
	int currentTrigId = 0;
	int currentNormalId = 0;
	int currentUVWId = 0;
	while( maxTrigs > triangles.size() )
	{
		if ( currId >= indexes.size() )
			return triangles;

		Vec3 v0 = vertices.at( indexes[currId] );
		Vec3 v1 = vertices.at( indexes[currId+1] );
		Vec3 v2 = vertices.at( indexes[currId+2] ) ;
		
		if ( ( v2-v0 ).cross( v1-v0 ).magnitudeSquared() <= 0 )
		{
			currId += 4;
			currentNormalId += 3;
			++currentTrigId;
			continue;
		}
		
		Triangle t( v0, v1, v2 );

		if ( normals )
		{
			t.n0 = normals->at( currentNormalId   );
			t.n1 = normals->at( currentNormalId+1 );
			t.n2 = normals->at( currentNormalId+2 );	
		}

		if ( uvw )
		{
			t.u[0] = uvw->at( currentUVWId   ).x;
			t.u[1] = uvw->at( currentUVWId+1 ).x;
			t.u[2] = uvw->at( currentUVWId+2 ).x;

			t.v[0] = uvw->at( currentUVWId   ).y;
			t.v[1] = uvw->at( currentUVWId+1 ).y;
			t.v[2] = uvw->at( currentUVWId+2 ).y;
		}
		else
		{
			t.u[0] = 0;	t.u[1] = 0;	t.u[2] = 0;
			t.v[0] = 0;	t.v[1] = 0;	t.v[2] = 0;
		}

	//	t.material = 0;

		triangles.push_back( Triangle( t ) );
		int32 index = triangles.size() - 1;

		if ( t.getSquare() <= 0 )
			throw std::exception("DEGENERATE triangle in inputSoftImage_StaticGeometry have come!");

		currId += 4;
		currentNormalId += 3;
		++currentTrigId;
	}

	return triangles;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void testRandomHLBVH( int maxTrigsPerTree )
{
	MCodes cpu1[15] = { 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,1 };
	RLEtest( cpu1, 15, 32 );

	MCodes cpu2[7] = { 0,0,0,0,0, 0,1 };
	RLEtest( cpu2, 7, 32 );

	MCodes cpu3[17] = { 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,1 };
	RLEtest( cpu3, 17, 32 );

	MCodes cpu4[15] = { 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15 };
	RLEtest( cpu4, 15, 32 );

	MCodes cpu5[17] = { 0,1,1,2,2, 2,3,3,3,3, 4,4,4,5,5, 6,7 };
	RLEtest( cpu5, 17, 32 );

	int32 count = 2;
	while(1)
	{	
		std::vector< Triangle > triangles;
		count *= 1.5;

		if ( count > maxTrigsPerTree )
		{
			std::cout<< "\n\n\n\nTESTING FINISHED.\n\n\n";
			return;
		}

		std::cout<<"\n\n\n\t\t NEXT TEST BEGAN... \t\t\n";
		DWORD t1 = timeGetTime();

		int32 deviation = count/100+1;
		int countTrigs = count;
		for ( int i=0; i<countTrigs; i++ )
		{
			Vec3 v1( rand(-10,10),rand(-10,10),rand(-10,10) );
			Vec3 v2( rand(-10,10),rand(-10,10),rand(-10,10) );
			Vec3 v3( rand(-10,10),rand(-10,10),rand(-10,10) );
			v1*=0.1;
			v2*=0.1;
			v3*=0.1;

			Vec3 randTrans ( rand(-deviation,deviation),rand(-deviation,deviation),rand(-deviation,deviation) );
			v1+=randTrans;
			v2+=randTrans;
			v3+=randTrans;
			Vec3 trueNormal = ( v1 - v2 ).cross( v1 - v3 );
			if ( trueNormal.magnitude() <= 0 )
			{
				++countTrigs;
			}
			else
			{			
				triangles.push_back( Triangle( v1,v2,v3 ) );
			}

			if ( countTrigs > count * 10 )
			{
				std::cout<<"too much degenerate in random gen tests skipped!\n";
				break;
			}
		}
		
		DWORD t2 = timeGetTime();
		std::cout<<"tree generation time is: \t\t\t" << 0.001f*(t2-t1) << " S" <<std::endl;
		
		TreeData tree;
		buildHLBVH( tree, triangles, 32 );
	}
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/