#pragma once

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

using namespace PTE;

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

void
buildSphere( std::vector< Vec3 >& outVertices, std::vector< int >& outIndexes, std::vector< Vec3 >& outNormals, 
const float verticalSegs, const float horizontalSegs, const float radius )
{
	outVertices.clear();
	outIndexes.clear();

	float verticalSegments = clamp ( 2, 500, verticalSegs );
	float horizontalSegments = clamp ( 2, 500, horizontalSegs );

	// resize
	int carryVrtx=0; 
	int nbVertices = ((verticalSegments - 1) * horizontalSegments) + 2;
	outVertices.reserve ( nbVertices );
	for( int i=0; i<nbVertices; i++ )
	{
		outVertices.push_back(Vec3(0));
	}		

	// put bottom vertex
	outVertices[carryVrtx++].set (0.f , -radius, 0.f ) ;

	// create rings of vertices at progressively higher latitudes
	for (int i = 0; i < verticalSegments - 1; i++)
	{
		float latitude = ((i + 1) * Pi / verticalSegments) - Pi/2;
		float dy = (float)sin(latitude);
		float dxz = (float)cos(latitude);
		// Create a single ring of vertices at this latitude.
		for (int j = 0; j < horizontalSegments; j++)
		{
			float longitude = j * TwoPi / horizontalSegments;
			float dx = (float)cos(longitude) * dxz;
			float dz = (float)sin(longitude) * dxz;
			// Vec3 normal = new Vec3(dx, dy, dz);
			outVertices[carryVrtx++].set(dx * radius , dy * radius ,dz * radius);
		}
	};

	// put top vertex
	outVertices[carryVrtx++].set (0.f , radius, 0.f );


	// fill description
	int nbIndices = (horizontalSegments*8) + ((verticalSegments - 2)*horizontalSegments)*8;
	int carryIdx=0;

	outIndexes.reserve ( nbIndices );
	for( int i=0; i<nbIndices; i++ )
	{
		outIndexes.push_back(0);
	}		

	for (int i = 0; i < horizontalSegments; i++)
	{
		outIndexes[carryIdx++]=(1 + i); 
		outIndexes[carryIdx++]=(1 + (i + 1) % (int)horizontalSegments);
		outIndexes[carryIdx++]=(0);
		outIndexes[carryIdx++]=(-2);
	};
	for (int i = 0; i < verticalSegments - 2; i++)
	{
		for (int j = 0; j < horizontalSegments; j++)
		{
			int nextI = i + 1;
			int nextJ = (j + 1) %(int) horizontalSegments;

			outIndexes[carryIdx++]=(1 + nextI * horizontalSegments + j); 
			outIndexes[carryIdx++]=(1 + i * horizontalSegments + nextJ);
			outIndexes[carryIdx++]=(1 + i * horizontalSegments + j);
			outIndexes[carryIdx++]=(-2);

			outIndexes[carryIdx++]=(1 + nextI * horizontalSegments + j); 
			outIndexes[carryIdx++]=(1 + nextI * horizontalSegments + nextJ);
			outIndexes[carryIdx++]=(1 + i * horizontalSegments + nextJ);

			outIndexes[carryIdx++]=(-2);
		}
	}
	int V_num = ( (verticalSegments - 1) * horizontalSegments ) + 2 ;

	//Create a fan connecting the top vertex to the top latitude ring.
	for (int i = 0; i < horizontalSegments; i++)
	{
		outIndexes[carryIdx++]=(V_num - 2 - i);
		outIndexes[carryIdx++]=(V_num - 2 - (i + 1) % (int)horizontalSegments);
		outIndexes[carryIdx++]=(V_num - 1);

		outIndexes[carryIdx++]=(-2);
	};


	// create smooth normals and spherical uv
	LONG nbSamples = outIndexes.size ()/4 * 3;
	LONG nbTriangles = outIndexes.size ()/4;
	outNormals.resize ( nbSamples );


	for ( LONG triIt=0, carryIdx=0; triIt<nbTriangles; ++triIt )
	{
		outNormals[triIt*3] = outVertices[ outIndexes[triIt*4] ].getNormalized (); 
		outNormals[triIt*3+1] = outVertices[ outIndexes[triIt*4+1] ].getNormalized (); 
		outNormals[triIt*3+2] = outVertices[ outIndexes[triIt*4+2] ].getNormalized (); 
	};

}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void ReadGT4Topology (
		const char * in_fileFullPathName
	,	std::vector< Vec3 >& outVertices
	,	std::vector< int >& outIndexes
	,	std::vector <Vec3> & outNormals
	,	std::vector <Vec3> & outUVWs
	,	std::vector <LONG> & outMatIDperTriangle
)
{
	outVertices.clear();
	outIndexes.clear();

	FILE * p_file = 0;
	char path[1000];
	GetCurrentDirectory( 1000, path );
	std::string str = path;
	str += "\\";
	str += in_fileFullPathName;

	fopen_s( &p_file, in_fileFullPathName, "rb" );
	if( !p_file )
	{
		fopen_s ( &p_file, str.c_str(), "rb" );
	}

	if ( !p_file )
	{
		std::cout << "[PTE][IO]: Unable to find or open file for data reading!" << std::endl;
		std::cout << str<<std::endl;
		//Application().LogMessage ( L"[IFX][IO]: Unable to find or open file for data reading!" ,siErrorMsg );
		return;
	};

	ULONG fileHead [] = {0,0};
	fread ( (void*)(&fileHead), 1, sizeof(ULONG)*2, p_file ); // get the two first bytes, 0=GT version and 1=global data size

	//if ( FileHead[0] != IFX_GT4_CURRENT_VERSION )
	//{
	//	Application().LogMessage ( L"Cached Gtopo build " + CString(FileHead[1]) + L" is incompatible with the actual build!" , siErrorMsg );
	//	fclose ( pFile );
	//	return false;
	//};

	if ( !fileHead[1] )
	{
		//Application().LogMessage ( L"[IFX][IO]: Wrong or empty file, unable to read data!" , siErrorMsg );
		std::cout << "[PTE][IO]: Wrong or empty file, unable to read data!" << std::endl;
		fclose ( p_file );
		return;
	};

	// read in buffer 
	void * p_dataOut = malloc ( fileHead[1] ); //out_dataBuff.Resize ( 0, FileHead[1] );
	fseek(p_file, 0L, SEEK_SET);
	fread ( p_dataOut, 1, fileHead[1], p_file );

	fclose ( p_file );


	// ######################################################################################
	// decode topology
	ULONG	m_GTHead = ((ULONG*)p_dataOut)[0];
	ULONG	m_StorageSizeInBytes = ((ULONG*)p_dataOut)[1];
	ULONG	m_nbVertices = ((ULONG*)p_dataOut)[2]; 
	ULONG	m_nbIndices = ((ULONG*)p_dataOut)[3]; 
	ULONG	m_nbSamples = ((ULONG*)p_dataOut)[4];
	ULONG	m_nbFaces = ((ULONG*)p_dataOut)[5]; 
	ULONG	m_nbIslands = ((ULONG*)p_dataOut)[6];
	LONG	m_MaterialsStorageBlockSizeInBytes = ((ULONG*)p_dataOut)[7];
	ULONG	m_ReservationSizeInBytes = ((ULONG*)p_dataOut)[8];
	LONG	m_lastOp = (((LONG*)p_dataOut)[9]);
	float	m_lastOpEvalTime = ((float*)p_dataOut)[10];
	ULONG	m_nbMaterials = ((ULONG*)p_dataOut)[11];

	// pointers to sub-data
	// per-vertex
	Vec3 * m_pPPOS; 
	Vec3 * m_pVELOCITY; // reserved for per-point deformations
	ULONG * m_pVERT_ISL_ID;
	// per-sample
	Vec3 * m_pUVW;
	Vec3 * m_pNORMALS; 
	// per-index
	LONG * m_pINDICES;
	// per-face
	LONG * m_pFACE_ID;
	LONG * m_pFACE_USERDATA;
	ULONG * m_pFACE_KNIFE_ID;
	ULONG * m_pFACECAPACITY;
	ULONG * m_pLEGACY_POLYISL_ID;
	ULONG * m_pPOLYISL_ID;
	// per-island
	Vec3 * m_pAABB_MIN;
	Vec3 * m_pAABB_MID;
	Vec3 * m_pAABB_MAX;
	Vec3 * m_pLIN_VEL;
	Vec3 * m_pANG_VEL;
	Vec3 * m_pPHYS_PROPS; // mass|bounce|friction
	Vec3 * m_pCOM; // center of mass
	ULONG * m_pNB_ISL_VERTICES;	
	ULONG * m_pNB_ISL_INDICES;
	ULONG * m_pNB_ISL_FACES;
	ULONG * m_pUPD_ISL_LIST;
	ULONG * m_pLEG_ISL_LIST;

	float * m_pAREAS;
	float * m_pVOLUMES;
	bool * m_pOPEN_ISL;
	bool * m_pNEG_ISL;
	// other
	char * m_pMATERIALS;
	void * m_RESERVATION;

	ULONG _offset = 0;
	_offset += sizeof(ULONG)*12;

	// set point positions
	m_pPPOS = (Vec3*)((char*)p_dataOut + _offset);
	_offset += sizeof(Vec3)*m_nbVertices;
	// set indices
	m_pINDICES = (LONG*)((char*)p_dataOut + _offset);
	_offset += sizeof(LONG)*m_nbIndices;
	// set uvw samples
	m_pUVW = (Vec3*)((char*)p_dataOut + _offset);
	_offset += sizeof(Vec3)*m_nbSamples;
	// set normals
	m_pNORMALS = (Vec3*)((char*)p_dataOut + _offset);
	_offset += sizeof(Vec3)*m_nbSamples;
	// set point velocity
	m_pVELOCITY = (Vec3*)((char*)p_dataOut + _offset);
	_offset += sizeof(Vec3)*m_nbVertices;
	// set point isl ID
	m_pVERT_ISL_ID = (ULONG*)((char*)p_dataOut + _offset);
	_offset += sizeof(ULONG)*m_nbVertices;
	// set faces IDs
	m_pFACE_ID = (LONG*)((char*)p_dataOut + _offset);
	_offset += sizeof(LONG)*m_nbFaces;
	// set faces user data
	m_pFACE_USERDATA = (LONG*)((char*)p_dataOut + _offset);
	_offset += sizeof(LONG)*m_nbFaces;
	// set faces knife ID
	m_pFACE_KNIFE_ID = (ULONG*)((char*)p_dataOut + _offset);
	_offset += sizeof(ULONG)*m_nbFaces;
	// set face's vertices number
	m_pFACECAPACITY = (ULONG*)((char*)p_dataOut + _offset);
	_offset += sizeof(ULONG)*m_nbFaces;
	// set face legacy island id
	m_pLEGACY_POLYISL_ID = (ULONG*)((char*)p_dataOut + _offset);
	_offset += sizeof(ULONG)*m_nbFaces;
	// set face updated island id
	m_pPOLYISL_ID = (ULONG*)((char*)p_dataOut + _offset);
	_offset += sizeof(ULONG)*m_nbFaces;
	// set islands bboxes
	m_pAABB_MIN = (Vec3*)((char*)p_dataOut + _offset);
	_offset += sizeof(Vec3)*m_nbIslands;	
	m_pAABB_MID = (Vec3*)((char*)p_dataOut + _offset);
	_offset += sizeof(Vec3)*m_nbIslands;
	m_pAABB_MAX = (Vec3*)((char*)p_dataOut + _offset);
	_offset += sizeof(Vec3)*m_nbIslands;
	// set volume of islands
	m_pVOLUMES = (float*)((char*)p_dataOut + _offset);
	_offset += sizeof(float)*m_nbIslands;
	// set area of islands
	m_pAREAS = (float*)((char*)p_dataOut + _offset);
	_offset += sizeof(float)*m_nbIslands;
	// set island openess
	m_pOPEN_ISL = (bool*)((char*)p_dataOut + _offset);
	_offset += sizeof(bool)*m_nbIslands;
	// set island orientation
	m_pNEG_ISL = (bool*)((char*)p_dataOut + _offset);
	_offset += sizeof(bool)*m_nbIslands;
	// set island LinVel
	m_pLIN_VEL = (Vec3*)((char*)p_dataOut + _offset);
	_offset += sizeof(Vec3)*m_nbIslands;
	// set island AngVel
	m_pANG_VEL = (Vec3*)((char*)p_dataOut + _offset);
	_offset += sizeof(Vec3)*m_nbIslands;
	// set island phys mat
	m_pPHYS_PROPS = (Vec3*)((char*)p_dataOut + _offset); 
	_offset += sizeof(Vec3)*m_nbIslands;
	// set island center of mass
	m_pCOM = (Vec3*)((char*)p_dataOut + _offset); 
	_offset += sizeof(Vec3)*m_nbIslands;
	// set island vertex nb
	m_pNB_ISL_VERTICES = (ULONG*)((char*)p_dataOut + _offset); 
	_offset += sizeof(ULONG)*m_nbIslands;
	// set island indices nb
	m_pNB_ISL_INDICES = (ULONG*)((char*)p_dataOut + _offset); 
	_offset += sizeof(ULONG)*m_nbIslands;
	// set faces vertex nb
	m_pNB_ISL_FACES = (ULONG*)((char*)p_dataOut + _offset); 
	_offset += sizeof(ULONG)*m_nbIslands;
	// set islands actual IDs 
	m_pUPD_ISL_LIST = (ULONG*)((char*)p_dataOut + _offset); 
	_offset += sizeof(ULONG)*m_nbIslands;
	// set islands legacy IDs
	m_pLEG_ISL_LIST = (ULONG*)((char*)p_dataOut + _offset); 
	_offset += sizeof(ULONG)*m_nbIslands;
	// set materials
	m_pMATERIALS = ((char*)p_dataOut + _offset);
	_offset += sizeof(char)*m_MaterialsStorageBlockSizeInBytes;
	// set reserved data
	m_RESERVATION = ((char*)p_dataOut + _offset);
	_offset += sizeof(char)*m_ReservationSizeInBytes;


	// fill out geometry decription
	outVertices.resize ( m_nbVertices );
	outIndexes.resize ( m_nbIndices );
	
	outNormals.resize ( m_nbSamples ); 
	outUVWs.resize ( m_nbSamples ); 
	outMatIDperTriangle.resize ( m_nbFaces ); 


	memcpy ( &(outVertices[0]), &(m_pPPOS[0]), sizeof(Vec3)*m_nbVertices ) ;
	memcpy ( &(outIndexes[0]), &(m_pINDICES[0]), sizeof(ULONG)*m_nbIndices );

	memcpy ( &(outNormals[0]), &(m_pNORMALS[0]), sizeof(Vec3)*m_nbSamples );
	memcpy ( &(outUVWs[0]), &(m_pUVW[0]), sizeof(Vec3)*m_nbSamples );

	memcpy ( &(outMatIDperTriangle[0]), &(m_pFACE_ID[0]), sizeof(LONG)*m_nbFaces );

	free ( p_dataOut ); 
	return ;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/