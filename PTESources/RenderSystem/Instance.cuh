#pragma once

#include "PTESources/CudaInclude.hpp"
#include "PTESources/RenderSystem/Light.hpp"
#include "PTESources/RenderSystem/HLBVH.cuh"
#include "PTESources/RenderSystem/Material.cuh"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

	
struct DevInstData
{
	Light* lights;
	int32 lightsCount;

	SurfaceMaterial* mats;
	int32 matsCount;
};


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


class Instance
{
public:

	Instance()
	{
		devInstData.lights = 0;
		devInstData.lightsCount = 0;

		devInstData.mats = 0;
		devInstData.matsCount = 0;
	}

	void addLight( const Light& l ) { mLights.push_back( l ); }

	void addTrig ( const Triangle& trig, const SurfaceMaterial* material );
		
	void inputSoftImage_StaticGeometry(
			const std::vector< Vec3 >& vertices
		,	const std::vector< int >& indexes
		,	const SurfaceMaterial* material
		,	const std::vector< Vec3 >* normals	= 0
		,	const std::vector< Vec3 >* uvw		= 0
		,	float scale							= 1		
		,	Vec3 disp							= Vec3 (0,0,0)
		,	const int maxTrigs					= 1000*1000*1000
	);
	
	void addFlatQuad( const Vec3& _00, const Vec3& _01, const Vec3& _11, const Vec3& _10, const SurfaceMaterial* material );
	void buildAndExportGeometry( int32 warpSize );	


public:
	
	DevInstData devInstData;
	TreeData buildTree;

private:

	typedef std::map< const SurfaceMaterial*, int32 > MatChecker;
	MatChecker matSetChecker;
	std::vector< const SurfaceMaterial > materialsSequence;

	std::vector< Triangle > trigsAccumulator;
	std::vector< Light > mLights;

}; // class Scene


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
