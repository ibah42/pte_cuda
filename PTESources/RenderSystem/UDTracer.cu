
#include "PTESources/RenderSystem/UDTracer.cuh"
#include "PTESources/RenderSystem/HLBVH.cuh"
#include "PTESources/RenderSystem/HLBVHTracer.cuh"
#include "PTESources/RenderSystem/Texture.cuh"
#include "PTESources/Math/Geometry.hpp"

#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/sort.h>
#include <thrust/copy.h>
#include <thrust/sequence.h>
#include <thrust/random.h>
#include <thrust/generate.h>
#include <thrust/detail/type_traits.h>

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
		
#define MAX_RAY_CAST_DEPTH 16
#define ALMOST_HALF_PI (Pi/2 - 0.001f)
#define LEFT_MC_BIT_1 0x8000##0000

typedef uint32 MCodes;

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


struct RayBounds
{
	Vec3 min, max;

	__device__ __host__ void zero()	{ min.zero(); max.zero(); }
};
struct RayBoundsExtractor
{
	__device__ RayBounds operator()( const Ray* ray ) const
	{
		RayBounds b;
		b.min = ray->origin;
		b.max = b.min;
		return b;
	}
};
struct RayBoundsComposer
{
	__device__ RayBounds operator()( const RayBounds& a, const RayBounds& b )
	{
		RayBounds box;

		box.min.x = minimal( a.min.x, b.min.x );
		box.min.y = minimal( a.min.y, b.min.y );
		box.min.z = minimal( a.min.z, b.min.z );

		box.max.x = maximal( a.max.x, b.max.x );
		box.max.y = maximal( a.max.y, b.max.y );
		box.max.z = maximal( a.max.z, b.max.z );

		return box;
	}
};


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
	

__host__ RayBounds getRayBounds( Ray** raysPtrs, int32 count )
{
	RayBounds zeroBounds;
	zeroBounds.min = Vec3( MAX_FLOAT );
	zeroBounds.max = Vec3( MIN_FLOAT );

	RayBoundsExtractor extractor;
	RayBoundsComposer composer;
	RayBounds bounds = thrust::transform_reduce(
			thrust::device_ptr< Ray* >( raysPtrs )
		,	thrust::device_ptr< Ray* >( raysPtrs + count )
		,	extractor
		,	zeroBounds
		,	composer
	);
	
	return bounds;
}		


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__global__ void generateRayHash(
		const int32 size
	,	MCodes* out
	,	Ray** rays
	,	const Vec3 commonAABB_halfBounds
	,	const Vec3 commonAABB_center // this is geom box center
)
{
	int32 id = blockIdx.x * blockDim.x + threadIdx.x;

	if( id >= size )
		return;

	DEVICE_PTE_ASSERT( commonAABB_halfBounds.x>=0, __FILE__, __LINE__ );
	DEVICE_PTE_ASSERT( commonAABB_halfBounds.y>=0, __FILE__, __LINE__ );
	DEVICE_PTE_ASSERT( commonAABB_halfBounds.z>=0, __FILE__, __LINE__ );

	MCodes mask = LEFT_MC_BIT_1;
	MCodes result = 0;

	Vec3 point = rays[id]->dir;
	point += Vec3(1,1,1); //[ 0..2 ]
		
	float boxCenter[3];
	boxCenter[0] = point.x;
	boxCenter[1] = point.y;
	boxCenter[2] = point.z;
	
	float bounds[3];
	bounds[0] = 1;
	bounds[1] = 1;
	bounds[2] = 1;

	for( int i=0; i<4; i++ )
	{
		for( int k=0; k<3; k++ )
		{
			if ( point[k] > boxCenter[k] )
			{
				boxCenter[k] += bounds[k] * 0.5f;
				result |= mask;
			}
			else
				boxCenter[k] -= bounds[k] * 0.5f;
		
			bounds[k] *= 0.5;
			mask >>= 1;

			DEVICE_PTE_ASSERT( mask > 0, __FILE__, __LINE__ );
		}
	}

	//12 bits used

	point = rays[id]->origin;
	boxCenter[0] = commonAABB_center.x;
	boxCenter[1] = commonAABB_center.y;
	boxCenter[2] = commonAABB_center.z;
	
	bounds[0] = commonAABB_halfBounds.x;
	bounds[1] = commonAABB_halfBounds.y;
	bounds[2] = commonAABB_halfBounds.z;

	for( int i=0; i<6; i++ )
	{
		for( int k=0; k<3; k++ )
		{
			if ( point[k] > boxCenter[k] )
			{
				boxCenter[k] += bounds[k] * 0.5f;
				result |= mask;
			}
			else
				boxCenter[k] -= bounds[k] * 0.5f;
		
			bounds[k] *= 0.5;
			mask >>= 1;

			DEVICE_PTE_ASSERT( mask > 0, __FILE__, __LINE__ );
		}
	}		
	
	out[id] = result;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


struct RaysCompactor
{
	__device__ bool operator()( const Ray* rawRay )
    {
		return rawRay->type != Ray::RAW_RAY;
    }
};


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__global__ void initRaysArray( Ray* rawRays, int32 count, Ray** array )
{
	int32 id = blockIdx.x * blockDim.x + threadIdx.x;

	if( id >= count )
		return;

	array[id] = rawRays + id;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__host__ void executeCompactHashSortRays( Ray* rawRays, int32 count, RayArray& out, int32 warpSize )
{
	out.compactedRaysCount = 0;
	out.raysPool = rawRays;
	Ray** tempArray;
	allocDevice( &out.sortedRaysPtr, count );
	allocDevice( &tempArray, count );

	dim3 grid( ceilf( float(count) / warpSize ), 1, 1 );
	dim3 block( warpSize, 1, 1 );

	initRaysArray<<< grid, block >>>( rawRays, count, out.sortedRaysPtr );
	//compact rays
	thrust::device_ptr< Ray* > end = thrust::copy_if(
			thrust::device_ptr< Ray* >( out.sortedRaysPtr )
		,	thrust::device_ptr< Ray* >( out.sortedRaysPtr + count )
		,	thrust::device_ptr< Ray* >( tempArray )
		,	RaysCompactor()
	);
	out.compactedRaysCount = end.get() - tempArray;

	freeDevice( out.sortedRaysPtr );
	out.sortedRaysPtr = tempArray;
	tempArray=0;
	
	//do not use them!
	count = -1;
	rawRays = 0;
	RayBounds rb = getRayBounds( out.sortedRaysPtr, out.compactedRaysCount );

	uint32* rayMC;
	allocDevice( &rayMC, out.compactedRaysCount );

	//generate ray - hash
	grid.x = ceilf( float(out.compactedRaysCount) / warpSize );
	generateRayHash<<< grid, block >>>(
			out.compactedRaysCount
		,	rayMC
		,	out.sortedRaysPtr
		,	( rb.max - rb.min ) * 0.5f
		,	( rb.max + rb.min ) * 0.5f
	);

	//sort rays
	thrust::sort_by_key(
			thrust::device_ptr< MCodes >( rayMC )
		,	thrust::device_ptr< MCodes >( rayMC + out.compactedRaysCount )
		,	thrust::device_ptr< Ray* >( out.sortedRaysPtr )
	);

	freeDevice(rayMC);
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__global__ void gpuPacketTrace(
		int32 count
	,	Ray** rays
	,	Intersection* isecs
	,	float maxRayCastQuadDepth
	,	const BoundedTriangle* trigs
	,	const int32* trigIds
	,	const TNode* root
)
{
	int32 id = blockIdx.x * blockDim.x + threadIdx.x;

	if( id >= count )
		return;

	HLBVHTracer::traceNearest( trigs, trigIds, root, *(rays[id]), isecs[id], maxRayCastQuadDepth );	
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


struct IntersectionCompactor
{
	__device__ bool operator()( const Intersection* isec )
    {
		return isec->hasOnePointContact();
    }
};


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__host__ void executePacketTrace(
		RayArray& in
	,	IntersectionsArray& out
	,	float maxRayCastQuadDepth
	,	const BoundedTriangle* trigs
	,	const int32* trigIds
	,	const TNode* root
	,	const int32 warpSize
)
{
	dim3 grid( ceilf( float(in.compactedRaysCount) / warpSize ), 1, 1 );
	dim3 block( warpSize, 1, 1 );
	
	if( out.compactedIsecCount != in.compactedRaysCount )
	{
		out.compactedIsecCount = in.compactedRaysCount;
		freeDevice(out.isecPool);
		freeDevice(out.compactedIsecPtr);

		allocDevice( & out.isecPool, in.compactedRaysCount );
		allocDevice( & out.compactedIsecPtr, in.compactedRaysCount );
	}

	//real trace exec!
	gpuPacketTrace<<< grid, block >>>(
			in.compactedRaysCount
		,	in.sortedRaysPtr
		,	out.isecPool
		,	maxRayCastQuadDepth
		,	trigs
		,	trigIds
		,	root
	);
	
	//compact isecs
	Intersection** tempArray;
	allocDevice( &tempArray, out.compactedIsecCount );

	thrust::device_ptr< Intersection* > end = thrust::copy_if(
			thrust::device_ptr< Intersection* >( out.compactedIsecPtr )
		,	thrust::device_ptr< Intersection* >( out.compactedIsecPtr + out.compactedIsecCount )
		,	thrust::device_ptr< Intersection* >( tempArray )
		,	IntersectionCompactor()
	);
	out.compactedIsecCount = end.get() - tempArray;

	freeDevice( out.compactedIsecPtr );
	out.compactedIsecPtr = tempArray;
	tempArray=0;	
}

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__host__ void executeTraceConveyorIteration( const int32 warpSize )
{
	//executeCompactHashSortRays()
//	executePacketTrace()
}

__host__ void executeGPUConveyor(
		CameraUnit cu
	,	RenderBuffer buff
	,	const DevInstData did
	,	Random random
	,	const RayTraceParams rtp
	,	const SphereEnvTex deviceSET
	,	const TNode* root
	,	const BoundedTriangle* trigs
	,	const int32* trigIds
	,	const int32 trigsCount
	,	const float maxRayCastQuadDepth
)
{
	assert( root );
	const int32 id = blockIdx.x * blockDim.x + threadIdx.x;
	if ( id >= buff.x() * buff.y() )
		return;
	
	const int32 x = id % buff.x();
	const int32 y = id / buff.x();

}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__host__ UDTracer::UDTracer( float maxRayCastQuadDepth, const SphereEnvTex* deviceSET, const RayTraceParams& params, int32 processorsCount )
	:	maxRayCastQuadDepth(maxRayCastQuadDepth)
	,	deviceSET(deviceSET)
	,	rtp(params)
	,	devRand( processorsCount )
	,	currentAASample(0)
	,	maxAASamples(1)
{}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template< int32 DEPTH >
__device__ __noinline__ Color runRayIteration(
		curandState* state
	,	const Ray& ray
	,	const RayTraceParams& rtp
	,	float maxRayCastQuadDepth
	,	const DevInstData& did
	,	const BoundedTriangle* trigs
	,	const int32* trigIds
	,	const TNode* root
	,	const SphereEnvTex& deviceSET
	,	const int32 threadId
)
{
	Intersection sourceIsec;
	HLBVHTracer::traceNearest( trigs, trigIds, root, ray, sourceIsec, maxRayCastQuadDepth );
		
 	if ( !sourceIsec.hasOnePointContact() )
 		return deviceSET.onRay( ray ); 	
	
	Color result( 0,0,0 );
	//return result;
	const BoundedTriangle& trig = *sourceIsec.hitTriangle;
	DEVICE_PTE_ASSERT( trig.trig.material < did.matsCount, __FILE__, __LINE__ ); 
	const SurfaceMaterial * smat = did.mats + trig.trig.material;
	
	Vec3 trigTrueNormal = sourceIsec.impactNorm;
	Vec3 trigSmoothNormal = sourceIsec.getPointNormal();
	
	if ( trigTrueNormal.dot( trigSmoothNormal ) < 0 )
  		trigSmoothNormal = - trigSmoothNormal;
 	
	float trueU, trueV;
	trueU =	 sourceIsec.Up0 * trig.trig.u[0]	 +	 sourceIsec.Vp1 * trig.trig.u[1]	 +	 sourceIsec.Wp2 * trig.trig.u[2];
	trueV =	 sourceIsec.Up0 * trig.trig.v[0]	 +	 sourceIsec.Vp1 * trig.trig.v[1]	 +	 sourceIsec.Wp2 * trig.trig.v[2];

	// import only through the trueNormal !!!!
	// because correction of vertNormal would be correct !
	
	VertexMaterial material;
	int32 separator = threadId % 4;
		
	smat->getVertexMaterial( separator, material, trueU, trueV, trigTrueNormal );
		

	if ( !smat->mNormalTex.isInited() )
	{
		material.vertNormal = trigSmoothNormal;
	}
	else
	{
		if( trigTrueNormal.dot( material.vertNormal ) < 0 )
			material.vertNormal = - material.vertNormal;

		material.vertNormal += trigSmoothNormal - trigTrueNormal;
		material.vertNormal.voidNormalize();
	}

	// 3 correction's of normals of trig
	// must use one half-space normals - correct sided normals;

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
//DIFFUSE

	if ( material.diffK > 0 )
	{
		int lc = did.lightsCount;

		/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
		//DIRECT LAMBERT_LIGHTING

		for( int i=0; i<lc; i++ )
		{
			Color directLight( 0, 0, 0 );
			const Light& light = did.lights[ i ];
			
			//ATTENTION - use volatile, because VS could generate incorrect code!
			volatile bool useSampledLight = bool( light.radius > 0.00001f ) && rtp.maxDirectLightSamples() > 0;

			float counter = 0;
			if ( ! useSampledLight )
			{
 				Vec3 dir = (light.position - sourceIsec.contact).getNormalized();
				if ( trigTrueNormal.dot( dir ) <= 0 )
					continue;

				float f = material.vertNormal.dot( dir );

				if ( f > 0 && ! HLBVHTracer::tracePoint( sourceIsec.contact, light.position, trigs, trigIds, root ) )
				{
					counter += 1;
					directLight += light.getResultPowerOnDist( (sourceIsec.contact - light.position).magnitude() ) * material.diffC * f;					
				}
			}
			if ( useSampledLight )
			{
				const float toLightCenterDist = ( light.position - sourceIsec.contact ).magnitude();
				if ( toLightCenterDist < light.radius )
				{
					counter += 1;
					directLight += light.color * material.diffC;
				}
				else
				{
					for( int i=0; i<rtp.maxDirectLightSamples(); ++i )
					{
						Vec3 randomPoint = light.sampleRandomDirectHemiSphere( state, sourceIsec.contact );
							
						Vec3 dir = (randomPoint - sourceIsec.contact).getNormalized();
						if ( trigTrueNormal.dot( dir ) <= 0 )
							continue;

						float f = material.vertNormal.dot( dir );

						if ( f > 0  && ! HLBVHTracer::tracePoint( sourceIsec.contact, randomPoint, trigs, trigIds, root ) )
						{
							counter += 1;
							directLight += light.getResultPowerOnDist( (sourceIsec.contact - randomPoint).magnitude() ) * material.diffC * f;
						}
					}

					if ( counter > 1 )
						directLight /= counter;
				}

			} // if ( useSampledLight )
						
			result += directLight;

		} // direct lighting (sampled or not)
	
		/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
		//DIFFUSE LIGHTING

		if ( ray.diffBounces > 0 )
		{
			Color diffuseColor( 0,0,0 );
			for( int i=0; i<rtp.maxDiffSamples(); ++i )
			{			
				Ray newRay = Ray::randomHemiSphereRay( state, ray, Ray::diffuse, material.vertNormal, sourceIsec.contact, TRIANGLE_RAYCAST_OFFSET );

				if ( newRay.dir.dot( trigTrueNormal ) < 0 )
				{
					// make fake-offset from trig
					newRay.origin += trigTrueNormal * TRIANGLE_RAYCAST_OFFSET;
					newRay.dir.rotateToVector3( Angle(ALMOST_HALF_PI), trigTrueNormal, ray.dir );
																			
					diffuseColor += runRayIteration< DEPTH+1 >( state, newRay, rtp, maxRayCastQuadDepth, did, trigs, trigIds, root, deviceSET, threadId );
				}
				else
					diffuseColor += runRayIteration< DEPTH+1 >( state, newRay, rtp, maxRayCastQuadDepth, did, trigs, trigIds, root, deviceSET, threadId );
				
			}

			diffuseColor /= float ( rtp.maxDiffSamples() );
			result += diffuseColor * material.diffC;
		}

		result *= material.diffK;
	}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
//REFLECTION

		
	if ( material.reflectK > 0 && ray.reflBounces > 0 )
	{
		Color reflectColor( 0,0,0 );
		
		Vec3 idealReflectDir = reflect( ray.dir, material.vertNormal );

		if ( material.glossReflect > 0.99999f )
 		{
 			Ray newRay( ray, Ray::reflect, sourceIsec.contact, idealReflectDir, TRIANGLE_RAYCAST_OFFSET );

			if ( newRay.dir.dot( trigTrueNormal ) < 0 )
			{
				// make fake-offset from trig
				newRay.origin += trigTrueNormal * TRIANGLE_RAYCAST_OFFSET;
				Vec3 oldDir = newRay.dir;
				newRay.dir.rotateToVector3( Angle(ALMOST_HALF_PI), trigTrueNormal, ray.dir );
				
				reflectColor += runRayIteration< DEPTH+1 >( state, newRay, rtp, maxRayCastQuadDepth, did, trigs, trigIds, root, deviceSET, threadId );
			}
 			else
 				reflectColor += runRayIteration< DEPTH+1 >( state, newRay, rtp, maxRayCastQuadDepth, did, trigs, trigIds, root, deviceSET, threadId );
 		}
 		else
		{
			for( int i=0; i<rtp.maxReflectSamples(); ++i )
			{				
				float glossRand = devRandf( state, material.glossReflect, 1 );
				Ray newRay( Ray::randomConeRay( state, ray, Ray::reflect, idealReflectDir, sourceIsec.contact, glossRand, TRIANGLE_RAYCAST_OFFSET ) );

				if ( newRay.dir.dot( trigTrueNormal ) < 0 )
				{
					// make fake-offset from trig
					newRay.origin += trigTrueNormal * TRIANGLE_RAYCAST_OFFSET;
					newRay.dir.rotateToVector3( Angle(ALMOST_HALF_PI), trigTrueNormal, ray.dir );

					reflectColor += runRayIteration< DEPTH+1 >( state, newRay, rtp, maxRayCastQuadDepth, did, trigs, trigIds, root, deviceSET, threadId );
				}
 				else
 					reflectColor += runRayIteration< DEPTH+1 >( state, newRay, rtp, maxRayCastQuadDepth, did, trigs, trigIds, root, deviceSET, threadId );
			}
			
			reflectColor /= float ( rtp.maxReflectSamples() );
		}	
	
		reflectColor = reflectColor * material.reflectC; // filtering!
		reflectColor *= material.reflectK;
	
		result += reflectColor;		
	}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
//REFRACTION


	if ( material.refractK > 0 && ray.refrBounces > 0 )
	{
		Color refractColor( 0,0,0 );

		Vec3 idealRefractDir = refract( ray.dir, material.vertNormal, material.ior );

		if ( material.glossRefract > 0.99999f )
		{
			Ray newRay( ray, Ray::refract, sourceIsec.contact, idealRefractDir, TRIANGLE_RAYCAST_OFFSET );
			if ( newRay.dir.dot( trigTrueNormal ) > 0 )
			{
				// make fake-offset from trig
				// -= - because from the other side then ray, raycasted to ray-side
				newRay.origin -= trigTrueNormal * TRIANGLE_RAYCAST_OFFSET;
				newRay.dir.rotateToVector3( Angle(ALMOST_HALF_PI), -trigTrueNormal, ray.dir );

				refractColor += runRayIteration< DEPTH+1 >( state, newRay, rtp, maxRayCastQuadDepth, did, trigs, trigIds, root, deviceSET, threadId );
			}
 			else
				refractColor += runRayIteration< DEPTH+1 >( state, newRay, rtp, maxRayCastQuadDepth, did, trigs, trigIds, root, deviceSET, threadId );
		}
		else
		{
			for( int i=0; i<rtp.maxRefractSamples(); ++i )
			{	
				float glossRand = devRandf( state, material.glossRefract, 1 );
				Ray newRay( Ray::randomConeRay( state, ray, Ray::refract, idealRefractDir, sourceIsec.contact, glossRand, TRIANGLE_RAYCAST_OFFSET ) );
				if ( newRay.dir.dot( trigTrueNormal ) > 0 )
				{
					// make fake-offset from trig
					// -= - because from the other side then ray, raycasted to ray-side
					newRay.origin -= trigTrueNormal * TRIANGLE_RAYCAST_OFFSET;
					newRay.dir.rotateToVector3( Angle(ALMOST_HALF_PI), -trigTrueNormal, ray.dir );

					refractColor += runRayIteration< DEPTH+1 >( state, newRay, rtp, maxRayCastQuadDepth, did, trigs, trigIds, root, deviceSET, threadId );
				}
				else
 					refractColor += runRayIteration< DEPTH+1 >( state, newRay, rtp, maxRayCastQuadDepth, did, trigs, trigIds, root, deviceSET, threadId );
			}

			refractColor /= float ( rtp.maxRefractSamples() );
		}
			
		refractColor = refractColor * material.refractC; // filtering!
		refractColor *= material.refractK;
		
		result += refractColor;		
	}
	

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
//EMITTED
	
	
	result += material.emitC;
		

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
//PHONG angle-attenuation


	if ( ray.type == Ray::diffuse )
	{
		float f = fabs( material.vertNormal.dot( ray.dir ) );
		result *= f;
	}		
	
	return result;

} // __device__ Color UDTracer::onGeometry(


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template<  >
__device__ __noinline__ Color runRayIteration< MAX_RAY_CAST_DEPTH>(
		curandState* state
	,	const Ray& ray
	,	const RayTraceParams& rtp
	,	float maxRayCastQuadDepth
	,	const DevInstData& did
	,	const BoundedTriangle* trigs
	,	const int32* trigIds
	,	const TNode* root
	,	const SphereEnvTex& deviceSET
	,	const int32 threadId
)
{
	return Color(1,1,1);
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


//flat (1-D) grid model used!
__global__ void raycastFromCamera(
		CameraUnit cu
	,	RenderBuffer buff
	,	const DevInstData did
	,	Random random
	,	const RayTraceParams rtp
	,	const SphereEnvTex deviceSET
	,	const TNode* root
	,	const BoundedTriangle* trigs
	,	const int32* trigIds
	,	const int32 trigsCount
	,	const float maxRayCastQuadDepth
)
{
	assert( root );
	const int32 id = blockIdx.x * blockDim.x + threadIdx.x;
	if ( id >= buff.x() * buff.y() )
		return;
	
	const int32 x = id % buff.x();
	const int32 y = id / buff.x();

	Ray camRay = cu.getRay( random.getByOverlappingThreadId(id), x, y );
	
	camRay.diffBounces = rtp.maxDiffDepth();
	camRay.reflBounces = rtp.maxReflectDepth();
	camRay.refrBounces = rtp.maxRefractDepth();

	Color result = runRayIteration< 0 >(
			random.getByOverlappingThreadId(id)
		,	camRay
		,	rtp
		,	maxRayCastQuadDepth
		,	did
		,	trigs
		,	trigIds
		,	root
		,	deviceSET
		,	id
	);

	cu.addScreenColor( &buff, x, y, result );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__host__ void UDTracer::updateFrame( Camera* camera, const Instance& data, int32 warpSize  )
{
	RenderBuffer& buff = camera->getPrimeRenderBuffer();

	camera->zeroLayerIfCameraStateChanged( camera->getPrimeRenderBufferLayer() );
	
	uint32 grid = (uint32)ceilf( float( buff.x() * buff.y() ) / warpSize );
	
	if ( currentAASample >= maxAASamples )
		currentAASample = 0;

	PTE_CHECK( cudaDeviceSetLimit( cudaLimitStackSize, 32*1024 ) );


	raycastFromCamera<<< grid, warpSize >>>(
			camera->data	 
		,	buff
		,	data.devInstData
		,	devRand
		,	rtp
		,	* deviceSET
		,	data.buildTree.bvh.root()
		,	data.buildTree.devTrigs
		,	data.buildTree.devTrigIds
		,	data.buildTree.size
		,	maxRayCastQuadDepth
	);	

	++currentAASample;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace PTE

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

