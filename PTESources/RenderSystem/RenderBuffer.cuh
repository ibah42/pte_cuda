#pragma once

#include "PTESources/CudaInclude.hpp"
#include "PTESources/RenderSystem/Color.cuh"
#include "PTESources/Math/Utility.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#define RBS_LAYERS_COUNT 4

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

class RenderBufferStack;

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


class RenderBuffer
{
public:

	friend class RenderBufferStack;

public:

	__host__ RenderBuffer()
		:	px ( 0 )
		,	py ( 0 )
		,	devColorBuff( 0 )
		,	devSquareDepthBuff( 0 )
	{}

	__host__ void swap( RenderBuffer& that );
	__host__ void free();

	__host__ void resize( int x, int y );

	__inline __host__ __device__ Color* color( int x, int y )
	{
		assert (			
				x >= 0			
			&&	x < px	
			&&	y >= 0			
			&&	y < py	
		);
		return devColorBuff + y * px + x;
	}

	__inline __host__ __device__ Color* color( int i )
	{
		assert( i >= 0  && i < px * py );
		return devColorBuff + i;
	}

	__inline __host__ __device__ float* squareDepth( int x, int y )
	{
		assert (			
				x >= 0			
			&&	x < px	
			&&	y >= 0			
			&&	y < py	
		);
		return devSquareDepthBuff + ( y * px ) + x;
	}

	__inline __host__ __device__ float* squareDepth( int i )
	{
		assert( i >= 0  && i < px * py );
		return devSquareDepthBuff + i;
	}
	
	__inline __host__ __device__ int x() const { return px; }
	__inline __host__ __device__ int y() const { return py; }
	
private:

	Color* devColorBuff;
	float* devSquareDepthBuff;
	
	int px;
	int py;

}; // class RenderBuffer


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


class RenderBufferStack
{
public:

	__host__ RenderBufferStack()
	{
		px = 0;
		py = 0;
	}
	
	__host__ void free()
	{
		for( int i=0; i<RBS_LAYERS_COUNT;i++)
			renderBufferStack[i].free();

		px=0;
		py=0;
	}

	__host__ void zeroLayer( int l );

	__host__ bool resize( int x, int y );

	__host__ int x() const	{ return px; }
	__host__ int y() const	{ return py; }

	__host__ RenderBuffer& operator []( int i )
	{
		assert( i < RBS_LAYERS_COUNT && i >= 0 );
		return renderBufferStack[i];
	}

	__host__ int getLayersCount() const { return RBS_LAYERS_COUNT; }
	__host__ void swapBuffers( int layer1, int layer2 );

private:
	
	int px;
	int py;

	RenderBuffer renderBufferStack[RBS_LAYERS_COUNT];

}; // class RenderBufferStack


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
