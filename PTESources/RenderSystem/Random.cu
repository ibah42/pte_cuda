
#include "PTESources/RenderSystem/Random.cuh"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
	

__global__ void initRandom( int64 seed, curandState *state, int size)
{
	int idx = blockIdx.x * blockDim.x + threadIdx.x;
	if( idx>= size)
		return;

	curand_init( seed, idx, 0, &state[idx] );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__host__ Random::Random( int32 processorsCount )
	:	size(processorsCount)
{		
	allocDevice( &states, size );
	uint64 seed = abs( int32(timeGetTime()) );
	if( seed < uint64(1024)*1024*1024*2 );
	{
		seed <<= 28;
		seed += ::rand() % 1024*1024*128;
	}

	uint32 grid = (uint32)ceilf( float(size)/WARP_SIZE );
	initRandom<<< grid, WARP_SIZE>>>(seed, states, size);
}
	

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace PTE

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

