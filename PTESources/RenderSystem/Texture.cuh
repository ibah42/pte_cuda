#pragma once

#include "PTESources/CudaInclude.hpp"
#include "PTESources/RenderSystem/Color.cuh"
#include "PTESources/Math/Utility.hpp"
#include "PTESources/Math/Vector3.hpp"
#include "PTESources/Math/Vector2.hpp"
#include "PTESources/Math/Ray.hpp"
#include "PTESources/Math/Geometry.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


struct ColorTextureUchar4
{
	__host__ ColorTextureUchar4( const ColorTextureUchar4* that );
	__host__ ColorTextureUchar4( ColorA c );
	__host__ ColorTextureUchar4( const char* fullPath );

	__host__ void free();
	__device__ ColorA get( float nx, float ny ) const;

	__host__ __device__ bool isInited() const { return texArray != 0; }

protected:

	cudaArray *texArray;
	cudaTextureObject_t tex;

}; // struct ColorTextureUchar4


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


struct NormalTextureVec3
	:	public ColorTextureUchar4
{
	__host__ NormalTextureVec3( const NormalTextureVec3* that );
	__host__ NormalTextureVec3( const char* fullPath )
		:	ColorTextureUchar4( fullPath )
	{}

	__device__ Vec3 get( float nx, float ny, const Vec3& trueNormal  ) const;
	__host__ void free(){ ColorTextureUchar4::free(); }

}; // struct NormalTextureVec3


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


struct EmitTextureUchar4
	:	public ColorTextureUchar4
{
	__host__ EmitTextureUchar4( const EmitTextureUchar4* that );
	__host__ EmitTextureUchar4( const ColorA c, float intensity )
		:	ColorTextureUchar4( c )
		,	intensity(intensity)
	{}

	__host__ EmitTextureUchar4( const char* fileName, float intensity )
		:	ColorTextureUchar4( fileName )
		,	intensity(intensity)
	{}
	
	__host__ void free(){ ColorTextureUchar4::free(); }
	__device__ Color get( float u, float v ) const
	{
		return ColorTextureUchar4::get( u, v ) * intensity;
	}

public:

	float intensity;

}; // struct EmitTextureUchar4


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


struct SphereEnvTex
	:	private ColorTextureUchar4
{
	__host__ SphereEnvTex( const char * fileName )
		:	ColorTextureUchar4(fileName)
	{}

	__host__ SphereEnvTex( ColorA c )
		:	ColorTextureUchar4(c)
	{}

	__device__ Color onRay ( const Ray& ray ) const;	
	__host__ void free(){ ColorTextureUchar4::free(); }

}; // struct SphereEnvTex


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

