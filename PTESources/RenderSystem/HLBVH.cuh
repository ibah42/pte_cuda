#pragma once

#include "PTESources/CudaInclude.hpp"
#include "PTESources/Math/Utility.hpp"
#include "PTESources/Math/Geometry.hpp"


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#define MAX_TREE_DEPTH 48
#define MC32

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


#ifdef MC64

	typedef uint64 MCodes;
	#define LEFT_MC_BIT_1 0x8000##0000##0000##0000
	#define COUNT_LEADING_ZEROS( value ) __clzll( value );
	#define MC_GENERATE_DEPTH 21
	#define MC_GENERATE_FINAL_MASK 0xFFFFffffFFFFfffe

#elif defined(MC32)

	typedef uint32 MCodes;
	#define LEFT_MC_BIT_1 0x8000##0000
	#define COUNT_LEADING_ZEROS( value ) __clz( value );
	#define MC_GENERATE_DEPTH 10
	#define MC_GENERATE_FINAL_MASK 0xFFFFfffC

#else

	#error must be defined MC32 for 30-bit Morton code or MC64 for 63-bit Morton code

#endif // MC64


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void RLEtest( const MCodes* cpuMC, const int32 size, const int32 warpSize );


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


struct TNode
{
	AABB bounds;
	
	// this is index in array of indexes of trigs!
	int32 startTrigIdId;
	int32 endTrigIdId;

	const TNode* left;
	const TNode* right;
};


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


struct HLBVH
{
	__host__ HLBVH()
	{
		ZeroMemory( this, sizeof(HLBVH) );
	}

	__host__ ~HLBVH()
	{
		for( int i=0; i<MAX_TREE_DEPTH; i++ )
		{
			if( nodeLayers[i] == 0 )
				break;

			freeDevice( nodeLayers[i] );			
		}
	}

	__device__ __host__ TNode* root()				{ return nodeLayers[0]; }
	__device__ __host__ const TNode* root() const	{ return nodeLayers[0]; }

public:

	int32 treeDepth;
	TNode* nodeLayers		[MAX_TREE_DEPTH];
	int32  nodeLayerSizes	[MAX_TREE_DEPTH];

};


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


// trigs + morton code + BVH
struct TreeData
{
	__host__ TreeData()
		:	size(0)
		,	codes(0)
		,	devTrigs(0)
		,	devTrigIds(0)
		//,	deviceClone(0)
		,	totalBounds( 0,0,0,0,0,0 )
	{}

	__host__ inline void operator = ( TreeData& that )
	{
		swap(that);
	}

	__host__ inline TreeData ( TreeData& that )
	{
		swap(that);
	}

	__host__ void init( const Triangle* array, int32 size, int32 warpSize );

	__host__ inline ~TreeData()
	{
		freeDevice( devTrigIds );
		freeDevice( codes );
		freeDevice( devTrigs );

// 		if( deviceClone )
// 			freeDevice(deviceClone);
	}
	
private:

	__host__ inline void TreeData::swap( TreeData & that )
	{
		devTrigIds = that.devTrigIds;
		codes = that.codes;
		devTrigs = that.devTrigs;
	
		size = that.size;
	
		totalBounds = that.totalBounds;

		that.totalBounds.set( 0,0,0, 0,0,0 );
		that.devTrigIds = 0;
		that.codes = 0;
		that.devTrigs = 0;
		that.size = -1;
	}

// 	__host__ TreeData* getDeviceClone()
// 	{
// 		if( deviceClone )
// 			return deviceClone;
// 
// 		allocDevice( &deviceClone, 1 );
// 		copyToDevice( deviceClone, this, 1 );
// 		return deviceClone;
// 	}

public:

	// array of BoundedTriangle, copied on GPU and stored
	const BoundedTriangle* devTrigs;

	// array of indexes on devTrigs
	int32* devTrigIds;

	// array of codes MC
	MCodes* codes;

	AABB totalBounds;

	// count of trigs
	int32 size;

	// built, tracable hierarchy
	HLBVH bvh;

// private:
// 
// 	TreeData* deviceClone;
};


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

__host__ void buildHLBVH( TreeData& result, const std::vector< Triangle >& trigs, int32 warpSize );

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
