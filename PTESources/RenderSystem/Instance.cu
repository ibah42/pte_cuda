

#include "PTESources/RenderSystem/Instance.cuh"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void Instance::addTrig ( const Triangle& trig, const SurfaceMaterial* material )
{
	int32 lastTrigId = (int32)trigsAccumulator.size();
	
	MatChecker::iterator found = matSetChecker.find( material );
	
	if( found == matSetChecker.end() )
	{
		matSetChecker.insert( std::pair< const SurfaceMaterial*, int32 >( material, int32(materialsSequence.size()) ) );
		materialsSequence.push_back( *material );

		trigsAccumulator.push_back( trig );
		trigsAccumulator[ lastTrigId ].material = materialsSequence.size()-1;
	}
	else
	{		
		trigsAccumulator.push_back( trig );
		trigsAccumulator[ lastTrigId ].material = (*found).second;
	}
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

void Instance::inputSoftImage_StaticGeometry(
		const std::vector< Vec3 >& vertices
	,	const std::vector< int >& indexes
	,	const SurfaceMaterial* material
	,	const std::vector< Vec3 >* normals
	,	const std::vector< Vec3 >* uvw		
	,	float scale							
	,	Vec3 disp							
	,	const int maxTrigs					
)
{
	assert( indexes.size() >= 4 );
	assert( vertices.size() >= 3 );
	if ( normals )
		assert( normals->size() >= 3 );
	
	if( normals )
		materialsSequence.reserve( normals->size() / 3 + 5 );

	int currId = 0;
	int currentTrigId = 0;
	int currentNormalId = 0;
	int currentUVWId = 0;
	while( maxTrigs > trigsAccumulator.size() )
	{
		if ( currId >= indexes.size() )
			return;

		Vec3 v0 = vertices.at( indexes[currId] ) * scale;
		Vec3 v1 = vertices.at( indexes[currId+1] ) * scale;
		Vec3 v2 = vertices.at( indexes[currId+2] ) * scale;
		v0 += disp;
		v1 += disp;
		v2 += disp;

		if ( (v2-v0).cross( v1-v0).magnitudeSquared() <= 0 )
		{
			currId += 4;
			currentNormalId += 3;
			++currentTrigId;
			continue;
		}
		
		Triangle t( v0, v1, v2 );

		if ( normals )
		{
			t.n0 = normals->at( currentNormalId );
			t.n1 = normals->at( currentNormalId+1 );
			t.n2 = normals->at( currentNormalId+2 );	
		}

		if ( uvw )
		{
			t.u[0] = uvw->at( currentUVWId ).x;
			t.u[1] = uvw->at( currentUVWId+1 ).x;
			t.u[2] = uvw->at( currentUVWId+2 ).x;

			t.v[0] = uvw->at( currentUVWId ).y;
			t.v[1] = uvw->at( currentUVWId+1 ).y;
			t.v[2] = uvw->at( currentUVWId+2 ).y;
		}
		else
		{
			t.u[0] = 0;	t.u[1] = 0;	t.u[2] = 0;
			t.v[0] = 0;	t.v[1] = 0;	t.v[2] = 0;
		}
		
		if ( t.getSquare() <= 0 )
			std::cout<< "\nDEGENERATE triangle in inputSoftImage_StaticGeometry have come!\n";
		else
			addTrig( t, material );

		currId += 4;
		currentNormalId += 3;
		++currentTrigId;
	}
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void Instance::buildAndExportGeometry( int32 warpSize )
{
	devInstData.lightsCount = (int32)mLights.size();
	if( devInstData.lightsCount>0)
	{
		allocDevice( &devInstData.lights, (int32)mLights.size() );
		copyToDevice( devInstData.lights, &(mLights[0]), (int32)mLights.size() );
	}
	
	allocDevice( &devInstData.mats, (int32)materialsSequence.size() );
	copyToDevice( devInstData.mats, &(materialsSequence[0]), (int32)materialsSequence.size() );
	devInstData.matsCount = (int32)materialsSequence.size();
	
	buildHLBVH( buildTree, trigsAccumulator, warpSize);
	
	trigsAccumulator.clear();
	matSetChecker.clear();
	mLights.clear();
	materialsSequence.clear();
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void Instance::addFlatQuad( const Vec3& _00, const Vec3& _01, const Vec3& _11, const Vec3& _10, const SurfaceMaterial* material )
{
	Triangle tR1( _01, _11, _10	);
	tR1.setUV( 0,1,1, 1,1,0 );

	Triangle tR2( _10, _00, _01	);
	tR2.setUV( 1,0,0, 0,0,1 );

	tR1.material = -42;
	tR2.material = -42;

	addTrig( tR1, material );
	addTrig( tR2, material );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace PTE

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

