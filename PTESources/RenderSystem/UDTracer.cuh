#pragma once

#include "PTESources/CudaInclude.hpp"
#include "PTESources/Math/Intersection.cuh"
#include "PTESources/RenderSystem/Material.cuh"
#include "PTESources/RenderSystem/Camera.cuh"
#include "PTESources/RenderSystem/Random.cuh"
#include "PTESources/RenderSystem/Instance.cuh"


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

struct SphereEnvTex;

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


struct RayArray
{
	Ray* raysPool;
	
	int32 compactedRaysCount;
	Ray** sortedRaysPtr;
};

struct IntersectionsArray
{
	Intersection* isecPool;
	
	int32 compactedIsecCount;
	Intersection** compactedIsecPtr;
};


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


struct RayTraceParams
{
	__host__ RayTraceParams()
	{}

	__host__ __device__ RayTraceParams( int32 diffDepth, int32 reflectDepth, int32 refractDepth, int32 diffSam, int32 reflSam, int32 refrSam, int32 perLightSample )
	{
		data[0] = diffDepth;
		data[1] = reflectDepth;
		data[2] = refractDepth;

		data[3] = diffSam;
		data[4] = reflSam;
		data[5] = refrSam;

		data[6] = perLightSample;
	}

	__host__ __device__ int32 maxDiffDepth()			const	{ return data[0]; }
	__host__ __device__ int32 maxReflectDepth()			const	{ return data[1]; }
	__host__ __device__ int32 maxRefractDepth()			const	{ return data[2]; }

	__host__ __device__ int32 maxDiffSamples()			const	{ return data[3]; }
	__host__ __device__ int32 maxReflectSamples()		const	{ return data[4]; }
	__host__ __device__ int32 maxRefractSamples()		const	{ return data[5]; }

	__host__ __device__ int32 maxDirectLightSamples()	const	{ return data[6]; }
	

private:
	
	int32 data[8];

}; // struct RayTraceParams


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


struct UDTracer
{
	__host__ UDTracer( float maxRayCastQuadDepth, const SphereEnvTex* deviceSET, const RayTraceParams& params, int32 processorsCount );

	__host__ void setAASamples(int32 count)
	{
		currentAASample = 0;
		maxAASamples = count;
	}
	__host__ int32 getAASamples()
	{
		return maxAASamples;
	}
	__host__ void UDTracer::updateFrame( Camera* camera, const Instance& data, int32 warpSize );

public:
	
	void free()
	{
		devRand.free();
	}
	
	~UDTracer()		{ free(); }

private:

	Random devRand;

	const RayTraceParams rtp;
	const SphereEnvTex* deviceSET;
	const float maxRayCastQuadDepth;
	int32 currentAASample;
	int32 maxAASamples;

}; // struct UDTracer


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
