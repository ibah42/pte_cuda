#include "PTESources/PCH.hpp"
#include "PTESources/RenderSystem/Scene.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
Scene::buildStaticGeom()
{
	BTVec vec = BTVec::buildFromArray( &(mBoundedTriangles[0]), (int)mBoundedTriangles.size() );
	if ( mStaticGeom )
		delete mStaticGeom;

	if ( vec.mCount > 0 )
		mStaticGeom = BVHNode::buildPackedBVH( vec );
	else
		mStaticGeom = 0;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
Scene::rayCast( Intersection& result, const Ray& ray, float maxQuadDist, const BoundedTriangle* ignore )
{
	assert( mStaticGeom->mBVHNodes );
	mStaticGeom->mBVHNodes->traceNearest( ray, result, maxQuadDist, ignore );

} // Scene::rayCast


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


bool
Scene::rayCastPoint( const Vec3& origin, const Vec3& point, const BoundedTriangle* ignore )
{
	assert( mStaticGeom->mBVHNodes );
	Vec3 trueDist = point - origin;
	Ray ray( origin, trueDist, Ray::camera );
	return mStaticGeom->mBVHNodes->traceAny( ray, trueDist.magnitudeSquared(), ignore );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


bool
Scene::rayCastAny( const Ray& ray, const BoundedTriangle* ignore )
{
	assert( mStaticGeom->mBVHNodes );
	return mStaticGeom->mBVHNodes->traceAny( ray, 9E+20f, ignore );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
Scene::update( Camera& cam, float dt )
{
	mRaysPerFrame = 0;
	if ( cam.getLayersCount() < 4 )
		cam.setLayersCount( 4 );

	assert( mStaticGeom->mBVHNodes );
	render( cam );

	float rps = mRaysPerFrame / dt;
	std::cout << "/n rays per second = "<<rps<<std::endl;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


Color Scene::renderBeam( Camera& cam, int x, int y, const int beams, float& depth )
{
	Color col( 0.f, 0.f, 0.f );
	
	// check if count has changed previously!
	if ( mCurrentAAIteration > mAACount)
		mCurrentAAIteration = 0;
	
	Ray ray = cam.getRay(
			x
		,	y
		,	mCurrentAAIteration
		,	beams
	);

	ray.type = Ray::camera;
	ray.diffBounces = mRayTraceParams.maxDiffDepth();
	ray.reflBounces = mRayTraceParams.maxReflectDepth();
	ray.refrBounces = mRayTraceParams.maxRefractDepth();

	col += geometryShaderInvokeWithDepth( ray, 0, mRayTraceParams, depth );
	
	++mCurrentAAIteration;

	if ( mCurrentAAIteration > mAACount)
		mCurrentAAIteration = 0;

	//	were used as all aa-rays casted per one frame
	//	col *= 1 / float(beams);

	return col;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


Color
Scene::geometryShaderInvoke( const Ray& ray, const BoundedTriangle* ignore, const RayTraceParams& currDepth )
{
	Intersection isec;
	rayCast( isec, ray, 9e+20f, ignore );
	mRaysPerFrame++;
	if ( isec.hasOnePointContact() )
		return mSurfaceShader.onGeometry( 0, isec, ray, currDepth, 9e+20f, mRaysPerFrame );
	else
		return mEnvShader->onRay( ray );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


Color
Scene::geometryShaderInvokeWithDepth( const Ray& ray, const BoundedTriangle* ignore, const RayTraceParams& currDepth, float& squareDepth )
{
	Intersection isec;
	isec.quadDistance = 9e+20f;
	rayCast( isec, ray, 9e+20f, ignore );
	mRaysPerFrame++;
	squareDepth = isec.quadDistance;

	if ( isec.hasOnePointContact() )
		return mSurfaceShader.onGeometry( 0, isec, ray, currDepth, 9e+20f, mRaysPerFrame );
	else
		return mEnvShader->onRay( ray );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


Color
Scene::geometryShaderInvokePrimeCast( const Ray& ray, const BoundedTriangle& prime, const RayTraceParams& currDepth )
{
	Intersection isec;
	Intersection::intersect( isec, ray, prime );
	if ( isec.hasOnePointContact() )
		return mSurfaceShader.onGeometry( 0, isec, ray, currDepth, 9e+20f, mRaysPerFrame );
	else
	{
		mRaysPerFrame++;
		rayCast( isec, ray, 9e+20f, &prime );

		if ( isec.hasOnePointContact() )
			return mSurfaceShader.onGeometry( 0, isec, ray, currDepth, 9e+20f, mRaysPerFrame );
		else
			return mEnvShader->onRay( ray );
	}	
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


inline Color* getAlias( RenderBuffer & buff, int x, int y )
{
	int	xn = clamp( 0, buff.xSize()-1, x );
	int	yn = clamp( 0, buff.ySize()-1, y );
	return buff.color( xn, yn );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


Color
Scene::adaptiveAA( Camera& camera, int x, int y, int sourceLayer, int beams, float& depth )
{
	//x9 sampling detector
	RenderBuffer& buff = *camera.mRenderBufferStack[sourceLayer];
	
	Color* c[9];

	c[0] = getAlias( buff,  x-1,  y-1 );
	c[1] = getAlias( buff,  x  ,  y-1 );
	c[2] = getAlias( buff,  x+1,  y-1 );
								 
	c[3] = getAlias( buff,  x-1,  y   );
	c[4] = getAlias( buff,  x  ,  y   );
	c[5] = getAlias( buff,  x+1,  y   );
								 
	c[6] = getAlias( buff,  x-1,  y+1 );
	c[7] = getAlias( buff,  x  ,  y+1 );
	c[8] = getAlias( buff,  x+1,  y+1 );

	float derivationR = 0, derivationG = 0, derivationB = 0;
	float sumR = 0, sumG = 0, sumB = 0;
	for( int i=0; i<9; ++i )
	{
		derivationR += c[4]->derivationR( *c[i] );
		derivationG += c[4]->derivationG( *c[i] );
		derivationB += c[4]->derivationB( *c[i] );

		sumR += c[i]->r;
		sumG += c[i]->g;
		sumB += c[i]->b;
	}
	
	assert( maxRGB_AADegradation_derivation <= maxRGB_AASample_derivation );

	if ( derivationR >= maxRGB_AASample_derivation || derivationG >= maxRGB_AASample_derivation || derivationB >= maxRGB_AASample_derivation )
	{
		return renderBeam( camera, x, y, beams, depth );
	}
// 	else if ( derivationR > maxRGB_AADegradation_derivation || derivationG > maxRGB_AADegradation_derivation || derivationB > maxRGB_AADegradation_derivation )
// 	{
// 		const float DegrForce = 0.8f;
// 		const float ADF = 1 - DegrForce;
// 		return Color(
// 				sumR / 9 * DegrForce + c[4]->r * ADF
// 			,	sumG / 9 * DegrForce + c[4]->g * ADF
// 			,	sumB / 9 * DegrForce + c[4]->b * ADF
// 		);
// 	}
	else
		return *c[4];
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
Scene::render( Camera& camera )
{
	int maxCount = camera.mNbPixelsX * camera.mNbPixelsY;
	int primeLayer = camera.getPrimeRenderBufferLayer();

	if ( mSceneChanged )
	{
		camera.zeroLayer( primeLayer, 0 );
		mSceneChanged = false;
	}
	else
		camera.zeroLayerIfCameraStateChanged( primeLayer, 0 );

	// this "if"-statement causes *holes* in scene - doesn't stop origin rays!
	// DEPRECATED - but check it!
	if ( camera.mScreenColorsThroughPathCount == 0 || mAACount <= 1 )
	{
		#pragma omp parallel for schedule( static, 4 )
		for ( int y = 0; y < camera.mNbPixelsY; ++y )
		{
			for ( int x = 0; x < camera.mNbPixelsX; ++x )
			{
				float depth;
				Color col = renderBeam( camera, x, y, 1, depth );
				camera.addScreenColor( primeLayer, x, y, col, depth );
			}
		}
	}

	if( mAACount > 1 )
	{
		#pragma omp parallel for schedule( static, 1 )
		for ( int y = 0; y < camera.mNbPixelsY; ++y )
		{
			for ( int x = 0; x < camera.mNbPixelsX; ++x )
			{
				float depth;
				Color col = adaptiveAA( camera, x, y, primeLayer, mAACount, depth );
				camera.addScreenColor( primeLayer, x, y, col, depth );
			}
		}
	}

	++ camera.mScreenColorsThroughPathCount;

} // Scene::render


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
Scene::inputSoftImage_StaticGeometry(
		const std::vector< Vec3 >& vertices, const std::vector< Vec3 >* normals,  const std::vector< Vec3 >* uvw
	,	const std::vector< int >& indexes, float scale, const SurfaceMaterial* material,Vec3 disp, const int maxTrigs
)
{
	assert( indexes.size() >= 4 );
	assert( vertices.size() >= 3 );
	if ( normals )
		assert( normals->size() >= 3 );

	int currId = 0;
	int currentTrigId = 0;
	int currentNormalId = 0;
	int currentUVWId = 0;
	while( maxTrigs > currentTrigId )
	{
		if ( currId>= indexes.size() )
			return;

		Vec3 v0 = vertices.at( indexes[currId] ) * scale;
		Vec3 v1 = vertices.at( indexes[currId+1] ) * scale;
		Vec3 v2 = vertices.at( indexes[currId+2] ) * scale;
		v0 += disp;
		v1 += disp;
		v2 += disp;

		if ( (v2-v0).cross( v1-v0).magnitudeSquared() <= 0 )
		{
			currId += 4;
			currentNormalId += 3;
			++currentTrigId;
			continue;
		}
		
		Triangle t( v0, v1, v2 );

		if ( normals )
		{
			t.n0 = normals->at( currentNormalId );
			t.n1 = normals->at( currentNormalId+1 );
			t.n2 = normals->at( currentNormalId+2 );	
		}

		if ( uvw )
		{
			t.u[0] = uvw->at( currentUVWId ).x;
			t.u[1] = uvw->at( currentUVWId+1 ).x;
			t.u[2] = uvw->at( currentUVWId+2 ).x;

			t.v[0] = uvw->at( currentUVWId ).y;
			t.v[1] = uvw->at( currentUVWId+1 ).y;
			t.v[2] = uvw->at( currentUVWId+2 ).y;
		}
		else
		{
			t.u[0] = 0;	t.u[1] = 0;	t.u[2] = 0;
			t.v[0] = 0;	t.v[1] = 0;	t.v[2] = 0;
		}


	//	t.biSided = true;
		if ( !material )
			t.material = &mDefaultMaterial;
		else
			t.material = material;

		int index =	addTriangle( t );

		if ( mBoundedTriangles.at( index ).trig.getSquare() <= 0 )
			throw std::exception("DEGENERATE triangle in inputSoftImage_StaticGeometry have come!");

		currId += 4;
		currentNormalId += 3;
		++currentTrigId;
	}
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


Scene::Scene( const char* envTexName )
	:	mLightRot(0)
	,	mLightRotRadius( 30 )
	,	mStaticGeom(0)
	,	maxRGB_AASample_derivation( 0 )
	,	maxRGB_AADegradation_derivation( 0 )
	,	mAACount(64)
	,	mEnvShader( new DefaultEnvironmentShader( envTexName ) )
//	,	mEnvShader( new DefaultEnvironmentShader( Color( 0.4f, 0.4f, 0.4f ) ) )
	,	mRayTraceParams( 3,5,7, 1,1,1, 1 )
	,	mCurrentAAIteration(0)
	,	mSceneChanged( true )
	,	mSurfaceShader( *this )
	,	mDefaultDiff( ColorA( 0.7f, 0.7f, 0.7f, 1 ) )
	,	mDefaultEmit( ColorA( 0, 0, 0, 0 ), 1 )
{	
	//do not use normal-tex's
	mDefaultMaterial.mNormalTex = 0;
	mDefaultMaterial.mDiffTex = & mDefaultDiff;
	mDefaultMaterial.mEmitTex = & mDefaultEmit;
 	mDefaultMaterial.reflectC = Color( 1, 1, 1 );
 	mDefaultMaterial.refractC = Color( 1, 1, 1 );
		
	mDefaultMaterial.reflectK = 0;
 	mDefaultMaterial.refractK = 0;
 	mDefaultMaterial.ior = 0.5f;
 	mDefaultMaterial.glossReflect = 1;
 	mDefaultMaterial.glossRefract = 1;

	if (
		   ( (false != true) == 1)
		&& ( (false != true) == 1.0f)
		&& ( (false == true) == 0)
		&& ( (false == true) == 0.0f)
	)
	{
	}
	else
		throw std::exception("\n\n math logic conversion (with bool) is not expected!");
	
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
Scene::addFlatQuad( const Vec3& _00, const Vec3& _01, const Vec3& _11, const Vec3& _10, SurfaceMaterial* mat )
{
	Triangle tR1( _01, _11, _10	);
	tR1.setUV( 0,1,1, 1,1,0 );

	Triangle tR2( _10, _00, _01	);
	tR2.setUV( 1,0,0, 0,0,1 );

	tR1.material = mat;
	tR2.material = mat;

	addTriangle( tR1 );
	addTriangle( tR2 );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
