#pragma once

#include "PTESources/CudaInclude.hpp"
#include "PTESources/RenderSystem/HLBVHTracer.cuh"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#define MAX_TRACE_STACK_SIZE (MAX_TREE_DEPTH*2+2)

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__device__ __inline bool
HLBVHTracer::traceAnyTriangles( const int32 startId, const int32 endId, const BoundedTriangle* baseAdr, const int32* trigIds, const Ray& ray )
{
	int32 begin = startId;
	while( begin <= endId )
	{		
		if( Intersection::intersect( ray, baseAdr[ trigIds[ begin ] ] ) )
			return true;
		
		++begin;
	}

	return false;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__device__ __inline void
HLBVHTracer::traceNearestTriangles( const int32 startId, const int32 endId, const BoundedTriangle* baseAdr, const int32* trigIds, const Ray& ray, Intersection& out, float maxQuadDist )
{
	int32 begin = startId;
	Intersection bestTempIsec;
	Intersection isec;
	while( begin <= endId )
	{
		isec.resetType();
		Intersection::intersect( isec, ray, baseAdr[ trigIds[ begin ] ] );
		isec.validateWithMaxQuadDist( maxQuadDist );

		bestTempIsec.setBestIsec( maxQuadDist, isec );
		++begin;
	}

	out.setBestIsec( maxQuadDist, bestTempIsec );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__device__ bool
HLBVHTracer::tracePoint( Vec3 origin, Vec3 point, const BoundedTriangle* baseAdr, const int32* trigIds, const TNode* node )
{
	Vec3 trueDist = point - origin;
	Ray ray( origin, trueDist, Ray::camera, TRIANGLE_RAYCAST_OFFSET );

	return HLBVHTracer::traceAny( baseAdr, trigIds, node, ray, trueDist.magnitudeSquared() );	
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__device__ bool
HLBVHTracer::traceAny( const BoundedTriangle* baseAdr, const int32* trigIds, const TNode* node, const Ray& ray, float maxQuadDist )
{
	float isecDist;
	const TNode copy = *node;
	Intersection temp;
	const TNode* stack[MAX_TRACE_STACK_SIZE];
	int32 head = 0;
	stack[0] = node;

	while(1)
	{
		if ( head < 0 )
			return false;
		
		const TNode* parent = stack[head];
		if ( Intersection::intersect( ray, parent->bounds, isecDist ) )
		{			
			if ( head+1 >=MAX_TRACE_STACK_SIZE )
			{
				printf("traceNearestImpl stack overflow!\n");
				return false;
			}
				
			if( parent->left == 0 )
			{
				assert( parent->right == 0 );

				HLBVHTracer::traceNearestTriangles( parent->startTrigIdId, parent->endTrigIdId, baseAdr, trigIds, ray, temp, maxQuadDist );
				if( temp.hasOnePointContact() )
					return true;
				
				-- head;
			}
			else
			{
				//replace current root with its right child				
				stack[head] = parent->right;
				stack[head+1] = parent->left;
				++ head;
			}
		}
		else
		{
			-- head;
		}
	}
	
	return false;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__device__ void
HLBVHTracer::traceNearest( const BoundedTriangle* baseAdr, const int32* trigIds, const TNode* node, const Ray& ray, Intersection& out, float maxQuadDist )
{
	float isecDist;
	const TNode copy = *node;
	Intersection temp;
	const TNode* stack[MAX_TRACE_STACK_SIZE];
	int32 head = 0;
	stack[0] = node;

	while(1)
	{
		if ( head < 0 )
			return;
		
		const TNode* parent = stack[head];
		if ( Intersection::intersect( ray, parent->bounds, isecDist ) )
		{			
			if ( head+1 >=MAX_TRACE_STACK_SIZE )
			{
				printf("traceNearestImpl stack overflow!\n");
				return;
			}
				
			if( parent->left == 0 )
			{
				assert( parent->right == 0 );
				
				temp.resetType();
				HLBVHTracer::traceNearestTriangles( parent->startTrigIdId, parent->endTrigIdId, baseAdr, trigIds, ray, temp, maxQuadDist );
				out.setBestIsec( maxQuadDist, temp ); 
				-- head;
			}
			else
			{
				//replace current root with its right child				
				stack[head] = parent->right;
				stack[head+1] = parent->left;
				++ head;
			}
		}
		else
		{
			-- head;
		}
	}
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
