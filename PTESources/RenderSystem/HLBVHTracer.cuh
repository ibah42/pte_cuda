#pragma once

#include "PTESources/CudaInclude.hpp"
#include "PTESources/RenderSystem/HLBVH.cuh"
#include "PTESources/Math/Intersection.cuh"
#include "PTESources/Math/Utility.hpp"
#include "PTESources/Math/Geometry.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


struct HLBVHTracer
{

	__device__ static __noinline__ void
	traceNearest( const BoundedTriangle* baseAdr, const int32* trigIds, const TNode* node, const Ray& ray, Intersection& out, float maxQuadDist );

	__device__ static __noinline__ bool
	traceAny( const BoundedTriangle* baseAdr, const int32* trigIds, const TNode* node, const Ray& ray, float maxQuadDist );
	
	__device__ static __noinline__ bool
	tracePoint( Vec3 origin, Vec3 point, const BoundedTriangle* baseAdr, const int32* trigIds, const TNode* node );


//private: do not use functions above
	

	__device__  static __inline void
	traceNearestTriangles( const int32 startId, const int32 endId, const BoundedTriangle* baseAdr, const int32* trigIds, const Ray& ray, Intersection& out, float maxQuadDist );

	__device__ static __inline bool
	traceAnyTriangles( const int32 startId, const int32 endId, const BoundedTriangle* baseAdr, const int32* trigIds, const Ray& ray );


}; // struct HLBVHTracer


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
