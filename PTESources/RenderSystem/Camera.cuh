#pragma once

#include "PTESources/Math/Vector3.hpp"
#include "PTESources/Math/Quaternion.hpp"
#include "PTESources/Math/Ray.hpp"
#include "PTESources/RenderSystem/RenderBuffer.cuh"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


class Camera;


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


class CameraUnit
{

	friend class Camera;

public:

	__host__ CameraUnit();
	__host__ ~CameraUnit(){}
	
	__host__ void resetDirection();

	__host__ void updateCameraAttributes ( const Vec3& in_pos, const Vec3& in_upVec, const Vec3& in_dir, const float in_FOV );
	__host__ void setParams( int nbPixelsX, int nbPixelsY, float geomCameraSize );

	__device__ void addScreenColor( RenderBuffer* layer, int x, int y, Color color );
	__device__ Ray getRay( curandState* state, const int x, const int y ) const;
	

	//BLUR\DOF
public:

	__host__ void cameraChanged()			{ mCameraStateChanged = true; }
	__host__ bool isCameraChanged()	const	{ return mCameraStateChanged; }
	__host__ void cameraChangeReset()		{ mCameraStateChanged = false; }

	float mSquareDistFocalPlane;
	float mPixelDOFDiskRatio;

private:

// 	void randCubicRayOffset( float& x, float& y, const int layerId, const int maxLayers );
// 	void cubicRayOffset( float& x, float& y, const int layerId, const int maxLayers );

private:	
		
	int32 renderIterationCounter;
	bool mCameraStateChanged;
	int mNbPixelsX;
	int mNbPixelsY;

    float m_aspectRatio;
	float m_geomCameraSize;
	
	float m_FOV;

	// camera space orientation
	Vec3 m_focusPoint;

	Vec3 m_frameOriginPos;
	Vec3 m_frameAxisX;
	Vec3 m_frameAxisY;

	// world space orientation
	Vec3 m_pos;
	Vec3 m_dir;
	Vec3 m_up;	
		
	int mPrimeLayer;

}; // class CameraUnit



/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


class Camera
{	
private:

	Camera( Camera & );
	void operator = ( Camera & );

public:
	__host__ Camera(){}
	__host__ void zeroLayerIfCameraStateChanged( int layer );
	__host__ RenderBuffer& getPrimeRenderBuffer()	{ return mRenderBufferStack[ data.mPrimeLayer ]; }
	__host__ int getPrimeRenderBufferLayer() const	{ return data.mPrimeLayer; }
	__host__ int getLayersCount() const				{ return mRenderBufferStack.getLayersCount(); }
	__host__ void free();
	__host__ bool resize( int32 x, int32 y );

private:
	__host__ void zeroLayer( int layer );

public:

	CameraUnit data;

private:
	
	RenderBufferStack mRenderBufferStack;

}; // class Camera


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
