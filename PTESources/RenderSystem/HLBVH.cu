#include "PTESources/RenderSystem/HLBVH.cuh"

#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/sort.h>
#include <thrust/copy.h>
#include <thrust/sequence.h>
#include <thrust/random.h>
#include <thrust/generate.h>
#include <thrust/detail/type_traits.h>

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
	
#define MAX_ARRAY_INDEX 1024*1024*128

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


struct Split
{
	int32 parentId;
	int32 min, max;
};


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


//run length encode
struct RLE
{
	__host__ RLE()
		:	mcCount(0)
		,	originMC(0)
		,	coded(0)
		,	codedCount(0)
	{}


	__host__ __device__ ~RLE()
	{
		//empty
	}

	__host__ __device__ void operator = ( const RLE& r )
	{
		set(r);
	}

	__host__ __device__ RLE( const RLE& r )
		:	mcCount(0)
		,	originMC(0)
		,	coded(0)
		,	codedCount(0)
	{		
		set(r);
	}

	__host__ __device__ void set( const RLE& r )
	{
		coded = r.coded;
		const_cast< int32& >( mcCount ) = r.mcCount;
		originMC = r.originMC;
		codedCount = r.codedCount;
	}

	// id of start of the repeated mc-codes
	// 1 1 1 2 2 2 3 3 3
	// for 1: 0;   2: 3;      3: 5
	// and the repeat count
	struct Coded
	{
		int32 count, id;
	};

	__host__ RLE( const MCodes* codes, int32 size )
		:	originMC(codes)
		,	mcCount(size)
		,	codedCount(size)
	{
		allocDevice( &coded, mcCount );
	}

	__host__ void acceptNewCodedSize( int32 size, RLE::Coded* newCodedCompacted )
	{
		codedCount = size;
		freeDevice( coded );
		coded = newCodedCompacted;
	}

	__host__ void free()
	{
		checkCudaErrors( cudaFree( coded ) );
		coded = 0;
		originMC = 0;
		const_cast< int32& >( mcCount ) = 0;
		codedCount = 0;
	}
	
public:
	
	const MCodes* originMC;
	const int32 mcCount;
	int32 codedCount;
	Coded* coded;	

}; // struct RLE


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void __global__ initRLE( RLE rle )
{
	const int32 id = blockIdx.x * blockDim.x + threadIdx.x;
	if ( id >= rle.mcCount )
		return;

	rle.coded[id].count = 1;
	rle.coded[id].id = id;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void __global__ gatherRLE_even( const int32 currDepth, RLE rle )
{
	const int32 id = blockIdx.x * blockDim.x + threadIdx.x;
	const int32 maxId = rle.mcCount-1;
	if ( id > maxId )
		return;

	const int32 dw = powPositive( 2, currDepth + 1 );
	const int32 workPlace1 = id * dw;
	const int32 workPlace2 = workPlace1 + dw/2;

	// if missed - just skipp
	if( workPlace2 > maxId || float(id) * float(dw) > MAX_ARRAY_INDEX )
		return;

	//workplace1 is main, root!
	if ( rle.originMC[workPlace1] == rle.originMC[workPlace2] && rle.coded[workPlace2].count > 0 )
	{
		rle.coded[workPlace1].id = minimal( rle.coded[workPlace1].id, rle.coded[workPlace2].id );
		rle.coded[workPlace1].count += rle.coded[workPlace2].count;

		rle.coded[workPlace2].count = 0;
	}
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void __global__ gatherRLE_odd( const int32 currDepth, RLE rle )
{
	const int32 id = blockIdx.x * blockDim.x + threadIdx.x;
	const int32 maxId = rle.mcCount-1;
	if ( id > maxId )
		return;

	const int32 dw = powPositive( 2, currDepth+1 );
	const int32 workPlace2 = (id+1) * dw;
	const int32 workPlace1 = workPlace2 - dw/2;

	// if missed - just skipp
	if( workPlace2 > maxId || float(id) * float(dw) > MAX_ARRAY_INDEX )
		return;

	//workplace2 is main, root!
	if ( rle.originMC[workPlace1] == rle.originMC[workPlace2] && rle.coded[workPlace1].count > 0 )
	{
		rle.coded[workPlace2].id = minimal( rle.coded[workPlace1].id, rle.coded[workPlace2].id );
		
		rle.coded[workPlace2].count += rle.coded[workPlace1].count;	
		rle.coded[workPlace1].count = 0;
	}
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__global__ void initScanRLEData( const RLE rle, int32* outArray )
{	
	const int32 id = blockIdx.x * blockDim.x + threadIdx.x;
	if ( id >= rle.mcCount )
		return;

	outArray[id] = rle.coded[id].count > 0 ? 0 : 1;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__global__ void compactRLE( const RLE rle, const int32* offsets, RLE::Coded* compactedCodes )
{	
	const int32 id = blockIdx.x * blockDim.x + threadIdx.x;
	if ( id >= rle.mcCount )
		return;

	if( rle.coded[id].count > 0 )
	{	
#ifdef _DEBUG
		if( rle.coded[id].count > 5 )
		{
			char buff1[40];
				
			printDeviceInt32(buff1, rle.coded[id].count);
			printf("%s%s%s", "\ncompacted long chain :: compactRLE ", buff1, " ;\n" );
		}
#endif
		compactedCodes[ id - offsets[id] ] = rle.coded[id];
		DEVICE_PTE_ASSERT( id-offsets[id] >= 0, __FILE__, __LINE__ );
	}
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__host__ void checkValidRLEMC( RLE::Coded* cpuCoded, const int32 CodedCount, const MCodes* cpuMC, const int32 mcCount )
{
	int32 mcId = 0;
	int32 codedId = 0;
	for( int i=0; i<CodedCount; i++)
	{
		int32 currentCodedCount = cpuCoded[i].count;
		CPU_PTE_ASSERT( currentCodedCount > 0, __FILE__, __LINE__ );
		CPU_PTE_ASSERT( codedId <= cpuCoded[i].id, __FILE__, __LINE__ );
		CPU_PTE_ASSERT( mcId == cpuCoded[i].id, __FILE__, __LINE__ );

		if ( currentCodedCount > 1 )
		{
			int32 rolledCount = 0;
			while( mcId+1 < mcCount && currentCodedCount > 0 )
			{
				if ( cpuCoded[i].count == rolledCount+1 )
					CPU_PTE_ASSERT( cpuMC[mcId] != cpuMC[ 1+mcId ], __FILE__, __LINE__ );
				else
					CPU_PTE_ASSERT( cpuMC[mcId] == cpuMC[ 1+mcId ], __FILE__, __LINE__ );

				++mcId;
				--currentCodedCount;
				rolledCount++;
			}
		}
		else
		{
			if( mcId+1 < mcCount )
			{
				CPU_PTE_ASSERT( cpuMC[mcId] != cpuMC[ mcId+1 ], __FILE__, __LINE__ );
				++mcId;
			}
		}	

		++codedId;
	}
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void RLEprocessMC( RLE& rle, const int32 warpSize )
{
	const int32 depth = ceilf( log2f( rle.mcCount ) );
	
	dim3 grid( ceilf( float(rle.mcCount) / warpSize ), 1, 1 );
	dim3 block( warpSize, 1, 1 );

	initRLE<<< grid, block >>>( rle );

	for ( int i=0; i<depth; i++ )
	{
		dim3 grid(
				ceilf( float(rle.mcCount) / warpSize / powPositive(2,i) )
			,	1
			,	1
		);

		dim3 block( warpSize, 1, 1 );
		gatherRLE_odd<<< grid, block >>>( i, rle );
		gatherRLE_even<<< grid, block >>>( i, rle );
	}

	int32* devCompactOffsets;
	allocDevice( &devCompactOffsets, rle.mcCount );
	initScanRLEData<<< grid, block >>>( rle, devCompactOffsets );

	thrust::inclusive_scan(
			thrust::device_ptr< int32 >( devCompactOffsets )
		,	thrust::device_ptr< int32 >( devCompactOffsets+rle.mcCount )
		,	thrust::device_ptr< int32 >( devCompactOffsets )
	);

	int32 rleCodedSize; 
	copyToHost( &rleCodedSize, devCompactOffsets + rle.mcCount - 1, 1 );
	rleCodedSize = rle.mcCount - rleCodedSize;

	CPU_PTE_ASSERT( rleCodedSize > 0 && rleCodedSize <= rle.mcCount, __FILE__, __LINE__ );

	if( rleCodedSize < rle.mcCount )
	{
		RLE::Coded* newCompactedCodes;
		allocDevice( &newCompactedCodes, rleCodedSize );

		compactRLE<<< grid,block >>>( rle, devCompactOffsets, newCompactedCodes );
		rle.acceptNewCodedSize(rleCodedSize, newCompactedCodes);
	}
	freeDevice(devCompactOffsets);

	#ifdef _DEBUG
	{
		RLE::Coded* CodedCPU = (RLE::Coded*)malloc( sizeof(RLE::Coded) * rle.codedCount );
		copyToHost( CodedCPU, rle.coded, rle.codedCount );
		
		MCodes* mcCPU = (MCodes*)malloc( sizeof(MCodes) * rle.mcCount );
		copyToHost( mcCPU, rle.originMC, rle.mcCount);	
		
		checkValidRLEMC( CodedCPU, rle.codedCount, mcCPU, rle.mcCount );

		free( mcCPU );
		free( CodedCPU );	
	}
	#endif
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void RLEtest( const MCodes* cpuMC, const int32 size, const int32 warpSize )
{
	MCodes* codes;
	allocDevice( &codes, size );
		
	copyToDevice( codes, cpuMC, size );

	RLE rle( codes, size );
	RLEprocessMC( rle, warpSize );
	RLE::Coded* dataCPU;
	dataCPU = (RLE::Coded*)malloc( sizeof(RLE::Coded) * rle.codedCount );
	copyToHost( dataCPU, rle.coded, rle.codedCount );	
	free( dataCPU );
	
	rle.free();
	freeDevice( codes );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__global__ void initTrigsIdAndTrigs( const Triangle* trigs, BoundedTriangle* outTrigs, int32* ids, int32 size )
{
	int32 id = blockIdx.x * blockDim.x + threadIdx.x;
	if ( id >= size )
		return;

	ids[id] = id;
	outTrigs[id].set( trigs + id );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__host__ void TreeData::init( const Triangle* CPUtrigs, int32 size, int32 warpSize )
{
	this->size = size;
	allocDevice( &devTrigIds, size );
	allocDevice( &codes, size );

	Triangle* devRawTrigs;
	allocDevice( &devRawTrigs, size );
	copyToDevice( devRawTrigs, CPUtrigs, size );

	allocDevice( &devTrigs, size );
	
	uint32 grid = uint32( ceilf( float(size)/warpSize ) );
	initTrigsIdAndTrigs<<< grid, warpSize >>>( devRawTrigs, const_cast<BoundedTriangle*>(devTrigs), devTrigIds, size );
	
	freeDevice(devRawTrigs);
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__global__ void GenerateMortonCode(
		const int32 size
	,	MCodes* outMC
	,	const BoundedTriangle* trigsBase
	,	const int32* trigsIds
	,	const Vec3 commonAABB_halfBounds
	,	const Vec3 commonAABB_center // this is geom box center
)
{
	int32 id = blockIdx.x * blockDim.x + threadIdx.x;

	if( id >= size )
		return;

	DEVICE_PTE_ASSERT( commonAABB_halfBounds.x>=0, __FILE__, __LINE__ );
	DEVICE_PTE_ASSERT( commonAABB_halfBounds.y>=0, __FILE__, __LINE__ );
	DEVICE_PTE_ASSERT( commonAABB_halfBounds.z>=0, __FILE__, __LINE__ );

	MCodes mask = LEFT_MC_BIT_1;
	MCodes result = 0;

	const Vec3 point = trigsBase[ trigsIds[id] ].box.center();
	
	double boxCenter[3];
	boxCenter[0] = commonAABB_center.x;
	boxCenter[1] = commonAABB_center.y;
	boxCenter[2] = commonAABB_center.z;
	
	double bounds[3];
	bounds[0] = commonAABB_halfBounds.x;
	bounds[1] = commonAABB_halfBounds.y;
	bounds[2] = commonAABB_halfBounds.z;

	for( int i=0; i<MC_GENERATE_DEPTH; i++ )
	{
		for( int k=0; k<3; k++ )
		{
			if ( point[k] > boxCenter[k] )
			{
				boxCenter[k] += bounds[k] * 0.5f;
				result |= mask;
			}
			else
				boxCenter[k] -= bounds[k] * 0.5f;
		
			bounds[k] *= 0.5;
			mask >>= 1;

			DEVICE_PTE_ASSERT( mask > 0, __FILE__, __LINE__ );
		}
	}	

	//correct last to bits to ZERO
	result &= MC_GENERATE_FINAL_MASK;
	
	outMC[id] = result;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


// MMBounds, MMBoundsExtractor, MMBoundsComposer
// used for extracting initial AABB of all trigs (on GPU)
struct MMBounds
{
	Vec3 min, max;

	__device__ __host__ void zero()	{ min.zero(); max.zero(); }
};
struct MMBoundsExtractor
{
	MMBoundsExtractor( const BoundedTriangle* trigsBase )
		:	trigsBase(trigsBase)
	{}

	__device__ MMBounds operator()( const int32 id ) const
	{
		MMBounds b;
		b.min = trigsBase[id].box.min();
		b.max = trigsBase[id].box.max();
		return b;
	}

private:
	const BoundedTriangle* trigsBase;
};
struct MMBoundsComposer
{
	__device__ MMBounds operator()( const MMBounds a, const MMBounds b )
	{
		MMBounds box;

		box.min.x = minimal( a.min.x, b.min.x );
		box.min.y = minimal( a.min.y, b.min.y );
		box.min.z = minimal( a.min.z, b.min.z );

		box.max.x = maximal( a.max.x, b.max.x );
		box.max.y = maximal( a.max.y, b.max.y );
		box.max.z = maximal( a.max.z, b.max.z );

		return box;
	}
};


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__global__ void splitBoundLayer( RLE rle, const Split* parent, const int32 parentSize, Split* outChild, const int32 depth )
{	
	const int32 id = blockIdx.x * blockDim.x + threadIdx.x;
	const int32 leftChildID  = id * 2;
	const int32 rightChildID = leftChildID + 1;

	if( id >= parentSize  )
		return;
		
	const Split p = parent[ id ];
		
	if( p.max - p.min < 2 || p.parentId < 0 )
	{
		outChild[leftChildID].parentId = -42;
		outChild[rightChildID].parentId = -42;
		return;
	}

	DEVICE_PTE_ASSERT( p.min < rle.codedCount, __FILE__, __LINE__ );
	DEVICE_PTE_ASSERT( p.max > p.min, __FILE__, __LINE__ );

	const MCodes firstCode = rle.originMC[ rle.coded[p.min].id ];
	const MCodes lastCode  = rle.originMC[ rle.coded[p.max-1].id ];

	if ( firstCode == lastCode )
	{
		outChild[leftChildID].parentId = -42;
		outChild[rightChildID].parentId = -42;
		return;
	}
	
	Split leftChild, rightChild;
	leftChild.parentId = id;
	rightChild.parentId = id;

	const int32 commonPrefix = COUNT_LEADING_ZEROS( firstCode ^ lastCode );
	int32 split = p.min;
	int32 step = p.max - p.min;

	do 
	{
		step = ( step + 1 ) / 2;
		int32 newSplit = split + step;

		if ( newSplit < p.max )
		{
			DEVICE_PTE_ASSERT( newSplit < rle.codedCount, __FILE__, __LINE__ );
			DEVICE_PTE_ASSERT( rle.coded[ newSplit ].id + rle.coded[ newSplit ].count - 1 < rle.mcCount, __FILE__, __LINE__ );
			
			const MCodes splitCode = rle.originMC[ rle.coded[ newSplit ].id ];
			const int32 splitPrefix = COUNT_LEADING_ZEROS( firstCode ^ splitCode );
			if ( splitPrefix > commonPrefix )
				split = newSplit;
		}

	} while (step > 1);


	leftChild.min = p.min;
	leftChild.max = split;

	rightChild.min = split+1;
	rightChild.max = p.max;

	outChild[leftChildID] = leftChild;
	outChild[rightChildID] = rightChild;
	
} // splitBoundLayer


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__global__ void initScanCompactSplitLayerData( Split* layer, const int32 layerSize, int32* outOffsets )
{
	const int32 id = blockIdx.x * blockDim.x + threadIdx.x;
	if ( id >= layerSize )
		return;
		
	int32 offset = layer[id].parentId >= 0 ? 0 : 1;		
	outOffsets[id] = offset;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__global__ void compactSplitLayer( const Split* layer, const int32 layerSize, const int32* offsets, Split* compactedLayer )
{	
	const int32 id = blockIdx.x * blockDim.x + threadIdx.x;
	if ( id >= layerSize )
		return;

	if( layer[id].parentId >= 0 )
	{		
		compactedLayer[ id - offsets[id] ] = layer[id];
		DEVICE_PTE_ASSERT( id-offsets[id] >= 0, __FILE__, __LINE__ );
	}
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


//exchange with new compacted layer!
__host__ inline int32 compactSplitLayers( Split** layer, const int32 layerSize, const int32 warpSize )
{
	int32* devCompactOffsets;
	allocDevice( &devCompactOffsets, layerSize );
	dim3 grid(ceilf( float(layerSize) / warpSize ), 1, 1 );
	dim3 block( warpSize, 1, 1 );

	initScanCompactSplitLayerData<<< grid, block >>>( *layer, layerSize, devCompactOffsets );

	thrust::inclusive_scan(
			thrust::device_ptr< int32 >( devCompactOffsets )
		,	thrust::device_ptr< int32 >( devCompactOffsets+layerSize )
		,	thrust::device_ptr< int32 >( devCompactOffsets )
	);

#ifdef _DEBUG
	{
		int32* offsetsCPU;
		allocHost( &offsetsCPU, layerSize );
		copyToHost( offsetsCPU, devCompactOffsets, layerSize );
		
		free( offsetsCPU );	
	}
#endif

	int32 compactedCount; 
	copyToHost( &compactedCount, devCompactOffsets + layerSize - 1, 1 );

	// all compacted to NULL - layer completely has no splits!
	if( compactedCount == layerSize )
	{
 		freeDevice( devCompactOffsets );
 		freeDevice( *layer );
 
 		*layer = 0;
 		return 0;

	}// all splits preformed (no gaps exists)
	else if( compactedCount <= 0 )
	{
		freeDevice( devCompactOffsets );
		return layerSize;
	}

	compactedCount = layerSize - compactedCount;
	Split* compactedLayer;
	allocDevice( &compactedLayer, compactedCount );

	compactSplitLayer<<< grid,block >>>( *layer, layerSize, devCompactOffsets, compactedLayer );

	freeDevice( devCompactOffsets );
	freeDevice( *layer );

	*layer = compactedLayer;
	return compactedCount;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__device__ __inline void checkTrigIndexing( const RLE & rle, const int32 splitId )
{
#ifdef _DEBUG
	DEVICE_PTE_ASSERT( splitId < rle.codedCount, __FILE__, __LINE__ );
	
	RLE::Coded coded = rle.coded[ splitId ];
	
	DEVICE_PTE_ASSERT( coded.id + coded.count <= rle.mcCount, __FILE__, __LINE__ );
	DEVICE_PTE_ASSERT( coded.count > 0, __FILE__, __LINE__ );
#endif
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__global__ void extractAABB_forOneNode( const RLE rle, const BoundedTriangle* baseAdr, const int32* trigIds, TNode* nodes, const int32 nodesCount )
{
	const int32 id = blockIdx.x * blockDim.x + threadIdx.x;
	if ( id >= nodesCount )
		return;

	if( ! nodes[id].bounds.isRaw() )
		return;
	
	int32 beginIds = nodes[id].startTrigIdId;
	const int32 endIds = nodes[id].endTrigIdId;

	DEVICE_PTE_ASSERT( beginIds <= endIds, __FILE__, __LINE__ );

#ifdef _DEBUG
	int32 count = endIds-beginIds+1;
	if( count > 6 )
	{
		char buff1[40], buff2[40], buff3[40];
				
		printDeviceInt32(buff1,beginIds);
		printDeviceInt32(buff2,endIds);
		printDeviceInt32(buff3,count);
		printf(
				"%s%s%s%s%s%s%s"
			,	"\ndetected long chain :: extractAABB_forOneNode "
			,	buff3
			,	":= "
			,	buff1
			,	"--"
			,	buff2
			,	" ;\n"
		);
	}
#endif

	DEVICE_PTE_ASSERT( beginIds <= endIds && beginIds >= 0, __FILE__, __LINE__ );

	AABB bound = ( baseAdr + trigIds[ beginIds ] )->box;
	++ beginIds;
	
	while( beginIds <= endIds )
	{
		bound.mergeWith( (baseAdr + trigIds[ beginIds ] )->box );
		++beginIds;
	}

	nodes[id].bounds = bound;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


//modify parentNodes - throught merging AABB by children
__global__ void mergeAABB_forParentNode( const Split* childLayers, const TNode* childNodes, const int32 childNodesCount, TNode* parentNodes, const int32 parentNodesCount )
{
	const int32 id = blockIdx.x * blockDim.x + threadIdx.x;
	const int32 id0 = 2 * id;
	const int32 id1 = id0 + 1;

	// id :  0 1 2 3 4  5  6    indexing parentNodes, childLayers(Split is binar bounds)

	// id0:  0 2 4 6 8 10 12    indexing childNodes

	// id1:  1 3 5 7 9 11 13    indexing childNodes

	DEVICE_PTE_ASSERT( childNodesCount % 2 == 0, __FILE__, __LINE__ );
	DEVICE_PTE_ASSERT( parentNodesCount % 2 == 0 || parentNodesCount == 1, __FILE__, __LINE__ );

	if ( id0 >= childNodesCount )
		return;
	
	DEVICE_PTE_ASSERT( id0 < childNodesCount, __FILE__, __LINE__ );
	DEVICE_PTE_ASSERT( id1 < childNodesCount, __FILE__, __LINE__ );

	DEVICE_PTE_ASSERT( ! childNodes[ id0 ].bounds.isRaw(), __FILE__, __LINE__ );
	DEVICE_PTE_ASSERT( ! childNodes[ id1 ].bounds.isRaw(), __FILE__, __LINE__ );
	
	const int32 parentNodeId = childLayers[ id0 ].parentId;

	DEVICE_PTE_ASSERT( parentNodeId == childLayers[ id1 ].parentId, __FILE__, __LINE__ );
	DEVICE_PTE_ASSERT( parentNodeId < parentNodesCount, __FILE__, __LINE__ );
	DEVICE_PTE_ASSERT( parentNodes[ parentNodeId ].bounds.isRaw(), __FILE__, __LINE__ );
	
	AABB bound = childNodes[ id0 ].bounds;
	bound.mergeWith ( childNodes[ id1 ].bounds );	
	
	parentNodes[ parentNodeId ].bounds = bound;	
	parentNodes[ parentNodeId ].left = childNodes + id0;
	parentNodes[ parentNodeId ].right = childNodes + id1;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__global__ void initBVHlayer( const RLE rle, const Split* split, TNode* nodes, const int32 nodesCount )
{
	const int32 id = blockIdx.x * blockDim.x + threadIdx.x;
	if ( id >= nodesCount )
		return;

	const Split p = split[id];
	nodes[id].bounds.makeBVHRawAABB();

	DEVICE_PTE_ASSERT( p.parentId >= 0, __FILE__, __LINE__ );
		
	checkTrigIndexing( rle, p.min );
	checkTrigIndexing( rle, p.max );

	nodes[id].startTrigIdId = rle.coded[ p.min ].id;

	const RLE::Coded upperBound = rle.coded[ p.max ];
	nodes[id].endTrigIdId = upperBound.id + upperBound.count - 1;
		
	DEVICE_PTE_ASSERT( nodes[id].startTrigIdId <= nodes[id].endTrigIdId, __FILE__, __LINE__ );

	nodes[id].left = 0;
	nodes[id].right = 0;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void checkDevNodes( const HLBVH& bvh )
{
#ifdef _DEBUG
	HLBVH test;
	for( int i=0; i<MAX_TREE_DEPTH; i++ )
	{
		if( bvh.nodeLayerSizes[i] <= 0 )
			break;

		test.nodeLayerSizes[i] = bvh.nodeLayerSizes[i];
		allocHost< TNode >( &( test.nodeLayers[i]), bvh.nodeLayerSizes[i] );
		copyToHost< TNode >( test.nodeLayers[i], bvh.nodeLayers[i], bvh.nodeLayerSizes[i] );
	}

	int bp = 0;
	for( int i=0; i<MAX_TREE_DEPTH; i++ )
	{
		freeHost( test.nodeLayers[i] );
		test.nodeLayers[i] = 0;
	}
#endif
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__host__ inline void buildNodes(
		Split** layers
	,	const int32* layersSize
	,	const BoundedTriangle* baseTrigAdr
	,	const int32* trigIds
	,	const int32 trigsCount
	,	HLBVH& bvh
	,	const int32 warpSize
	,	const RLE& rle
)
{
	CPU_PTE_ASSERT( layersSize[0] > 0, __FILE__, __LINE__ );
	CPU_PTE_ASSERT( layersSize[1] > 0, __FILE__, __LINE__ );
	
	int32 trueDepth = 0;
	for(
			int i = 0
		;	i < MAX_TREE_DEPTH
		;	i++
	)
	{
		const int32 nodesCount = layersSize[i];
		if( nodesCount <= 0 )
		{
			bvh.nodeLayerSizes[i] = 0;
			bvh.nodeLayers[i] = 0;
			break;
		}

		allocDevice< TNode >( &(bvh.nodeLayers[i]), nodesCount );
		bvh.nodeLayerSizes[i] = nodesCount;

		uint32 grid = ceilf( float( nodesCount ) / warpSize );
		initBVHlayer<<< grid, warpSize >>>( rle, layers[i], bvh.nodeLayers[i], nodesCount );

		++trueDepth;
	}	
	
	checkDevNodes(bvh);

	for( int i = MAX_TREE_DEPTH-1; i>=0; i-- )
	{
		if( bvh.nodeLayerSizes[i]<=0 )
			continue;

		uint32 grid = ceilf( float( bvh.nodeLayerSizes[i] ) / warpSize );
		extractAABB_forOneNode<<< grid, warpSize >>>( rle, baseTrigAdr, trigIds, bvh.nodeLayers[i], bvh.nodeLayerSizes[i] );

		//check init nodes
		//checkDevNodes(bvh);

		CPU_PTE_ASSERT( layers[i], __FILE__, __LINE__ );
		if( i >= 1 )
		{
			mergeAABB_forParentNode<<< grid, warpSize >>>( layers[i], bvh.nodeLayers[i], bvh.nodeLayerSizes[i], bvh.nodeLayers[i-1], bvh.nodeLayerSizes[i-1] );

			checkDevNodes(bvh);
		}
	}

} // buildNodes


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__host__ void processSimpleTree( TreeData& tree )
{
	allocDevice< TNode >( &(tree.bvh.nodeLayers[0]), 1 );
	tree.bvh.nodeLayerSizes[0] = 1;
	tree.bvh.nodeLayerSizes[1] = 0;
	tree.bvh.treeDepth = 1;

	TNode root;
	root.bounds = tree.totalBounds;
	
	root.startTrigIdId = 0;
	root.endTrigIdId = tree.size - 1;
	
	root.right = 0;
	root.left = 0;
	
	copyToDevice( tree.bvh.nodeLayers[0], &root, 1 );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__host__ inline void makeSplits( TreeData& tree, RLE& rle, const int32 warpSize )
{
	Split* layers[MAX_TREE_DEPTH];
	int32 layersSize[MAX_TREE_DEPTH];

	ZeroMemory( layers, sizeof(Split*)*(MAX_TREE_DEPTH) );
	ZeroMemory( layersSize, sizeof(int32)*(MAX_TREE_DEPTH) );
	
	if( rle.codedCount <= 4 )
	{
		processSimpleTree( tree );
		return;
	}
	
	Split origin;
	origin.parentId = 0;
	origin.min = 0;
	origin.max = rle.codedCount-1;
	
	allocDevice< Split >( &(layers[0]), 1 );
	copyToDevice( layers[0], &origin, 1 );
	layersSize[0] = 1;
	
	tree.bvh.treeDepth = MAX_TREE_DEPTH;
	DWORD ts1 = timeGetTime();
	for(
			int32 parentLayerSize = 1,	newLayerSize = 2,	i = 0
		;	i < MAX_TREE_DEPTH-1
		;	i++
	)
	{
		CPU_PTE_ASSERT( layersSize[i+1] % 2 == 0, __FILE__, __LINE__ );
		allocDevice< Split >( &(layers[i+1]), newLayerSize );
		uint32 grid = ceilf(float( newLayerSize )/warpSize);
		splitBoundLayer<<< grid, warpSize >>>( rle, layers[i], parentLayerSize, layers[i+1],i );
		
#ifdef _DEBUG
		{
			CPU_PTE_ASSERT( parentLayerSize % 2==0 || parentLayerSize == 1, __FILE__, __LINE__ );
			CPU_PTE_ASSERT( newLayerSize % 2==0, __FILE__, __LINE__ );

			Split* splitCPU1 = (Split*)malloc( sizeof(Split) * parentLayerSize );
			copyToHost( splitCPU1, layers[i], parentLayerSize );
			
			Split* splitCPU2 = (Split*)malloc( sizeof(Split) * newLayerSize );
			copyToHost( splitCPU2, layers[i+1], newLayerSize );

			//check for correct parent-id generation
			for( int k=0; k<newLayerSize; k++ )
			{
				int32 id = k/2;
				CPU_PTE_ASSERT( splitCPU2[k].parentId == id || splitCPU2[k].parentId < 0, __FILE__, __LINE__ );
			}

			free(splitCPU1);
			free(splitCPU2);
		}
#endif // _DEBUG

		
		newLayerSize = compactSplitLayers( &(layers[i+1]), newLayerSize, warpSize );
		
		if( newLayerSize <= 0 )
		{
			tree.bvh.treeDepth = i+1;
			break;
		}

#ifdef _DEBUG
		Split* splitCPU1 = (Split*)malloc( sizeof(Split) * parentLayerSize );
		copyToHost( splitCPU1, layers[i], parentLayerSize );

		Split* splitCPU2 = (Split*)malloc( sizeof(Split) * newLayerSize );
		copyToHost( splitCPU2, layers[i+1], newLayerSize );

		//check for correct parent-id generation
		int32 parityCheck = 0;
		for( int32 k=0; k<newLayerSize-1; k++ )
		{
			CPU_PTE_ASSERT( splitCPU2[k].parentId <= splitCPU2[k+1].parentId, __FILE__, __LINE__ );
			
			if( splitCPU2[k].parentId == splitCPU2[k+1].parentId )
				parityCheck += 1;
			else
				parityCheck -= 1;

			CPU_PTE_ASSERT( parityCheck < 2, __FILE__, __LINE__ );

			if( k%2 == 0 && k+1 < newLayerSize )
				CPU_PTE_ASSERT( splitCPU2[k].parentId == splitCPU2[k+1].parentId, __FILE__, __LINE__ );
		}

		free(splitCPU1);
		free(splitCPU2);
#endif // _DEBUG

		//do not multiply newLayerSize*2!
		//it's done in compactSplitLayers!
		
		//after compaction generated new size!
		layersSize[i+1] = newLayerSize;

		parentLayerSize = newLayerSize;
		newLayerSize *= 2;

	} // for

	//and now make a big extraction of splitts!
#ifdef _DEBUG
	{
		Split* SplitCPU[MAX_TREE_DEPTH];
		ZeroMemory( SplitCPU, sizeof(Split*)*(MAX_TREE_DEPTH) );

		for( int i=0; i<MAX_TREE_DEPTH; i++ )
		{
			if( layersSize[i] <= 0 )
				break;

			allocHost( &SplitCPU[i], layersSize[i] );
			copyToHost( SplitCPU[i], layers[i], layersSize[i] );
		}

		int bp = 0;
		for( int i=0; i<MAX_TREE_DEPTH; i++ )
		{
			freeHost( SplitCPU[i] );
			SplitCPU[i] = 0;
		}
	}
#endif

	DWORD ts2 = timeGetTime();

	std::cout << "bottom->root split generation time: \t\t" << ts2 - ts1 << " ms" << std::endl;
	
	std::cout << "tree depth: \t\t\t\t\t" << tree.bvh.treeDepth <<" d"<< std::endl;
	DWORD tb1 = timeGetTime();
	buildNodes( layers, layersSize, tree.devTrigs, tree.devTrigIds, tree.size, tree.bvh, warpSize, rle );
	DWORD tb2 = timeGetTime();

	std::cout << "bottom->root node build time: \t\t\t" << tb2 - tb1 << " ms" << std::endl;
	
	for( int i=0; i<MAX_TREE_DEPTH; ++i)
	{
		freeDevice( layers[i] );
		layers[i] = 0;
	}
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__host__ void makeTree( TreeData& result, int32 warpSize )
{		
	dim3 grid( ceilf( float(result.size) / warpSize ), 1, 1 );
	dim3 block( warpSize, 1, 1 );

	MMBounds zeroBounds;
	zeroBounds.min = Vec3( MAX_FLOAT );
	zeroBounds.max = Vec3( MIN_FLOAT );

	MMBoundsExtractor extractor( result.devTrigs );
	MMBounds bounds = thrust::transform_reduce(
			thrust::device_ptr< int32 >( result.devTrigIds )
		,	thrust::device_ptr< int32 >( result.devTrigIds + result.size )
		,	extractor
		,	zeroBounds
		,	MMBoundsComposer()
	);
	result.totalBounds.set( bounds.min, bounds.max );
		
	DWORD ts1 = timeGetTime();
	GenerateMortonCode<<< grid, block >>>(
			result.size
		,	result.codes
		,	result.devTrigs
		,	result.devTrigIds
		,	( bounds.max - bounds.min ) * 0.5f
		,	( bounds.max + bounds.min ) * 0.5f
	);
			
	thrust::sort_by_key(
			thrust::device_ptr< MCodes >( result.codes )
		,	thrust::device_ptr< MCodes >( result.codes + result.size )
		,	thrust::device_ptr< int32 >( result.devTrigIds )
	);

	DWORD ts2 = timeGetTime();
	std::cout << "TrigId + MC (key) sort time: \t\t\t"<< ts2-ts1 << " ms"<< std::endl;

#ifdef _DEBUG
	MCodes* codesCPU;
	allocHost( &codesCPU, result.size );
	copyToHost( codesCPU, result.codes, result.size );

	for( int i=0; i<result.size-1; ++i )
	{
		// check  whether codes are sorted
		CPU_PTE_ASSERT( codesCPU[i] <= codesCPU[i+1], __FILE__, __LINE__ );
	}

	free( codesCPU );
#endif

	DWORD trlec1 = timeGetTime();
	RLE rle( result.codes, result.size );
	RLEprocessMC( rle, warpSize );
	DWORD trlec2 = timeGetTime();

	std::cout << "MC-RLE compact ratio: \t\t\t\t" << float(rle.mcCount) / rle.codedCount << " r " << std::endl;
	std::cout << "MC-RLE compact time: \t\t\t\t"<< trlec2 - trlec1 << " ms" << std::endl;

	DWORD tsb1 = timeGetTime();
	makeSplits( result, rle, warpSize );	
	DWORD tsb2 = timeGetTime();

	std::cout << "Split\\tree generation time: \t\t\t"<< tsb2 - tsb1 << " ms" << std::endl;

	rle.free();
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__host__ void buildHLBVH( TreeData& result, const std::vector< Triangle >& trigs, int32 warpSize )
{
	std::cout << "trigs count: \t\t\t\t\t" << trigs.size() << std::endl;
	
	DWORD t1 = timeGetTime();
	result.init( &(trigs[0]), trigs.size(), warpSize );
	DWORD t2 = timeGetTime();

	makeTree( result, warpSize );
	DWORD t3 = timeGetTime();
	
	std::cout << "trigs copy to GPU: "<< t2-t1 << " ms, build time:"<< t3-t2 << " ms" << std::endl << std::endl;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace PTE

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/