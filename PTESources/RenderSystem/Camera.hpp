#pragma once

#include "PTESources/Math/Vector3.hpp"
#include "PTESources/Math/Quaternion.hpp"
#include "PTESources/RenderSystem/RenderBuffer.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


class Camera
{
private:

	Camera( Camera & );
	void operator = ( Camera & );

public:

	void zeroLayer( int layer, float zeroSquareDepth );
	void zeroLayerIfCameraStateChanged( int layer, float zeroSquareDepth );

	Camera();
	~Camera(){}

	Ray getRay( const int x, const int y, const int currentAASample, const int maxAASamples );

	void resetDirection();

	void updateCameraAttributes ( const Vec3& in_pos, const Vec3& in_upVec, const Vec3& in_dir, const float in_FOV );

	void addScreenColor( int layer, int x, int y, const Color& color, float depth );

	void setLayersCount( int count );

	void setParams(	int nbPixelsX, int nbPixelsY, float geomCameraSize );

	void* getRGBAas8888( int layer );

	RenderBuffer& getPrimeRenderBuffer() { return *( mRenderBufferStack[ mPrimeLayer ] ); }
	int getPrimeRenderBufferLayer() const { return mPrimeLayer; }
	int getLayersCount() const { return mRenderBufferStack.getLayersCount(); }
	

	//BLUR\DOF
public:

	float mSquareDistFocalPlane;
	float mPixelDOFDiskRatio;

private:

	void randCubicRayOffset( float& x, float& y, const int layerId, const int maxLayers );
	void cubicRayOffset( float& x, float& y, const int layerId, const int maxLayers );

private:	
		
	bool mCameraStateChanged;

public:
	
	RenderBufferStack mRenderBufferStack;
	int mNbPixelsX;
	int mNbPixelsY;
	float mScreenColorsThroughPathCount;

private:

    float m_aspectRatio;
	float m_geomCameraSize;
	
	float m_FOV;

	// camera space orientation
	Vec3 m_focusPoint;

	Vec3 m_frameOriginPos;
	Vec3 m_frameAxisX;
	Vec3 m_frameAxisY;


	// world space orientation
	Vec3 m_pos;
	Vec3 m_dir;
	Vec3 m_up;	

	int mPrimeLayer;
}; // class Camera


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
