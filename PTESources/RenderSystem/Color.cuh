
#pragma once

#include "PTESources/CudaInclude.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

struct ColorA;
struct Color;

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

// TRUE color - doesn't use alpha!
struct Color
{
public:	

	__host__ __device__ __inline bool isZero() const 
	{
		return r + g + b < 0.0001f;
	}

	__host__ __device__ __inline Color( const ColorA& c );

	__host__ __device__ __inline Color( const Color& c)
	{
		r = c.r;
		g = c.g;
		b = c.b;
	}
	__host__ __device__ __inline Color()
	{}

	__host__ __device__ __inline Color( float _r, float _g, float _b )
	{
		r = _r;
		g = _g;
		b = _b;
	}

	__host__ __device__ __inline void set( float _r, float _g, float _b )
	{
		r = _r;
		g = _g;
		b = _b;
	}

	__host__ __device__ __inline Color( uchar4 ch )
	{
		this->r = ch.x/255.f;
		this->g = ch.y/255.f;
		this->b = ch.z/255.f;
	}


	__host__ __device__ __inline void operator += ( const Color& c )
	{
		r += c.r;
		g += c.g;
		b += c.b;
	}
	
	__host__ __device__ __inline Color operator += ( const Color& c ) const
	{
		return Color( r+c.r, g+c.g, b+c.b );
	}

	__host__ __device__ __inline  Color operator - ( const Color& c )
	{
		return Color( r - c.r, g - c.g, b - c.b );
	}

	__host__ __device__ __inline float derivationR ( const Color& c )
	{
		return fabs( r - c.r );
	}

	__host__ __device__ __inline float derivationG ( const Color& c )
	{
		return fabs( g - c.g );
	}

	__host__ __device__ __inline float derivationB ( const Color& c )
	{
		return fabs( b - c.b );
	}

	__host__ __device__ __inline void operator /= ( float f )
	{
		float k = 1.f / f;
		r *= k;
		g *= k;
		b *= k;
	}

	__host__ __device__ __inline Color operator / ( float c ) const
	{
		float k = 1 / c;
		return Color ( r*k, g*k, b*k );
	}

	__host__ __device__ __inline void operator *= ( float f )
	{
		r *= f;
		g *= f;
		b *= f;
	}

	__host__ __device__ __inline Color operator * ( const Color& c ) const
	{
		return Color( r*c.r, g*c.g, b*c.b );		
	}

	__host__ __device__ __inline Color operator * ( float f ) const
	{
		return Color( r*f, g*f, b*f );
	}

	__host__ __device__ __inline void setZero()
	{
		r=0; g=0; b=0;
	}

public:

	float r, g, b;

}; // class Color


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


struct ColorA
	:	public Color
{

	__host__ __device__ __inline bool isZero() const
	{
		return r + g + b < 0.0001f;
	}

	__host__ __device__ __inline ColorA(){}

	__host__ __device__ __inline ColorA( const Color& c, float a )
		:	alpha(a)
		,	Color(c)
	{}

	__host__ __device__ __inline ColorA( float r, float g, float b, float a )
		:	Color( r, g, b )
		,	alpha(a)
	{
	}

	__host__ __device__ __inline ColorA( uchar4 ch )
	{
		r = ch.x/255.f;
		g = ch.y/255.f;
		b = ch.z/255.f;
		alpha = ch.w/255.f;
	}

	__host__ __device__ __inline ColorA operator *( float intescity )
	{
		ColorA r = *this;
		r.r *= intescity;
		r.g *= intescity;
		r.b *= intescity;
		return r;
	}

	__host__ __device__ __inline void setZero()
	{
		r=0; g=0; b=0; alpha=0;
	}

public:

	float alpha;
};


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__host__ __device__ __inline Color::Color( const ColorA& c)
{
	r = c.r;
	g = c.g;
	b = c.b;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
