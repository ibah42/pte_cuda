#pragma once

#include "PTESources/CudaInclude.hpp"
#include "PTESources/RenderSystem/Texture.cuh"
#include "PTESources/RenderSystem/Color.cuh"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


struct VertexMaterial
{
	__device__ void normalize()
	{
		glossReflect = clamp( 0, 1, glossReflect );
		glossRefract = clamp( 0, 1, glossRefract );

		float d = 1.f / sqrt( reflectK*reflectK + refractK*refractK + diffK*diffK );
		reflectK *= d;
		refractK *= d;
		diffK *= d;
	}

public:

	Vec3 vertNormal;
	float reflectK, refractK, diffK;
	float ior;
	float glossReflect, glossRefract;

	// Color-filters
	Color refractC, reflectC;

	// true-colors
	Color diffC, emitC;
};


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


struct SurfaceMaterial
{
	__host__ SurfaceMaterial( NormalTextureVec3* ntex, ColorTextureUchar4* diffTex, EmitTextureUchar4* emitTex )
		:	mNormalTex(ntex)
		,	mDiffTex(diffTex)
		,	mEmitTex(emitTex)
	{
		reflectC = Color( 1, 1, 1 );
		refractC = Color( 1, 1, 1 );
		reflectK = 0;
		refractK = 0;
		ior = 1;
		glossReflect = 1;
		glossRefract = 1;
	}
	

	__device__ void getVertexMaterial( const int32 separator, VertexMaterial& out, float u, float v, const Vec3& usedTrigNormal ) const
	{
		if( mEmitTex.isInited() )
		{
			switch( separator )
			{
				case 0: out.emitC = mEmitTex.get( u, v ); break;
				case 1: out.emitC = mEmitTex.get( u, v ); break;
				case 2: out.emitC = mEmitTex.get( u, v ); break;
				case 3: out.emitC = mEmitTex.get( u, v ); break;
			}
		}
		else
			out.emitC.setZero();
		 
		ColorA diffColor;

		switch( separator )
		{
			case 0: diffColor = mDiffTex.get( u, v ); break;
			case 1: diffColor = mDiffTex.get( u, v ); break;
			case 2: diffColor = mDiffTex.get( u, v ); break;
			case 3: diffColor = mDiffTex.get( u, v ); break;
		}
		
		out.diffC = diffColor;
		out.diffK = diffColor.alpha;

		if ( mNormalTex.isInited() )
		{
			switch( separator )
			{
				case 0: out.vertNormal = mNormalTex.get( u, v, usedTrigNormal ); break;
				case 1: out.vertNormal = mNormalTex.get( u, v, usedTrigNormal ); break;
				case 2: out.vertNormal = mNormalTex.get( u, v, usedTrigNormal ); break;
				case 3: out.vertNormal = mNormalTex.get( u, v, usedTrigNormal ); break;
			}			
		}
		else
			out.vertNormal = usedTrigNormal;

		out.glossReflect = glossReflect;
		out.glossRefract = glossRefract;

		out.ior = ior;
		out.reflectK = reflectK;
		out.refractK = refractK;
		
		out.reflectC = reflectC;
		out.refractC = refractC;

		out.normalize();
	}

public:

	NormalTextureVec3 mNormalTex;
	ColorTextureUchar4 mDiffTex;
	EmitTextureUchar4 mEmitTex;

	float reflectK, refractK;
	float ior;
	float glossReflect, glossRefract;

	// Color-filters
	Color refractC, reflectC;	
};


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/