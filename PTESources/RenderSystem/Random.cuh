#pragma once

#include "PTESources/CudaInclude.hpp"
#include "PTESources/Math/Utility.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


// can be used sequential at one kernel-call at a time!
class Random
{
private:

	static const uint32 WARP_SIZE = 32;
	
public:

	__host__ Random()
		:	states(0), size(0)
	{}
	__host__ Random( int32 processorsCount );
	__host__ void free()
	{
		size = 0;
		freeDevice(states);
		states = 0;
	}
	
	__device__ curandState* getByOverlappingThreadId( const int32 id )
	{
		return states + (id % size);
	}

	__host__ __device__  curandState* getGenerators()	{ return states; }
	__host__ __device__ int32 getSize() const			{ return size; }

private:

	curandState *states;
	int32 size;

}; // class Random


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
