#pragma once

#include "PTESources/RenderSystem/Color.cuh"
#include "PTESources/Math/Vector3.hpp"
#include "PTESources/Math/Utility.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


struct Light
{
	__host__ __device__ Light(  Vec3 v, Color c, float r = 0 )
		:	position( v )
		,	color( c )
		,	radius( r )
	{
		doubleAttenuationDist = 30;
	}

	__device__ Color getResultPowerOnDist( float dist ) const
	{
		dist -= radius;
		if ( dist <= 0 )
			return color;

		float r = clamp( 0.0f, 1.0f, 1 - dist / 2 / doubleAttenuationDist );
		return color * r;
	}

	__device__ Vec3 sampleRandomDirectHemiSphere( curandState* state, const Vec3& rayStart ) const
	{
		Vec3 res( devRand_m1_p1f(state), devRand_m1_p1f(state), devRand_m1_p1f(state) );
		res.voidNormalize();
				
		Vec3 dir = position - rayStart;
		dir.voidNormalize();

		if ( dir.dot( res ) > 0 )
			res = - res;

		res *= radius;
		res += position;
		return res;
	}

public:

	Color color;
	float doubleAttenuationDist;
	Vec3 position;

	// if radius == 0 - no light sampling;
	float radius;

}; // class Light


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
