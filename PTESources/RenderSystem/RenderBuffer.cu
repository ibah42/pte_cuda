
#include "PTESources/RenderSystem/RenderBuffer.cuh"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__host__ void RenderBuffer::swap( RenderBuffer& that )
{
	std::swap( px, that.px );
	std::swap( py, that.py );
	std::swap( devColorBuff, that.devColorBuff );
	std::swap( devSquareDepthBuff, that.devSquareDepthBuff );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__host__ void RenderBuffer::free()
{
	freeDevice(devColorBuff);
	freeDevice(devSquareDepthBuff);
	px=0;
	py=0;	
	devColorBuff = 0;
	devSquareDepthBuff=0;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__host__ void RenderBuffer::resize( int x, int y )
{
	px = x;
	py = y;
	freeDevice(devColorBuff);
	freeDevice(devSquareDepthBuff);
		
	allocDevice( &devColorBuff, px*py );		
	allocDevice( &devSquareDepthBuff, px*py );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__host__ bool RenderBufferStack::resize( int x, int y )
{
	assert( x * y > 0 );
		
	if ( px != x || py != y )
	{
		for( int i=0; i<RBS_LAYERS_COUNT; i++)
		{
			renderBufferStack[i].resize(x,y);
		}

		px = x;
		py = y;
		return true;
	}

	return false;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__host__ void RenderBufferStack::swapBuffers( int layer1, int layer2 )
{		
	renderBufferStack[ layer1 ].swap( renderBufferStack[ layer2 ] );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__host__ void RenderBufferStack::zeroLayer( int l )
{
	assert( l<RBS_LAYERS_COUNT );
	cudaMemset( renderBufferStack[l].devColorBuff, 0, px*py*sizeof(Color) );
	cudaMemset( renderBufferStack[l].devSquareDepthBuff, 0, px*py*sizeof(float) );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
