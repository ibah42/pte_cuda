#include "PTESources/PCH.hpp"
#include "PTESources/RenderSystem/Camera.hpp"
#include "PTESources/RenderSystem/Shaders.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


Camera::Camera()
{
	mCameraStateChanged = true;
	mPrimeLayer = 0;
	mScreenColorsThroughPathCount = 0;
		
	mNbPixelsX = 100;
	mNbPixelsY = 100;
	
	resetDirection();
	
	setParams( 256, 256, 1 );
	setLayersCount( 4 );

	mPixelDOFDiskRatio = 0;
	mSquareDistFocalPlane = 1000*1000;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void 
Camera::setParams( int nbPixelsX, int nbPixelsY, float geomCameraSize )
{
	if( nbPixelsX < 1 )
		nbPixelsX = 1;

	if ( nbPixelsY < 1 )
		nbPixelsY = 1;

	if ( mNbPixelsY != nbPixelsY || mNbPixelsX != nbPixelsX || m_geomCameraSize != geomCameraSize )
		mCameraStateChanged = true;

	mNbPixelsX = nbPixelsX;
	mNbPixelsY = nbPixelsY;

	m_geomCameraSize = geomCameraSize;
	m_aspectRatio = nbPixelsX / float( nbPixelsY );

	int layersCount = mRenderBufferStack.getLayersCount();
	if ( layersCount <= 0 )
		layersCount = 1;

	setLayersCount( layersCount );
	
} // Camera::setParams


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void 
Camera::setLayersCount( int count )
{
	assert( count > 0 );
	if ( mRenderBufferStack.resize( count, mNbPixelsX, mNbPixelsY ) )
		mCameraStateChanged = true;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void Camera::zeroLayerIfCameraStateChanged( int layer, float zeroSquareDepth )
{
	if ( mCameraStateChanged )
		zeroLayer( layer, zeroSquareDepth );

	mCameraStateChanged = false;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void Camera::zeroLayer( int layer, float zeroSquareDepth )
{
	mScreenColorsThroughPathCount = 0;
	RenderBuffer& buffer = * mRenderBufferStack[layer];
	ZeroMemory ( buffer.mColorBuff, buffer.xSize()* buffer.ySize() * sizeof(Color) );
		
	#pragma omp parallel for
	for( int y=0; y<mNbPixelsY; ++y )
	{
		for( int x=0; x<mNbPixelsX; ++x )
		{
			* buffer.squareDepth( x,y ) = zeroSquareDepth;
		}
	}
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void Camera::resetDirection()
{
	m_dir.set( 0,0,1 );
	m_up.set( 0,1,0 );
}


// /*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
// 
// 
// void
// Camera::setScreenColor( int layer, int x, int y, const Color& color )
// {
// 	assert (
// 			x >= 0
// 		&&	x < m_nbPixelsX
// 		&&	y >= 0
// 		&&	y < m_nbPixelsY
// 
// 		&&	layer >= 0
// 		&&	layer < m_renderBufferStack.getLayersCount()
// 	);
// 
// 	*( ( *m_renderBufferStack[ layer ] )( x, y ) ) = color;
// }


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
Camera::addScreenColor( int layer, int x, int y, const Color& color, float in_depth )
{
	assert (
			x >= 0
		&&	x < mNbPixelsX
		&&	y >= 0
		&&	y < mNbPixelsY

		&&	layer >= 0
		&&	layer < mRenderBufferStack.getLayersCount()
	);

	float k = mScreenColorsThroughPathCount / ( mScreenColorsThroughPathCount + 1 );

	Color& source = *( ( *mRenderBufferStack[ layer ] ).color( x, y ) );
	float& depth = *( ( *mRenderBufferStack[ layer ] ).squareDepth( x, y ) );
	source = source *mScreenColorsThroughPathCount;
//	depth = depth * k;

	//float nk =  / ( mScreenColorsThroughPathCount+1 );
	source += color ;
	source /= mScreenColorsThroughPathCount+1;
	//depth += in_depth * nk;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void 
Camera::updateCameraAttributes ( const Vec3& in_pos, const Vec3& in_upVec, const Vec3& in_dir, const float in_FOV  )
{
	if ( 
			!	in_pos.equal( m_pos )
		||	!	in_upVec.equal( m_up )
		||	!	in_dir.equal( m_dir )
		||		fabs( in_FOV - m_FOV ) > 0.001f
	)
	{
		mCameraStateChanged = true;
		
		m_pos = in_pos;
		m_up = in_upVec.getNormalized();
		m_dir = in_dir.getNormalized();
		m_FOV = in_FOV;

		//m_aspectRatio=1;
		// recalc frame coordinate system
		m_frameAxisX = m_dir.cross ( m_up ).getNormalized ();
		m_frameAxisY = m_frameAxisX.cross ( m_dir ).getNormalized ();

		// get frame space system origin point
		m_frameOriginPos = m_pos - ( m_frameAxisX * ( m_aspectRatio * 0.5f ) );
		// offset from center by half of frame_size_X in direction of frame_axis_X
		
		m_frameOriginPos -= m_frameAxisY * (  0.5f ); // also offset by Y

		// recalc focus point based on given FOV
		//m_focusPoint = ...

		Angle a = Angle::buildDegrees( in_FOV * 0.5f );
		float f = 0.5f * m_geomCameraSize / a.tan();
	
		m_focusPoint = m_pos - f * m_dir;
	}

} // Camera::updateCameraAttributes


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


PTE::Ray Camera::getRay( const int x, const int y, const int currentAASample, const int maxAASamples )
{
	assert (
			x >= 0
		&&	x < mNbPixelsX
		&&	y >= 0
		&&	y < mNbPixelsY
		&&	m_aspectRatio > 0
		&&	m_aspectRatio < 999999.f
	);

	float xr = float(x);
	float yr = float(y);

	randCubicRayOffset( xr, yr, currentAASample, maxAASamples );
	
	float AARasterPosX = xr / mNbPixelsX * m_aspectRatio * m_geomCameraSize;
	float AARasterPosY = yr / mNbPixelsY * m_geomCameraSize;
	Vec3 projectionAARayStartWorldPos = m_frameOriginPos + ( m_frameAxisX*AARasterPosX ) + ( m_frameAxisY*AARasterPosY );
		
	Vec3 AARayDir = ( projectionAARayStartWorldPos - m_focusPoint ).getNormalized();

	if ( mSquareDistFocalPlane <= 0 || mPixelDOFDiskRatio <= 0 )
	{		
		return Ray( projectionAARayStartWorldPos, AARayDir, Ray::camera );
	}
	else
	{
		float angle = randf( 0, TwoPi );
		float ratio = mPixelDOFDiskRatio* (mNbPixelsX+mNbPixelsY) * 0.5f * randf_11();
		xr += ratio * cos( angle );
		yr += ratio * sin( angle );

		float DOFRasterPosX = xr / mNbPixelsX * m_aspectRatio * m_geomCameraSize;
		float DOFRasterPosY = yr / mNbPixelsY * m_geomCameraSize;

		Vec3 projectionDOFRayStartWorldPos = m_frameOriginPos + ( m_frameAxisX*DOFRasterPosX ) + ( m_frameAxisY*DOFRasterPosY );

		Vec3 DOFdir = projectionAARayStartWorldPos + AARayDir * sqrt( mSquareDistFocalPlane );
		DOFdir -= projectionDOFRayStartWorldPos;
		DOFdir.voidNormalize();
	
		return Ray( projectionDOFRayStartWorldPos, DOFdir, Ray::camera );
	}
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void* Camera::getRGBAas8888( int _layer )
{
	char* result = ( char* ) malloc( 3 * mNbPixelsX * mNbPixelsY );
	int size = mNbPixelsX * mNbPixelsY;
	
	#pragma omp parallel for
	for( int i = 0; i < size; ++i )
	{
		char* current = result + i * 3;

		Color* currentoColor = ( *mRenderBufferStack[ _layer ] ).color( i );
		
		*current = unsigned char( currentoColor->r * 255 );
		++current;
		
		*current = unsigned char( currentoColor->g * 255 );
		++current;
		
		*current = unsigned char( currentoColor->b * 255 );
		++current;
	}

	return result;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


float rand_m1_or_p1()
{
	if ( ::rand()%2 )
		return -1.f;
	else
		return 1.f;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#define X4_STRIDE 0.33f
#define X16_STRIDE_3 0.33f
#define X16_STRIDE_1 0.11f

#define X64_STRIDE_1 0.33f / 7.f
#define X64_STRIDE_2 3*X64_STRIDE_1
#define X64_STRIDE_3 5*X64_STRIDE_1
#define X64_STRIDE_4 7*X64_STRIDE_1


void
Camera::randCubicRayOffset( float& x, float& y, const int layerId, const int maxLayers )
{
	
#define DOF_DEVIATION_X1	0.45f
#define DOF_DEVIATION_X4	X4_STRIDE * 0.75f
#define DOF_DEVIATION_X16	X16_STRIDE_1 * 0.75f
#define DOF_DEVIATION_X64	X64_STRIDE_1 * 0.75f


	cubicRayOffset( x, y, layerId, maxLayers );
	switch( maxLayers )
	{
		case 1:
			x += DOF_DEVIATION_X1 * randf_11();
			y += DOF_DEVIATION_X1 * randf_11();
			break;

		case 4:
			x += DOF_DEVIATION_X4 * randf_11();
			y += DOF_DEVIATION_X4 * randf_11();
			break;

		case 16:
			x += DOF_DEVIATION_X16 * randf_11();
			y += DOF_DEVIATION_X16 * randf_11();
			break;

		case 64:
			x += DOF_DEVIATION_X64 * randf_11();
			y += DOF_DEVIATION_X64 * randf_11();
			break;
	}
	

	
#undef AA_X4_DEVIATION
#undef AA_X16_DEVIATION
#undef AA_X64_DEVIATION
 
} // randCubicRayOffset


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/



void Camera::cubicRayOffset( float& x, float& y, const int layerId, const int maxLayers )
{
	float phi = Pi / 6.5f;
	switch( maxLayers )
	{
		case 4:
		{
			switch( layerId )
			{
				case 0:	x -= X4_STRIDE*cos( phi ); y -= X4_STRIDE*sin( phi ); break;
				case 1:	x += X4_STRIDE*cos( phi ); y -= X4_STRIDE*sin( phi ); break;
				case 2:	x += X4_STRIDE*cos( phi ); y += X4_STRIDE*sin( phi ); break;
				case 3:	x -= X4_STRIDE*cos( phi ); y += X4_STRIDE*sin( phi ); break;
			}
			break;
		}
		case 16:
		{
 			switch( layerId )
 			{
 				case 0:	x -= X16_STRIDE_3*cos( phi ); y -= X16_STRIDE_3*sin( phi ); break;
 				case 1:	x -= X16_STRIDE_1*cos( phi ); y -= X16_STRIDE_3*sin( phi ); break;
 				case 2:	x += X16_STRIDE_1*cos( phi ); y -= X16_STRIDE_3*sin( phi ); break;
 				case 3:	x += X16_STRIDE_3*cos( phi ); y -= X16_STRIDE_3*sin( phi ); break;
 
 				case 4:	x -= X16_STRIDE_3*cos( phi ); y -= X16_STRIDE_1*sin( phi ); break;
 				case 5:	x -= X16_STRIDE_1*cos( phi ); y -= X16_STRIDE_1*sin( phi ); break;
 				case 6: x += X16_STRIDE_1*cos( phi ); y -= X16_STRIDE_1*sin( phi ); break;
 				case 7:	x += X16_STRIDE_3*cos( phi ); y -= X16_STRIDE_1*sin( phi ); break;
 
 				case 8:	x -= X16_STRIDE_3*cos( phi ); y += X16_STRIDE_1*sin( phi ); break;
 				case 9:	x -= X16_STRIDE_1*cos( phi ); y += X16_STRIDE_1*sin( phi ); break;
 				case 10:x += X16_STRIDE_1*cos( phi ); y += X16_STRIDE_1*sin( phi ); break;
 				case 11:x += X16_STRIDE_3*cos( phi ); y += X16_STRIDE_1*sin( phi ); break;
 
 				case 12:x -= X16_STRIDE_3*cos( phi ); y += X16_STRIDE_3*sin( phi ); break;
 				case 13:x -= X16_STRIDE_1*cos( phi ); y += X16_STRIDE_3*sin( phi ); break;
 				case 14:x += X16_STRIDE_1*cos( phi ); y += X16_STRIDE_3*sin( phi ); break;
 				case 15:x += X16_STRIDE_3*cos( phi ); y += X16_STRIDE_3*sin( phi ); break;
 			}
			break;
		}
		case 64:
		{
			switch( layerId )
			{
				case 0:	x -= X64_STRIDE_4; y -= X64_STRIDE_4; break;
				case 1:	x -= X64_STRIDE_3; y -= X64_STRIDE_4; break;
				case 2:	x -= X64_STRIDE_2; y -= X64_STRIDE_4; break;
				case 3:	x -= X64_STRIDE_1; y -= X64_STRIDE_4; break;
				case 4:	x += X64_STRIDE_1; y -= X64_STRIDE_4; break;
				case 5:	x += X64_STRIDE_2; y -= X64_STRIDE_4; break;
				case 6: x += X64_STRIDE_3; y -= X64_STRIDE_4; break;
				case 7:	x += X64_STRIDE_4; y -= X64_STRIDE_4; break;

				case 8:	x -= X64_STRIDE_4; y -= X64_STRIDE_3; break;
				case 9:	x -= X64_STRIDE_3; y -= X64_STRIDE_3; break;
				case 10:x -= X64_STRIDE_2; y -= X64_STRIDE_3; break;
				case 11:x -= X64_STRIDE_1; y -= X64_STRIDE_3; break;
				case 12:x += X64_STRIDE_1; y -= X64_STRIDE_3; break;
				case 13:x += X64_STRIDE_2; y -= X64_STRIDE_3; break;
				case 14:x += X64_STRIDE_3; y -= X64_STRIDE_3; break;
				case 15:x += X64_STRIDE_4; y -= X64_STRIDE_3; break;

				case 16:x -= X64_STRIDE_4; y -= X64_STRIDE_2; break;
				case 17:x -= X64_STRIDE_3; y -= X64_STRIDE_2; break;
				case 18:x -= X64_STRIDE_2; y -= X64_STRIDE_2; break;
				case 19:x -= X64_STRIDE_1; y -= X64_STRIDE_2; break;
				case 20:x += X64_STRIDE_1; y -= X64_STRIDE_2; break;
				case 21:x += X64_STRIDE_2; y -= X64_STRIDE_2; break;
				case 22:x += X64_STRIDE_3; y -= X64_STRIDE_2; break;
				case 23:x += X64_STRIDE_4; y -= X64_STRIDE_2; break;

				case 24:x -= X64_STRIDE_4; y -= X64_STRIDE_1; break;
				case 25:x -= X64_STRIDE_3; y -= X64_STRIDE_1; break;
				case 26:x -= X64_STRIDE_2; y -= X64_STRIDE_1; break;
				case 27:x -= X64_STRIDE_1; y -= X64_STRIDE_1; break;
				case 28:x += X64_STRIDE_1; y -= X64_STRIDE_1; break;
				case 29:x += X64_STRIDE_2; y -= X64_STRIDE_1; break;
				case 30:x += X64_STRIDE_3; y -= X64_STRIDE_1; break;
				case 31:x += X64_STRIDE_4; y -= X64_STRIDE_1; break;

				case 32:x -= X64_STRIDE_4; y += X64_STRIDE_1; break;
				case 33:x -= X64_STRIDE_3; y += X64_STRIDE_1; break;
				case 34:x -= X64_STRIDE_2; y += X64_STRIDE_1; break;
				case 35:x -= X64_STRIDE_1; y += X64_STRIDE_1; break;
				case 36:x += X64_STRIDE_1; y += X64_STRIDE_1; break;
				case 37:x += X64_STRIDE_2; y += X64_STRIDE_1; break;
				case 38:x += X64_STRIDE_3; y += X64_STRIDE_1; break;
				case 39:x += X64_STRIDE_4; y += X64_STRIDE_1; break;

				case 40:x -= X64_STRIDE_4; y += X64_STRIDE_2; break;
				case 41:x -= X64_STRIDE_3; y += X64_STRIDE_2; break;
				case 42:x -= X64_STRIDE_2; y += X64_STRIDE_2; break;
				case 43:x -= X64_STRIDE_1; y += X64_STRIDE_2; break;
				case 44:x += X64_STRIDE_1; y += X64_STRIDE_2; break;
				case 45:x += X64_STRIDE_2; y += X64_STRIDE_2; break;
				case 46:x += X64_STRIDE_3; y += X64_STRIDE_2; break;
				case 47:x += X64_STRIDE_4; y += X64_STRIDE_2; break;

				case 48:x -= X64_STRIDE_4; y += X64_STRIDE_3; break;
				case 49:x -= X64_STRIDE_3; y += X64_STRIDE_3; break;
				case 50:x -= X64_STRIDE_2; y += X64_STRIDE_3; break;
				case 51:x -= X64_STRIDE_1; y += X64_STRIDE_3; break;
				case 52:x += X64_STRIDE_1; y += X64_STRIDE_3; break;
				case 53:x += X64_STRIDE_2; y += X64_STRIDE_3; break;
				case 54:x += X64_STRIDE_3; y += X64_STRIDE_3; break;
				case 55:x += X64_STRIDE_4; y += X64_STRIDE_3; break;

				case 56:x -= X64_STRIDE_4; y += X64_STRIDE_4; break;
				case 57:x -= X64_STRIDE_3; y += X64_STRIDE_4; break;
				case 58:x -= X64_STRIDE_2; y += X64_STRIDE_4; break;
				case 59:x -= X64_STRIDE_1; y += X64_STRIDE_4; break;
				case 60:x += X64_STRIDE_1; y += X64_STRIDE_4; break;
				case 61:x += X64_STRIDE_2; y += X64_STRIDE_4; break;
				case 62:x += X64_STRIDE_3; y += X64_STRIDE_4; break;
				case 63:x += X64_STRIDE_4; y += X64_STRIDE_4; break;
			}
			break;
		}

		break;
	}
} // rayOffset


#undef X4_STRIDE
#undef X16_STRIDE_1
#undef X16_STRIDE_3

#undef X64_STRIDE_1
#undef X64_STRIDE_2
#undef X64_STRIDE_3
#undef X64_STRIDE_4

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
