
#include "PTESources/RenderSystem/Camera.cuh"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__host__ bool Camera::resize( int32 x, int32 y )
{
	return mRenderBufferStack.resize( x, y );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__host__ CameraUnit::CameraUnit()
{
	mCameraStateChanged = true;
	mPrimeLayer = 0;
		
	mNbPixelsX = 100;
	mNbPixelsY = 100;
	
	resetDirection();
	
	setParams( 400, 400, 1 );

	mPixelDOFDiskRatio = 0.f;
	mSquareDistFocalPlane = 1000*1000;
	renderIterationCounter=0;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__host__ void CameraUnit::setParams( int nbPixelsX, int nbPixelsY, float geomCameraSize )
{
	if( nbPixelsX < 1 )
		nbPixelsX = 1;

	if ( nbPixelsY < 1 )
		nbPixelsY = 1;

	if ( mNbPixelsY != nbPixelsY || mNbPixelsX != nbPixelsX || m_geomCameraSize != geomCameraSize )
		mCameraStateChanged = true;

	mNbPixelsX = nbPixelsX;
	mNbPixelsY = nbPixelsY;

	m_geomCameraSize = geomCameraSize;
	m_aspectRatio = nbPixelsX / float( nbPixelsY );
	
} // CameraUnit::setParams


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__host__ void Camera::zeroLayerIfCameraStateChanged( int layer )
{
	if ( data.mCameraStateChanged )
	{
		zeroLayer(layer);
		data.mCameraStateChanged = false;
	}
	else
		data.renderIterationCounter+=1;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__host__ void Camera::zeroLayer( int layer )
{
	data.renderIterationCounter = 0;
	mRenderBufferStack.zeroLayer( layer );	
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__host__ void CameraUnit::resetDirection()
{
	m_dir.set( 0,0,1 );
	m_up.set( 0,1,0 );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__host__ void CameraUnit::updateCameraAttributes ( const Vec3& in_pos, const Vec3& in_upVec, const Vec3& in_dir, const float in_FOV  )
{
	if ( 
			!	in_pos.equal( m_pos )
		||	!	in_upVec.equal( m_up )
		||	!	in_dir.equal( m_dir )
		||		fabs( in_FOV - m_FOV ) > 0.001f
	)
	{
		mCameraStateChanged = true;
		
		m_pos = in_pos;
		m_up = in_upVec.getNormalized();
		m_dir = in_dir.getNormalized();
		m_FOV = in_FOV;

		//m_aspectRatio=1;
		// recalc frame coordinate system
		m_frameAxisX = m_dir.cross ( m_up ).getNormalized ();
		m_frameAxisY = m_frameAxisX.cross ( m_dir ).getNormalized ();

		// get frame space system origin point
		m_frameOriginPos = m_pos - ( m_frameAxisX * ( m_aspectRatio * 0.5f ) );
		// offset from center by half of frame_size_X in direction of frame_axis_X
		
		m_frameOriginPos -= m_frameAxisY * (  0.5f ); // also offset by Y

		// recalc focus point based on given FOV
		//m_focusPoint = ...

		Angle a = Angle::buildDegrees( in_FOV * 0.5f );
		float f = 0.5f * m_geomCameraSize / a.tan();
	
		m_focusPoint = m_pos - f * m_dir;
	}

} // CameraUnit::updateCameraAttributes


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__host__ void Camera::free()
{
	mRenderBufferStack.free();
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__device__ void CameraUnit::addScreenColor( RenderBuffer* layer, int x, int y, Color color )
{
	assert (
			x >= 0
		&&	x < mNbPixelsX
		&&	y >= 0
		&&	y < mNbPixelsY
	);

	Color* source = layer->color( x, y );
	float* depth = layer->squareDepth( x, y );
	
	*source = *source * renderIterationCounter;
	*source += color ;
	*source /= renderIterationCounter + 1;

//	*depth = *depth * currentAASample;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__device__ Ray CameraUnit::getRay( curandState* state, const int x, const int y ) const
{
	assert (
			x >= 0
		&&	x < mNbPixelsX
		&&	y >= 0
		&&	y < mNbPixelsY
		&&	m_aspectRatio > 0
		&&	m_aspectRatio < 999999.f
	);

	float xr = float(x);
	float yr = float(y);

	//randCubicRayOffset( xr, yr, currentAASample, maxAASamples );
	
	float AARasterPosX = xr / ( (float)mNbPixelsX - devRand_0_p1f(state) ) * m_aspectRatio * m_geomCameraSize;
	float AARasterPosY = yr / ( (float)mNbPixelsY - devRand_0_p1f(state) ) * m_geomCameraSize;
	Vec3 projectionAARayStartWorldPos = m_frameOriginPos + ( m_frameAxisX*AARasterPosX ) + ( m_frameAxisY*AARasterPosY );
		
	Vec3 AARayDir = ( projectionAARayStartWorldPos - m_focusPoint ).getNormalized();

	if ( mSquareDistFocalPlane <= 0 || mPixelDOFDiskRatio <= 0 )
	{		
		return Ray( projectionAARayStartWorldPos, AARayDir, Ray::camera, 0 );
	}
	else
	{
		float angle = devRandf( state, 0, TwoPi );
		float ratio = mPixelDOFDiskRatio* (mNbPixelsX+mNbPixelsY) * 0.5f * devRand_m1_p1f(state);
		xr += ratio * cos( angle );
		yr += ratio * sin( angle );

		float DOFRasterPosX = xr / mNbPixelsX * m_aspectRatio * m_geomCameraSize;
		float DOFRasterPosY = yr / mNbPixelsY * m_geomCameraSize;

		Vec3 projectionDOFRayStartWorldPos = m_frameOriginPos + ( m_frameAxisX*DOFRasterPosX ) + ( m_frameAxisY*DOFRasterPosY );

		Vec3 DOFdir = projectionAARayStartWorldPos + AARayDir * sqrt( mSquareDistFocalPlane );
		DOFdir -= projectionDOFRayStartWorldPos;
		DOFdir.voidNormalize();
	
		return Ray( projectionDOFRayStartWorldPos, DOFdir, Ray::camera, 0 );
	}
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
