
#include "PTESources/RenderSystem/Texture.cuh"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


struct BMPHeader
{
    unsigned int    bfType;
    unsigned long   bfSize;
    unsigned int    bfReserved1;
    unsigned int    bfReserved2;
    unsigned long   bfOffBits;
};
 
struct BMPInfoHeader
{
    unsigned int    biSize;
    int             biWidth;
    int             biHeight;
    unsigned short  biPlanes;
    unsigned short  biBitCount;
    unsigned int    biCompression;
    unsigned int    biSizeImage;
    int             biXPelsPerMeter;
    int             biYPelsPerMeter;
    unsigned int    biClrUsed;
    unsigned int    biClrImportant;
};


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


inline unsigned short read_u16(FILE *fp)
{
	unsigned char b0, b1;

	b0 = getc(fp);
	b1 = getc(fp);

	return ((b1 << 8) | b0);
}
inline unsigned int read_u32(FILE *fp)
{
	unsigned char b0, b1, b2, b3;

	b0 = getc(fp);
	b1 = getc(fp);
	b2 = getc(fp);
	b3 = getc(fp);

	return ((((((b3 << 8) | b2) << 8) | b1) << 8) | b0);
}
inline int read_s32(FILE *fp)
{
	unsigned char b0, b1, b2, b3;

	b0 = getc(fp);
	b1 = getc(fp);
	b2 = getc(fp);
	b3 = getc(fp);

	return ((int)(((((b3 << 8) | b2) << 8) | b1) << 8) | b0);
} 


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


float4* readBMPFile( const char* fullPath, int32& width, int32& height )
{
	FILE * file = 0;
	fopen_s ( &file, fullPath, "rb" );
	
	CPU_PTE_ASSERT( file, __FILE__, __LINE__ );

	BMPHeader header;

	header.bfType      = read_u16(file);
	header.bfSize      = read_u32(file);
	header.bfReserved1 = read_u16(file);
	header.bfReserved2 = read_u16(file);
	header.bfOffBits   = read_u32(file);

	BMPInfoHeader bmiHeader;

	bmiHeader.biSize          = read_u32(file);
	bmiHeader.biWidth         = read_s32(file);
	bmiHeader.biHeight        = read_s32(file);
	bmiHeader.biPlanes        = read_u16(file);
	bmiHeader.biBitCount      = read_u16(file);
	bmiHeader.biCompression   = read_u32(file);
	bmiHeader.biSizeImage     = read_u32(file);
	bmiHeader.biXPelsPerMeter = read_s32(file);
	bmiHeader.biYPelsPerMeter = read_s32(file);
	bmiHeader.biClrUsed       = read_u32(file);
	bmiHeader.biClrImportant  = read_u32(file);

	const char* typeStr = reinterpret_cast< const char* >( &header.bfType );
	assert(  ( typeStr[0] == 'b' || typeStr[0] == 'B' ) && ( typeStr[1] == 'm' || typeStr[1] == 'M' ) );

	width = bmiHeader.biWidth;
	height = bmiHeader.biHeight;

	float4 * buffer;
	allocHost( &buffer, height*width );
	ZeroMemory( buffer, sizeof(float4)*height*width );

	int id = 0;
	for (int i=0; i<height; i++)
	{
		for (int j=0; j<width; j++)
		{			
			//BGR read!!!! instead of RGB
			buffer[id].z = float( getc(file) / 255.f );
			buffer[id].y = float( getc(file) / 255.f );
			buffer[id].x = float( getc(file) / 255.f );		
			
			buffer[id].w = float( 255.f / 255.f );
			++id;
		}

		// last byte skip
	}

	fclose(file);
	return buffer;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__host__ ColorTextureUchar4::ColorTextureUchar4( const char* fullPath )
	:	texArray(0)
	,	tex(0)
{
	if( fullPath == 0 )
		return;

	int32 width, height;
	float4* buffer = readBMPFile( fullPath, width, height );
	
	cudaChannelFormatDesc channelDesc =	cudaCreateChannelDesc<float4>();
	checkCudaErrors( cudaMallocArray(&texArray, &channelDesc, width, height) );
	checkCudaErrors( cudaMemcpyToArray(texArray, 0, 0, buffer, width * height*sizeof(float4), cudaMemcpyHostToDevice) );
	freeHost( buffer );
	buffer = 0;

	cudaResourceDesc resDesc;
	memset(&resDesc, 0, sizeof(resDesc));
	resDesc.resType = cudaResourceTypeArray;
	resDesc.res.array.array = texArray;
	
	cudaTextureDesc texDesc;
	memset(&texDesc, 0, sizeof(texDesc));
	
	texDesc.addressMode[0] = cudaAddressModeWrap;
	texDesc.addressMode[1] = cudaAddressModeWrap; 
	
	texDesc.filterMode = cudaFilterModeLinear; 
	texDesc.readMode = cudaReadModeElementType; 
	texDesc.normalizedCoords = 1; 

	tex = 0xff;
	checkCudaErrors( cudaCreateTextureObject(&tex, &resDesc, &texDesc, NULL) );
	assert(texArray);
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__host__ ColorTextureUchar4::ColorTextureUchar4( ColorA c )
	:	texArray(0)
	,	tex(0)
{
	float4 buffer;
	buffer.x = c.r;
	buffer.y = c.g;
	buffer.z = c.b;
	buffer.w = c.alpha;
	
	cudaChannelFormatDesc channelDesc =	cudaCreateChannelDesc<float4>();
	checkCudaErrors( cudaMallocArray(&texArray, &channelDesc, 1, 1) );
	checkCudaErrors( cudaMemcpyToArray(texArray, 0, 0, &buffer, 1 * 1 * sizeof(float4), cudaMemcpyHostToDevice) );
	
	cudaResourceDesc resDesc;
	memset(&resDesc, 0, sizeof(resDesc));
	resDesc.resType = cudaResourceTypeArray;
	resDesc.res.array.array = texArray;
	
	cudaTextureDesc texDesc;
	memset(&texDesc, 0, sizeof(texDesc));
	
	texDesc.addressMode[0] = cudaAddressModeWrap;
	texDesc.addressMode[1] = cudaAddressModeWrap; 
	
	texDesc.filterMode = cudaFilterModeLinear; 
	texDesc.readMode = cudaReadModeElementType; 
	texDesc.normalizedCoords = 1; 

	
	tex = 0xff;
	checkCudaErrors( cudaCreateTextureObject(&tex, &resDesc, &texDesc, NULL) );
	assert(texArray);
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__host__ void ColorTextureUchar4::free()
{
	cudaDestroyTextureObject(tex);
	cudaFreeArray(texArray);
	tex = 0;
	texArray = 0;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__device__ ColorA ColorTextureUchar4::get( float nx, float ny ) const
{
	DEVICE_PTE_ASSERT( nx>=0 && nx<=1 && ny>=0 && ny<=1, __FILE__, __LINE__ );

	float4 r = tex2D<float4>( tex, nx, ny );
	return ColorA( r.x, r.y, r.z, r.w );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__device__ Vec3 NormalTextureVec3::get( float nx, float ny, const Vec3& trueNormal  ) const
{
	ColorA c = ColorTextureUchar4::get( nx, ny );
	Vec3 t;
	t.x = c.r;
	t.y = c.g;
	t.z = c.b;
	
	t.voidNormalize();

	Vec3 rotAxis = trueNormal.cross( Vec3(0,1,0) );
	if ( rotAxis.magnitude() > 0.999f )
	{
		float angleCos = trueNormal.dot( Vec3(0,1,0) );

		t.rotate( angleCos, rotAxis );
		t.voidNormalize();
	}

	return t;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__device__ Color SphereEnvTex::onRay( const Ray& ray ) const
{
	float u = 0.5f + atan2( ray.dir.z, ray.dir.x ) / TwoPi;
	// used 1-v!
	float v = 0.5f + asin( ray.dir.y ) / Pi;

	return ColorTextureUchar4::get( u, v );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__host__ ColorTextureUchar4::ColorTextureUchar4( const ColorTextureUchar4* that )
{
	if( that )
	{
		tex = that->tex;
		texArray = that->texArray;
	}
	else
	{
		tex = 0;
		texArray = 0;
	}
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__host__ NormalTextureVec3::NormalTextureVec3( const NormalTextureVec3* that )
	:	ColorTextureUchar4(that)
{}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


__host__ EmitTextureUchar4::EmitTextureUchar4( const EmitTextureUchar4* that )
	:	ColorTextureUchar4(that)
{
	if( that )
		intensity = that->intensity;
	else
		intensity = 0;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace PTE

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

