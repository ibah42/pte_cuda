#include "PTESources/PCH.hpp"
#include "PTESources/RenderSystem/Texture.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

		 
struct BMPHeader
{
    unsigned int    bfType;
    unsigned long   bfSize;
    unsigned int    bfReserved1;
    unsigned int    bfReserved2;
    unsigned long   bfOffBits;
};
 
struct BMPInfoHeader
{
    unsigned int    biSize;
    int             biWidth;
    int             biHeight;
    unsigned short  biPlanes;
    unsigned short  biBitCount;
    unsigned int    biCompression;
    unsigned int    biSizeImage;
    int             biXPelsPerMeter;
    int             biYPelsPerMeter;
    unsigned int    biClrUsed;
    unsigned int    biClrImportant;
};

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

inline unsigned short read_u16(FILE *fp)
{
	unsigned char b0, b1;

	b0 = getc(fp);
	b1 = getc(fp);

	return ((b1 << 8) | b0);
}
inline unsigned int read_u32(FILE *fp)
{
	unsigned char b0, b1, b2, b3;

	b0 = getc(fp);
	b1 = getc(fp);
	b2 = getc(fp);
	b3 = getc(fp);

	return ((((((b3 << 8) | b2) << 8) | b1) << 8) | b0);
}
inline int read_s32(FILE *fp)
{
	unsigned char b0, b1, b2, b3;

	b0 = getc(fp);
	b1 = getc(fp);
	b2 = getc(fp);
	b3 = getc(fp);

	return ((int)(((((b3 << 8) | b2) << 8) | b1) << 8) | b0);
} 


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

//http://ziggi.org/chtenie-bmp-v-cpp/

ColorTextureUchar4::ColorTextureUchar4( const char* relativePath )
{
	FILE * file = 0;
	char path[1000];
	GetCurrentDirectory( 1000, path );
	std::string str = path;
	str += "\\";
	str += relativePath;

	fopen_s ( &file, str.c_str(), "rb" );
	
	BMPHeader header;

	header.bfType      = read_u16(file);
	header.bfSize      = read_u32(file);
	header.bfReserved1 = read_u16(file);
	header.bfReserved2 = read_u16(file);
	header.bfOffBits   = read_u32(file);

	BMPInfoHeader bmiHeader;

	bmiHeader.biSize          = read_u32(file);
	bmiHeader.biWidth         = read_s32(file);
	bmiHeader.biHeight        = read_s32(file);
	bmiHeader.biPlanes        = read_u16(file);
	bmiHeader.biBitCount      = read_u16(file);
	bmiHeader.biCompression   = read_u32(file);
	bmiHeader.biSizeImage     = read_u32(file);
	bmiHeader.biXPelsPerMeter = read_s32(file);
	bmiHeader.biYPelsPerMeter = read_s32(file);
	bmiHeader.biClrUsed       = read_u32(file);
	bmiHeader.biClrImportant  = read_u32(file);

	const char* typeStr = reinterpret_cast< const char* >( &header.bfType );
	assert(  ( typeStr[0] == 'b' || typeStr[0] == 'B' ) && ( typeStr[1] == 'm' || typeStr[1] == 'M' ) );

	mx = bmiHeader.biWidth;
	my = bmiHeader.biHeight;
	totalCount = mx*my;


	int id = 0;
	for (int i=0; i<my; i++)
	{
		for (int j=0; j<mx; j++)
		{			
			buffer[id].b = getc(file);
			buffer[id].g = getc(file);
			buffer[id].r = getc(file);
			buffer[id].al = 255;
			++id;
		}

		// last byte skip
	}

	fclose(file);
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

//http://ziggi.org/chtenie-bmp-v-cpp/

ColorA 
ColorTexture::onFilterBiLinear( float u, float v ) const
{
	u -= int(u);
	v -= int(v);

	if ( mx < 2 && my < 2 )
	{
		ColorA result;
		result.alpha = buffer->al / 255.f;
		result.c.r = buffer->r / 255.f;
		result.c.g = buffer->g / 255.f;
		result.c.b = buffer->b / 255.f;

		return result;
	}
	
	float xTrace = mx * u;
	float yTrace = my * v;

	// locate neighbors to interpolate
	const int x0 = (int)xTrace;
	const int x1 = x0 + 1;
	const int y0 = (int)yTrace;
	const int y1 = y0 + 1;

	// get interpolation weights
	const float s1 = xTrace - x0;
	const float s0 = 1.0f - s1;

	const float t1 = yTrace - y0;
	const float t0 = 1.0f - t1;

	// 4 corners 
	int i000 = x0 + y0 * mx ;
	int i010 = i000 + mx;
	int i100 = i000 + 1;
	int i110 = i010 + 1;

	i000 = clamp( 0, totalCount-1, i000 );
	i010 = clamp( 0, totalCount-1, i010 );
	i100 = clamp( 0, totalCount-1, i100 );
	i110 = clamp( 0, totalCount-1, i110 );

	assert( i000 < mx*my && i010 < mx*my && i100 < mx*my && i110 < mx*my );
	
	ColorTexture::Texel tex0,tex1,tex2,tex3;
	tex0 = buffer[i000];
	tex1 = buffer[i010];
	tex2 = buffer[i100];
	tex3 = buffer[i110];

	ColorA result;
	result.alpha = s0* ( tex0.al / 255.f * t0  + tex0.al / 255.f * t1 ) + s1 * ( tex2.al / 255.f * t0  + tex3.al / 255.f * t1 );
	result.c.r = s0* ( tex0.r / 255.f * t0  + tex1.r / 255.f * t1 ) + s1 * ( tex2.r / 255.f * t0  + tex3.r / 255.f * t1 );
	result.c.g = s0* ( tex0.g / 255.f * t0  + tex1.g / 255.f * t1 ) + s1 * ( tex2.g / 255.f * t0  + tex3.g / 255.f * t1 );
	result.c.b = s0* ( tex0.b / 255.f * t0  + tex1.b / 255.f * t1 ) + s1 * ( tex2.b / 255.f * t0  + tex3.b / 255.f * t1 );

	return result;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


NormalTexture::NormalTexture( const char* relativePath )
{
	FILE * file = 0;
	char path[1000];
	GetCurrentDirectory( 1000, path );
	std::string str = path;
	str += "\\";
	str += relativePath;

	fopen_s ( &file, str.c_str(), "rb" );

	BMPHeader header;

	header.bfType      = read_u16(file);
	header.bfSize      = read_u32(file);
	header.bfReserved1 = read_u16(file);
	header.bfReserved2 = read_u16(file);
	header.bfOffBits   = read_u32(file);

	BMPInfoHeader bmiHeader;

	bmiHeader.biSize          = read_u32(file);
	bmiHeader.biWidth         = read_s32(file);
	bmiHeader.biHeight        = read_s32(file);
	bmiHeader.biPlanes        = read_u16(file);
	bmiHeader.biBitCount      = read_u16(file);
	bmiHeader.biCompression   = read_u32(file);
	bmiHeader.biSizeImage     = read_u32(file);
	bmiHeader.biXPelsPerMeter = read_s32(file);
	bmiHeader.biYPelsPerMeter = read_s32(file);
	bmiHeader.biClrUsed       = read_u32(file);
	bmiHeader.biClrImportant  = read_u32(file);

	const char* typeStr = reinterpret_cast< const char* >( &header.bfType );
	assert(  ( typeStr[0] == 'b' || typeStr[0] == 'B' ) && ( typeStr[1] == 'm' || typeStr[1] == 'M' ) );

	mx = bmiHeader.biWidth;
	my = bmiHeader.biHeight;
	totalCount = mx*my;

	buffer = new Vec3[ mx*my ];

	int id = 0;
	for (int i=0; i<my; i++)
	{
		for (int j=0; j<mx; j++)
		{			
			// B G R
			// blue is true-height ( y in 3D, z in 2D )
			// red is x ( x in 3D\2D space )
			// green is ( z in 3D, y in 2D space )
			buffer[id].y = 1 - getc(file) / 128.f;
			buffer[id].z = 1 - getc(file) / 128.f;
			buffer[id].x = 1 - getc(file) / 128.f;
			buffer[id].voidNormalize();
			++id;
		}

		// last byte skip
	}

	fclose(file);
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


Vec3 
NormalTexture::onFilterBiLinear( float u, float v, const Vec3& trueNormal ) const
{
	if ( mx < 2 && my < 2 )
		return trueNormal;

	float xTrace = mx * u;
	float yTrace = my * v;

	// locate neighbors to interpolate
	const int x0 = (int)xTrace;
	const int x1 = x0 + 1;
	const int y0 = (int)yTrace;
	const int y1 = y0 + 1;

	// get interpolation weights
	const float s1 = xTrace - x0;
	const float s0 = 1.0f - s1;

	const float t1 = yTrace - y0;
	const float t0 = 1.0f - t1;

	// 4 corners 
	int i000 = x0 + y0 * mx ;
	int i010 = i000 + mx;
	int i100 = i000 + 1;
	int i110 = i010 + 1;

	i000 = clamp( 0, totalCount-1, i000 );
	i010 = clamp( 0, totalCount-1, i010 );
	i100 = clamp( 0, totalCount-1, i100 );
	i110 = clamp( 0, totalCount-1, i110 );

	assert( i000 < mx*my && i010 < mx*my && i100 < mx*my && i110 < mx*my );
	
	Vec3 tex0,tex1,tex2,tex3;
	tex0 = buffer[i000];
	tex1 = buffer[i010];
	tex2 = buffer[i100];
	tex3 = buffer[i110];
	
	assert( (0 != 1) == 1);

	tex0 *= -1.f * ( tex0.dot(trueNormal) < 0 );
	tex1 *= -1.f * ( tex1.dot(trueNormal) < 0 );
	tex2 *= -1.f * ( tex2.dot(trueNormal) < 0 );
	tex3 *= -1.f * ( tex3.dot(trueNormal) < 0 );

	Vec3 result;
	
	result.x = s0* ( tex0.x * t0  + tex1.x * t1 ) + s1 * ( tex2.x * t0  + tex3.x * t1 );
	result.y = s0* ( tex0.y * t0  + tex1.y * t1 ) + s1 * ( tex2.y * t0  + tex3.y * t1 );
	result.z = s0* ( tex0.z * t0  + tex1.z * t1 ) + s1 * ( tex2.z * t0  + tex3.z * t1 );

	result.voidNormalize();

	Vec3 rotAxis = trueNormal.cross( Vec3(0,1,0) );
	if ( rotAxis.magnitude() > 0.999f )
	{
		float angleCos = trueNormal.dot( Vec3(0,1,0) );

		result.rotate( angleCos, rotAxis );
		result.voidNormalize();
	}

	return result;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


Spheremap::Spheremap( const char * fileName )
	:	tex( fileName )
{}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


Color Spheremap::intersect( const Ray& ray )
{
	float u = 0.5f + atan2( ray.dir.z, ray.dir.x ) / TwoPi;
	// used 1-v!
	float v = 0.5f + asin( ray.dir.y ) / Pi;

	ColorA c = tex.onFilterBiLinear( fabs(u), fabs(v) );
	return Color( c.c.r, c.c.g, c.c.b );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace PTE{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
