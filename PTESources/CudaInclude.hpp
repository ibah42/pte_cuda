
/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


#include <limits.h>
#include <cuda_runtime.h>
#include <helper_math.h>
#include <helper_cuda.h>
#include <device_launch_parameters.h>
#include <cuda.h>
#include <builtin_types.h>
#include <drvapi_error_string.h>
#include <cuda_texture_types.h>
#include <curand.h>
#include <curand_kernel.h>

#include <cassert>
#include <cmath>
#include <cassert>
#include <ctime>
#include <malloc.h>
#include <vector>
#include <set>
#include <map>
#include <iostream>

#include <atomic>

#include <windows.h>

#define TRIANGLE_RAYCAST_OFFSET (0.001f)

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#define MAX_FLOAT +9E+30f
#define MIN_FLOAT -9E+30f

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/