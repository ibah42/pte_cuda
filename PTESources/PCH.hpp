#pragma once 

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#include "PTESources/CudaInclude.hpp"

#define USE_GLM

#include "PTESources/GL/glew.h"
#include "PTESources/GL/glut.h"
#include "PTESources/glm-0.9.2.7/glm/glm.hpp"
#include "PTESources/glm-0.9.2.7/glm/gtc/matrix_transform.hpp"

#include "PTESources/Math/Utility.hpp"
#include "PTESources/Math/Matrix33.hpp"
#include "PTESources/Math/Matrix44.hpp"
#include "PTESources/Math/Transform.hpp"
#include "PTESources/Math/Vector2.hpp"
#include "PTESources/Math/Vector3.hpp"
#include "PTESources/Math/Vector4.hpp"
#include "PTESources/Math/Quaternion.hpp"
#include "PTESources/Math/Angle.hpp"
#include "PTESources/Math/Geometry.hpp"

#include "PTESources/RenderSystem/Color.cuh"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
